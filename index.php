<?php 
include('admin/core/config.php');
if(!isset($_SESSION['cust_user_id'])){
  $cart = "display: none";
  $auth = "display: block";
}else{
  $userID = $_SESSION['cust_user_id'];
  $cart = "display: block";
  $auth = "display: none";
}
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Flower PauER Arrangement Shop Syste,</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="admin/assets/css/animate.css" rel="stylesheet">
  <link href="admin/assets/css/sweetalert.min.css" rel="stylesheet">
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="admin/assets/js/sweetalert.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      max-height: 400px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  .badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 12px;
    font-weight: bold;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    /* vertical-align: middle; */
    background-color: #fb6fcc;
    border-radius: 10px;
    position: absolute;
    bottom: 7px;
    left: 10px;
}
  #ribbon-container {
        position: absolute;
        top: 8px;
        right: 5px;
        overflow: visible; /* so we can see the pseudo-elements we're going to add to the anchor */
        font-size: 18px; /* font-size and line-height must be equal so we can account for the height of the banner */
        line-height: 18px;
      }
      
      #ribbon-container:before {
        content: "";
        height: 0;
        width: 0;
        display: block;
        position: absolute;
        top: 3px;
        left: 0;
        border-top: 3px solid rgba(0,0,0,.3);
        border-bottom: 29px solid rgba(0,0,0,.3);
        border-right: 24px solid rgba(0,0,0,.3);
        border-left: 34px solid transparent;
      }
      
      #ribbon-container:after { /* This adds the second part of our dropshadow */
        content: "";
        height: 3px;
        background: rgba(0,0,0,.3);
        display: block;
        position: absolute;
        bottom: -3px;
        left: 58px;
        right: 3px;
      }
      
      #ribbon-container a {
          display: block;
          padding: 8px;
          position: relative;
          background: red;
          overflow: visible;
          height: 32px;
          margin-left: 29px;
          color: #fff;
          text-decoration: none;
      }
      
      #ribbon-container a:after { /* this creates the "folded" part of our ribbon */
          content: "";
          height: 0;
          width: 0;
          display: block;
          position: absolute;
          bottom: -16px;
          right: 0;
          border-top: 16px solid #610404;
          border-right: 15px solid transparent;
      }
      
      #ribbon-container a:before { /* this creates the "forked" part of our ribbon */
          content: "";
          height: 0px;
          width: 0;
          display: block;
          position: absolute;
          top: 0;
          left: -29px;
          border-top: 16px solid red;
          border-bottom: 16px solid red;
          border-right: 16px solid transparent;
          border-left: 29px solid transparent;
      }
      hr {
          margin-top: 31px;
          margin-bottom: 20px;
          border: 0;
          border-top: 1px solid #eeeeee;
      }
     /* #ribbon-container a:hover {
        background:#009ff1;
      }
      
      #ribbon-container a:hover:before {  this makes sure that the "forked" part of the ribbon changes color with the anchor on :hover 
        border-top: 29px solid #009ff1; 
        border-bottom: 29px solid #009ff1;
      }*/
  </style>
</head>
<body>
<nav class="navbar navbar-inverse" style="background-color: #f31b6c66;">
  <div class="container-fluid">
    <center><div class="navbar-header">
      <img src="assets/images/logo13.png" style="height: 50px;width: 50px;object-fit: cover;">
    </div></center>
  </div>
</nav>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="assets/images/banner1.png" style="object-fit: cover;" alt="Image">
        <div class="carousel-caption">
          <p></p>
        </div>      
      </div>

      <div class="item">
        <img src="assets/images/banner2.png" alt="Image">
        <div class="carousel-caption">
          <p></p>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
<nav class="navbar" style="background-color: #f31b6c66;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color: black;"></span>
        <span class="icon-bar" style="background-color: black;"></span>
        <span class="icon-bar" style="background-color: black;"></span>                        
      </button>
      <a class="navbar-brand" href="#" onclick='window.location="index.php"'><img src="assets/images/logo13.png" style="height: 20px;width: 32px;object-fit: cover;"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#" onclick='window.location="customer/item_view.php?view=cake-list"'> Cakes </a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Types <span class="caret"></span></a>
          <ul class="dropdown-menu">
          	<?php 
          	$category = mysql_query("SELECT * FROM tbl_category");
          	while($row_c = mysql_fetch_array($category)){
              $catID = $row_c['category_id'];
              $view_name = 'view-item';
          	?>
            	<li><a href="#" onclick='window.location="customer/item_view.php?view=category-items&id=<?=$catID?>"'><?=$row_c['category_name']?></a></li>
        	<?php } ?>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Categories <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php 
          	$occasion = mysql_query("SELECT * FROM tbl_occasion_list");
          	while($row_o = mysql_fetch_array($occasion)){
              $occasionID = $row_o['occasion_id'];
          	?>
            	<li><a href="#" onclick='window.location="customer/item_view.php?view=occasion-items&id=<?=$occasionID?>"'><?=$row_o['occasion_name']?></a></li>
        	<?php } ?>
          </ul>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li style="<?=$auth?>"><a href="#" data-toggle='modal' data-target='#loginModal'>Login</a></li>
        <li style="<?=$auth?>"><a href="#" data-toggle='modal' data-target='#signup'>Sign Up</a></li>
        <li style="<?=$cart?>"><a href="#" onclick='viewCart()'><span class="glyphicon glyphicon-shopping-cart"><span class="badge"><?=countCartItems($userID)?></span></span></a></li>
        <li class="dropdown" style="<?=$cart?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#" onclick='window.location="customer/item_view.php?view=profile&id=<?=$userID?>"'> <span class="glyphicon glyphicon-user"></span> Profile </a></li>
            <li><a href="#" onclick='window.location="customer/item_view.php?view=t_and_c"'><span class="glyphicon glyphicon-th-list"></span> Terms and Conditions </a></li>
            <li><a href="#" onclick='window.location="customer/item_view.php?view=messages"'><span class="glyphicon glyphicon-envelope"></span> Messages </a></li>
            <li><a href="#" onclick='logout()'><span class="glyphicon glyphicon-log-out"></span> Logout </a></li>
          </ul>
        </li>
        
      </ul>
    </div>
  </div>
</nav>
<div class="container text-center">    
  <h3 style="font-weight: bolder;color: red;" class="animated flash infinite">Flash Sales</h3><br>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>
<div class="row animated zoomIn">
   <?php 
   $curdate = date("Y-m-d", strtotime(getCurrentDate()));
     $itemList = mysql_query("SELECT 
                              o.occasion_item_id as typeID, 
                              o.occasion_item_name as `name`, 
                              o.occasion_item_desc as `desc`, 
                              o.occasion_item_img as `img`, 
                              o.occasion_item_price as `price`, 
                              'O' as `type` 
                              FROM `tbl_occasion_items` as o, tbl_price_discount as d WHERE o.occasion_item_id = d.item_id AND d.item_type = 'O' AND d.status = 0 AND d.discount_duration_date >= '$curdate'
                            UNION  
                            SELECT 
                            c.item_id as typeID, 
                            c.item_name as `name`, 
                            c.item_desc as `desc`, 
                            c.item_img as `img`, 
                            c.item_price as `price`, 
                            'C' as `type` 
                            FROM `tbl_category_items` as c, tbl_price_discount as cd WHERE c.item_id = cd.item_id AND cd.item_type = 'C' AND cd.status = 0 AND cd.discount_duration_date >= '$curdate'
                            UNION 
                            SELECT ce.cake_id as typeID, 
                            ce.cake_name as `name`, 
                            ce.cake_desc as `desc`, 
                            ce.cake_img as `img`, 
                            ce.cake_price as `price`, 
                            'CE' as `type` 
                            FROM `tbl_cakes` as ce, tbl_price_discount as ced WHERE ce.cake_id = ced.item_id AND ced.item_type = 'CE' AND ced.status = 0 AND ced.discount_duration_date >= '$curdate'");
     while($itemfetch = mysql_fetch_array($itemList)){
      $id = $itemfetch['typeID'];
      $type = $itemfetch['type'];
      $color = (getRemainingQuantity($id, $type) == 0)?"color:red":"";
      $text_status = (getRemainingQuantity($id, $type) == 0)?"NOT AVAILABLE":"AVAILABLE";
      $remain_qntty = "<h4 style='padding: 7px;background-color: #10c2ffa6;
    border-radius: 5px;$color'>".$text_status."</h4>";

      $location = ($itemfetch['type'] == 'O')?"window.location=customer/item_view.php?view=occasion-add-to-cart&id=$id":(($itemfetch['type'] == 'C')?"window.location=customer/item_view.php?view=category-add-to-cart&id=$id":"window.location=customer/item_view.php?view=cake-add-to-cart&id=$id");

      $discountCheck = mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$id' AND item_type = '$type' AND status = 0");
      $countRows = mysql_num_rows($discountCheck);
      $mfa_rows = mysql_fetch_array($discountCheck);
      $discount = ($mfa_rows['discount_type'] == 0)?"&#8369; ".number_format($mfa_rows['discounted_price'], 2)." Off":"".number_format($mfa_rows['discounted_price'])."% Off";
      if($countRows > 0){
        $ribbon = "<div id='ribbon-container'><a href='#'>".$discount."</a></div>";
        $hr = "<hr>";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($mfa_rows['new_price'], 2)."</h4>";

        $original_price = " <h4 style='font-weight: bolder;text-decoration: line-through;background-color: red;color: white;border-radius: 5px;'>&#8369; ".number_format($itemfetch['price'], 2)."</h4>";
      }else{
        $ribbon = "";
        $hr = "";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($itemfetch['price'], 2)."</h4>";

        $original_price = "";
      }
      if(isset($_SESSION['cust_user_id'])){
   ?>
      <div class="col-md-3">        
          <div class="thumbnail" style="cursor: pointer;" onclick='addToCartClickedItem(<?=$id?>,"<?=$type?>")'>
              <?=$ribbon?>
              <div class="caption" style="background-color: #f3c1d4;color: black;font-weight: bolder;">
                <hr>
                  <h4 style="font-size: 25px;"><?=$itemfetch['name']?></h4>
              </div>
              <img src="admin/assets/images/<?=$itemfetch['img']?>" alt="..." style='height: 170px;width: 100%;object-fit: cover;'>
              <div class="row">
                <div class="col-md-6"><?=$original_price?></div>
                <div class="col-md-6"><?=$discounted_price?></div>
                <div class="col-md-12"><?=$remain_qntty?></div>
              </div>
          </div>
     
      </div>
      <?php }else{ ?>
        <div class="col-md-3">
          <div class="thumbnail" style="cursor: pointer;">
            <?=$ribbon?>
              <div class="caption" style="background-color: #f3c1d4;color: black;font-weight: bolder;">
                <hr>
                  <h4 style="font-size: 25px;"><?=$itemfetch['name']?></h4>
                  
              </div>
              <img src="admin/assets/images/<?=$itemfetch['img']?>" alt="..." style='height: 170px;width: 100%;object-fit: cover;'>
              <div class="row">
                <div class="col-md-6"><?=$original_price?></div>
                <div class="col-md-6"><?=$discounted_price?></div>
              </div>
          </div>

        </div>
      <?php } ?>
         
  <?php  } ?>

</div>
</div><br>
<div class="container text-center">    
  <h3> Shop By Categories </h3><br>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>
<div class="row animated zoomIn">
   <?php 
     $shopbycat = mysql_query("SELECT * FROM tbl_category");
     while($shopfetch = mysql_fetch_array($shopbycat)){
      $categoryID = $shopfetch['category_id'];
      if(isset($_SESSION['cust_user_id'])){
   ?>
    <div class="col-md-3">            
          <div class="thumbnail" style="cursor: pointer;" onclick='window.location="customer/item_view.php?view=category-items&id=<?=$categoryID?>"'>
              <div class="caption" style="position: absolute;color: black;font-weight: bolder;">
                  <h4 style="font-size: 25px;"><?=$shopfetch['category_name']?></h4>
              </div>
              <img src="assets/images/shop_by.jpg" alt="...">
          </div>
      </div>
  <?php }else { ?> 
    <div class="col-md-3">            
          <div class="thumbnail" style="cursor: pointer;" onclick=''>
              <div class="caption" style="position: absolute;color: black;font-weight: bolder;">
                  <h4 style="font-size: 25px;"><?=$shopfetch['category_name']?></h4>
              </div>
              <img src="assets/images/shop_by.jpg" alt="...">
          </div>
      </div>
  <?php } } ?>

</div>
</div><br>
<div class="container text-center">    
  <h3> Item List </h3><br>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>
<div class="row animated zoomIn">
   <?php 
      $curdate = date("Y-m-d", strtotime(getCurrentDate()));
     $itemList = mysql_query("SELECT occasion_item_id as typeID, occasion_item_name as `name`, occasion_item_desc as `desc`, occasion_item_img as `img`, occasion_item_price as `price`, 'O' as `type` FROM `tbl_occasion_items` UNION SELECT item_id as typeID, item_name as `name`, item_desc as `desc`, item_img as `img`, item_price as `price`, 'C' as `type` FROM `tbl_category_items` UNION SELECT cake_id as typeID, cake_name as `name`, cake_desc as `desc`, cake_img as `img`, cake_price as `price`, 'CE' as `type` FROM `tbl_cakes`");
     while($itemfetch = mysql_fetch_array($itemList)){
      $id = $itemfetch['typeID'];
      $type = $itemfetch['type'];

      $color = (getRemainingQuantity($id, $type) == 0)?"color:red":"";
      $remain_qntty = "<h4 style='padding: 7px;background-color: #10c2ffa6;
    border-radius: 5px;$color'>".$text_status."</h4>";
    //   $remain_qntty = "<h4 style='padding: 7px;background-color: #10c2ffa6;
    // border-radius: 5px;$color'>Remaining Quantity: ".getRemainingQuantity($id, $type)."</h4>";

      $original_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($itemfetch['price'], 2)."</h4>";
      $location = ($itemfetch['type'] == 'O')?"window.location=customer/item_view.php?view=occasion-add-to-cart&id=$id":(($itemfetch['type'] == 'C')?"window.location=customer/item_view.php?view=category-add-to-cart&id=$id":"window.location=customer/item_view.php?view=cake-add-to-cart&id=$id");

      $discountCheck = mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$id' AND item_type = '$type' AND status = 0 AND discount_duration_date >= '$curdate'");
      $countRows = mysql_num_rows($discountCheck);
      $mfa_rows = mysql_fetch_array($discountCheck);
      if($countRows == 0){
      if(isset($_SESSION['cust_user_id'])){
   ?>
      <div class="col-md-3">  
          <div class="thumbnail" style="cursor: pointer;" onclick='addToCartClickedItem(<?=$id?>,"<?=$type?>")'>
          
              <div class="caption" style="background-color: #f3c1d4;color: black;font-weight: bolder;">
                <hr>
                  <h4 style="font-size: 25px;"><?=$itemfetch['name']?></h4>
              </div>
              <img src="admin/assets/images/<?=$itemfetch['img']?>" alt="..." style='height: 170px;width: 100%;object-fit: cover;'>
              <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6"><?=$original_price?></div>
                <div class="col-md-12"><?=$remain_qntty?></div>
              </div>
          </div>
      </div>
      <?php }else{ ?>
        <div class="col-md-3">
          <div class="thumbnail" style="cursor: pointer;">
              <div class="caption" style="background-color: #f3c1d4;color: black;font-weight: bolder;">
                <hr>
                  <h4 style="font-size: 25px;"><?=$itemfetch['name']?></h4>
                  
              </div>
              <img src="admin/assets/images/<?=$itemfetch['img']?>" alt="..." style='height: 170px;width: 100%;object-fit: cover;'>
              <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6"><?=$original_price?></div>
              </div>
          </div>

        </div>
      <?php } ?>
         
  <?php }  } ?>

</div>
</div><br>

<footer class="container-fluid text-center" style="background-color: #faa4c43d;">
 	<p class="credits">
         © 2020 Flower PauER Arrangement Shop System.
     </p>
</footer>
<div class="modal fade" id="loginModal" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="cake_variants"><span class="glyphicon glyphicon-lock"></span> Authentication </h4>
      </div>
      <div class="modal-body">
          <div class="input-group">
            <div class="input-group-addon">
                Username
            </div>
            <input type="text" id="username" name="username" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Password
            </div>
            <input type="password" id="password" name="password" class="form-control">
          </div>
      </div>
      <div class="modal-footer">
        <span class="btn-group">
          <button class="btn btn-primary btn-sm" id="btnLogin" onclick='loginUser()' type="button"><span class="fa fa-check-circle"></span> Login</button>
        </span>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="signup" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="cake_variants"><span class="glyphicon glyphicon-lock"></span> Register </h4>
      </div>
      <div class="modal-body">
        <div class="input-group">
            <div class="input-group-addon">
                Firstname
            </div>
            <input type="text" id="firstname" name="firstname" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Middlename
            </div>
            <input type="text" id="middlename" name="middlename" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Lastname
            </div>
            <input type="text" id="lastname" name="lastname" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Contact #
            </div>
            <input type="text" id="contact" name="contact" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Address
            </div>
            <textarea style="resize: none;" rows="4" class="form-control" id="address"></textarea>
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                E-mail Address
            </div>
            <input type="text" id="emailadd" name="emailadd" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Username
            </div>
            <input type="text" id="s_username" name="s_username" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Password
            </div>
            <input type="password" id="s_password" name="s_password" class="form-control">
          </div>
      </div>
      <div class="modal-footer">
        <span class="btn-group">
          <button class="btn btn-primary btn-sm" id="btnsignin" onclick='registerUser()' type="button"><span class="fa fa-check-circle"></span> Login</button>
        </span>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript">
  //window.location.href = 'index.php?view=home';
  $(document).ready(function(){

  });
  function addToCartClickedItem(id, type){
    $.post("admin/ajax/quantityChecker.php", {
      id: id,
      type: type
    }, function(data){
      if(data > 0){
        if(type == 'C'){
          window.location = 'customer/item_view.php?view=category-add-to-cart&id='+id;
        }else if(type == 'O'){
          window.location = 'customer/item_view.php?view=occasion-add-to-cart&id='+id;
        }else{
          window.location = 'customer/item_view.php?view=cake-add-to-cart&id='+id;
        }
      }else{
        swal({
            title: "Aw Snap!",
            text: "Unable to add this item to your cart",
            type: "warning"
          }, function(){
            swal.close();
          });
      }
    })  
  }
  function viewCart(){
    window.location = 'customer/item_view.php?view=view-cart';
  }
  function logout(){
    window.location = 'admin/ajax/cust_logout.php';
  }
  function registerUser(){
    var firstname = $("#firstname").val();
    var middlename = $("#middlename").val();
    var lastname = $("#lastname").val();
    var contact = $("#contact").val();
    var address = $("#address").val();
    var username = $("#s_username").val();
    var password = $("#s_password").val();
    var emailadd = $("#emailadd").val();
    $("#btnsignin").prop("disabled", true);
    $("#btnsignin").html("<span class='glyphicon glyphicon-play-circle'></span> Loading");
    $.post("admin/ajax/register_customer.php", {
      firstname: firstname,
      middlename: middlename,
      lastname: lastname,
      contact: contact,
      address: address,
      username: username,
      password: password,
      emailadd: emailadd
    }, function(data){
      if(data > 0){
          swal({
            title: "All Good!",
            text: "Registration Complete",
            type: "success"
          }, function(){
            window.location.reload();
          });
      }else{
        swal("Something went wrong, Please Contact administrator");
      }
      
      
      $("#btnsignin").prop("disabled", true);
      $("#btnsignin").html("<span class='glyphicon glyphicon-play-circle'></span> Loading");
    });
  }
  function loginUser(){
    var username = $("#username").val();
    var password = $("#password").val();
    $.post("admin/ajax/sign_in_customer.php", {
      username: username,
      password: password
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Credentials Matched",
            type: "success"
          }, function(){
            window.location.reload();
          });
      }else{
        swal("Something went wrong, Please Contact administrator");
      }
    })
  }
</script>
