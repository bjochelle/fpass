<?php
switch ($view) {
	case 'category-items':
		require '../customer/category_items.php';
		break;
	case 'occasion-items':
		require '../customer/occasion_items.php';
		break;
	case 'category-add-to-cart':
		require '../customer/category_add_to_cart.php';
		break;
	case 'occasion-add-to-cart':
		require '../customer/occasion_add_to_cart.php';
		break;
	case 'cake-list':
		require '../customer/cake_list.php';
		break;
	case 'cake-add-to-cart':
		require '../customer/cake_add_to_cart.php';
		break;
	case 'view-cart':
		require '../customer/view_cart.php';
		break;
	case 'delivery-location':
		require '../customer/delivery_location.php';
		break;
	case 'profile':
		require '../customer/profile.php';
		break;
	case 'messages':
		require '../customer/messages.php';
		break;
	case 't_and_c':
		require '../customer/terms_and_conditions.php';
		break;
	case 'view-map':
		require '../customer/view_map.php';
		break;
	default:
		# code...
		break;
}
?>