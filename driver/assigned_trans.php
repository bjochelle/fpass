<div class="col-sm-12" style="overflow: auto;">
	<table id="Categories" class="table table-bordered" style="width: 100%">
		<thead>
			<tr>
				<th>#</th>
				<th>Customer</th>
				<th>Address</th>
				<th>Delivery Schedule</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready( function(data){
		getData();
	});
	function viewMap(id,type){
		window.location = 'index.php?view=view-map&id='+id+'&type='+type;
	}
	function finishDelivery(id,type){
		$.post("../admin/ajax/finishDelivery.php", {
			id:id,
			type:type
		}, function(data){
			if(data > 0){
				swal({
			        title: "All Good!",
			        text: "Items Successfully Delivered",
			        type: "success"
			      }, function(){
			       	getData();
			      });
			}else{

			}
		})
	}
	function getData(){
	    $("#Categories").DataTable().destroy();
	    $('#Categories').dataTable({
	    "processing":true,
	    "ajax":{
	        "url":"../admin/ajax/datatables/assign_trans.php",
	        "dataSrc":"data"
	    },
	    "columns":[
	        {
	            "data":"count"
	        },
	        {
	            "data":"customer"
	        },
	        {
	            "data":"show_address"
	        },
	        {
	            "data":"sched"
	        },
	        {
	            "data":"action"
	        }
	        
	    ]   
	    });
	  }
</script>