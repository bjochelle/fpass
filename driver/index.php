<?php 
include '../admin/core/config.php';
$user_id = $_SESSION['user_id'];
$category_id = $_SESSION['category'];

checkSession();
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';

?>
<!DOCTYPE>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title> Flower Pauer Arrangement Shop System </title>


  <link href="../admin/assets/images/FB_IMG_16054370060645320.jpg" rel="icon">
  <link href="../admin/assets/images/FB_IMG_16054370060645320.jpg" rel="apple-touch-icon">


  <link href="assets/bootstrap.css" rel="stylesheet">

  <link href="../admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="../admin/assets/css/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="../admin/assets/css/sweetalert.min.css" rel="stylesheet">

  <script src="assets/jquery.js"></script>
  <script src="assets/popper.js"></script>
  <script src="assets/bootstrap.js"></script>

 
  <script src="../admin/assets/js/jquery.dataTables.js"></script>
  <script src="../admin/assets/js/dataTables.bootstrap4.js"></script>
  <script src="../admin/assets/js/sweetalert.min.js"></script>

</head>

<body>
  <div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Driver's Access</h1>
  <p>Hello, <?=strtoupper(getuser($user_id));?></p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="#" onclick='logout()'><span class="fa fa-power-off"></span></a>
 <!--  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button> -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
   <!--  <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>    
    </ul> -->
  </div>  
</nav>
<div class="container" style="margin-top:30px">
  <div class="row">
    
    <?php 
      if($view == 'assigned-transaction'){
        require 'assigned_trans.php';
      }else{
        require 'view_map.php';
      }
    ?>
   
    </div>
  </div>
<div class="jumbotron text-center" style="margin-bottom:0">
  <p>Flower Pauer Arrangement Shop System</p>
</div>
</body>
<script type="text/javascript">
  function logout(){
      window.location.href = '../admin/ajax/logout.php';
    }
</script>
</html>

