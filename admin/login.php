<?php 
include 'core/config.php';
if(isset($_SESSION['user_id'])){
  header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title> Flower Pauer Arrangement Shop System </title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/style-responsive.css" rel="stylesheet">

</head>

<body>
  <div id="login-page">
    <div class="container">
      <form class="form-login" action="" id="loginForm" method="POST">
        <h2 class="form-login-heading"> Flower Pauer Arrangement Shop System </h2>
        <div class="login-wrap">
          <input type="text" name="username" class="form-control" placeholder="Username" autofocus>
          <br>
          <input type="password" name='password' class="form-control" placeholder="Password">
          <br>
          <button class="btn btn-theme btn-block" id='loginBtn' type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
        </div>
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
  <script>
    $(document).ready( function(){
      $("#loginForm").submit(function(e){
            e.preventDefault();
            $("#loginBtn").prop("disabled", true);
            $("#loginBtn").html("<span class='fas fa-spin fa-spinner'></span> Authenticating");
            var url = 'ajax/auth.php';
            var data = $(this).serialize();
            $.post(url , data , function(data){
                if(data == 0){
                    window.location = 'index.php?page=dashboard';
                }else if(data == 2){
                    window.location = '../driver/index.php?view=assigned-transaction';
                }else{
                   setTimeout(function(){
                    $("#showAlert").html("");
                   }, 3000);
                   $("#showAlert").html("<h5 style='color:red;text-align:center;' class='animated shake alert alert-danger'><span class='fas fa-exclamation-triangle'></span> Credentials did not matched.<h5>");
                }
                $("#password").val("");
                $("#username").val("");
                $("#loginBtn").prop("disabled", false);
                $("#loginBtn").html("<span class='fas fa-check-circle'></span> Sign in");
            });
        });
    })
    $.backstretch("../images/bg_2.jpg", {
      speed: 500
    });
  </script>
</body>

</html>
