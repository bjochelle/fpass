<?php 
include 'core/config.php';
$user_id = $_SESSION['user_id'];
$category_id = $_SESSION['category'];

checkSession();
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Flower Pauer Arrangement Shop System</title>

  <!-- Favicons -->
  <link href="assets/images/FB_IMG_16054370060645320.jpg" rel="icon">
  <link href="assets/images/FB_IMG_16054370060645320.jpg" rel="apple-touch-icon">

  <link href="assets/css/bootstrap.min.css" rel="stylesheet">

  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery.gritter.css" />

  <link href="assets/css/style.css" rel="stylesheet">
  
  <link href="assets/css/style-responsive.css" rel="stylesheet">
  <link href="assets/css/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="assets/css/bootstrap-multiselect.css" rel="stylesheet">
  <link href='assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  <link href="assets/css/sweetalert.min.css" rel="stylesheet">

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
  
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php include 'core/header.php'; ?>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <?php include 'core/sidebar.php'; ?>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <?php include 'routes/routes.php'; ?>
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
   <!--  <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer> -->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="assets/js/jquery.sparkline.js"></script>
  <script src="assets/js/common-scripts.js"></script>
  <script type="text/javascript" src="assets/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="assets/js/gritter-conf.js"></script>
  <script src="assets/js/sparkline-chart.js"></script>
 
  <script src="assets/js/jquery.dataTables.js"></script>
  <script src="assets/js/dataTables.bootstrap4.js"></script>
  <script src="assets/js/notify.min.js"></script>
  <script src="assets/js/bootstrap-multiselect.js"></script>
  <script src='assets/fullcalendar/lib/moment.min.js'></script>
  <script src="assets/fullcalendar/fullcalendar.min.js"></script>
  <script src="assets/js/sweetalert.min.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
         $('#select_search_all').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search Item'
        }); 
    });

    function printIframe(id){
        var iframe = document.frames ? document.frames[id] : document.getElementById(id);
        var ifWin = iframe.contentWindow || iframe;
        iframe.focus();
        ifWin.printPage();
        return false;
    }
    function gotoTransaction(id,nid){
      $.post("ajax/update_notif.php", {
        id: id,
        nid: nid
      }, function(data){
        if(data > 0){
           window.location = 'index.php?page=online-transaction-details&id='+id;
        }else{
           alertMe("fa fa-exclamation","Aw Snap! Error while saving the data.","error"); 
        }
      });
     
    }
    function gotoMsg(id,nid){
       $.post("ajax/update_notif.php", {
        id: id,
        nid: nid
      }, function(data){
        if(data > 0){
           window.location="index.php?page=messages&id="+id;
        }else{
           alertMe("fa fa-exclamation","Aw Snap! Error while saving the data.","error"); 
        }
      });
      
    }
    function logout(){
      window.location.href = 'ajax/logout.php';
    }
    function alertMe(icon,title,message,type){
      $.notify({
        icon: icon,
        title: title,
        message: message
      },{
        type: type
      });
    }
  </script>
</body>

</html>
