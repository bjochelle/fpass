<?php
$occ_id = $_GET['occ_id'];

?>
<h3><i class="fa fa-angle-right"></i> <?=getOccasionDetails('occasion_name',$occ_id)?> Item List </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#occasion_variation"><span class='fa fa-plus-circle'></span> Add</button>
      <button class='btn btn-sm btn-success pull-right' data-toggle='modal' data-target="#discount2"><span class='fa fa-plus-circle'></span> Add Discount</button>
      <button class='btn btn-sm btn-warning pull-right' id="removeAll" onclick='removeAllDiscount()'><span class='fa fa-minus-circle'></span> Remove all Discount</button>
  </div>
  <div class='col-md-12' id='item_containter' style='margin-top:10px;'>
    <?=getAllItemsPerOccasion($occ_id)?>
    <input type="hidden" id="catID" value="<?=$occ_id?>" name="">
  </div>
</div>
<?php require 'modals/add_occasion_variation.php'; ?>
<?php require 'modals/add_qntty_category_items.php'; ?>
<?php require 'modals/add_discount_price_2.php'; ?>
<script type="application/javascript">
  function usePercentage(){
    if($("input[name='usePercentages']").is(":checked")){
      $("#perc").css("display","block");
      $("#amnt").css("display","none");
    }else{
      $("#perc").css("display","none");
      $("#amnt").css("display","block");
    }
  }
  function removeDiscount(id){
    var type = 'O';
    $("#removeDiscount"+id).prop("disabled", true);
    $("#removeDiscount"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/delete_discount_category.php",{
      id: id,
      type: type
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Discount Successfully Removed","success");
      }else{
        alertMe("fa fa-close","Aw Snap!","Unable to Remove Data, Please try again","danger");
      }

      $("#item_containter").load(location.href + " #item_containter");
      $("#removeDiscount"+id).prop("disabled", false);
      $("#removeDiscount"+id).html("<span class='fa fa-trash'></span> Delete ");
    });
  }
  function removeAllDiscount(){
    var catID = $("#catID").val();
    var type = 'O';
    $("#removeAll").prop("disabled", true);
    $("#removeAll").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/removeAllDiscount.php", {
      catID: catID,
      type: type
    }, function(data){
        if(data > 0){
          alertMe("fa fa-check-circle","All Good!","Discount Successfully Removed","success");
        }else{
          alertMe("fa fa-close","Aw Snap!","Unable to Delete Data, Please try again","danger");
        }

        $("#item_containter").load(location.href + " #item_containter");
        $("#removeAll").prop("disabled", false);
        $("#removeAll").html("<span class='fa fa-minus-circle'></span> Remove All Discount ");
    })
  }
  function addDiscountPrice(){
    var select_search_all = $("#select_search_all").val();
    if($("input[name='usePercentages']").is(":checked")){
      var discount = $("#perc_discount").val();
      var perc = 1;
    }else{
      var discount = $("#amnt_discount").val();
      var perc = 0;
    }
    var duration_date = $("#duration_date").val();
    var type = 'O';
    $.post("ajax/addDiscountedPriceItems.php", {
      select_search_all: select_search_all,
      discount: discount,
      perc: perc,
      duration_date: duration_date,
      type: type
    }, function(data){
      $("#discount2").modal('hide');
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Discount Successfully Added","success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Something went wrong, Please try again","danger");
      }
       $("#item_containter").load(location.href + " #item_containter");
    })
  }
  function addQntty(val){
    $("#qnttyModal").modal();
    $("#item_id").val(val);
  }
  // function removeDiscount(id){
  //   var type = 'O';
  //   $("#removeDiscount"+id).prop("disabled", true);
  //   $("#removeDiscount"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
  //   $.post("ajax/delete_discount_category.php",{
  //     id: id,
  //     type: type
  //   }, function(data){
  //     if(data > 0){
  //       alertMe("fa fa-check-circle","All Good!","Item Variation Successfully Deleted","success");
  //     }else{
  //       alertMe("fa fa-close","Aw Snap!","Unable to Delete Data, Please try again","danger");
  //     }

  //     $("#item_containter").load(location.href + " #item_containter");
  //     $("#catVariation"+id).prop("disabled", false);
  //     $("#catVariation"+id).html("<span class='fa fa-trash'></span> Delete ");
  //   });
  // }
  function addQuantityItem(){
    var item_id = $("#item_id").val();
    var quantity_val = $("#quantity_val").val();
    var type = 'o';
    $.post("ajax/addItemQntty.php", {
      item_id: item_id,
      quantity_val: quantity_val,
      type: type
    }, function(data){
      $("#qnttyModal").modal('hide');
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Quantity Successfully Added","success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Something went wrong, Please try again","danger");
      }
       $("#item_containter").load(location.href + " #item_containter");
    })
  }
  $(document).ready(function() {
    getCategory();
    $("#occasion_variation_add").on('submit',(function(e) {
    e.preventDefault();
        $("#variation_add").prop("disabled", true);
        $("#variation_add").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.ajax({
        url:"ajax/add_occasion_variation.php",
        type: "POST",
        data:  new FormData(this),
        beforeSend: function(){},
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            $("#occasion_variation").modal('hide');
            if(data == 1){
                alertMe("fa fa-check-circle","All Good!","Item Variation Successfully Added","success");
            }else{
                alert("Error");
            }
            $("#variation_add").prop("disabled", false);
            $("#variation_add").html("<span class='fa fa-check-circle'></span> Save Changes ");
            $("#item_containter").load(location.href + " #item_containter");
        },error: function(){
            alert("Error");
        }
        
    });
        
    }));


  });
  $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  function deleteOccasionVariation(id){
    $("#occasionVariation"+id).prop("disabled", true);
    $("#occasionVariation"+id).html("<span class='fa fa-check-circle'></span> Loading... ");
    $.post("ajax/delete_occasion_variants.php",{
      id: id
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Item Variation Successfully Deleted","success");
      }else{
        alertMe("fa fa-close","Aw Snap!","Unable to Delete Data, Please try again","danger");
      }

      $("#item_containter").load(location.href + " #item_containter");
      $("#occasionVariation"+id).prop("disabled", false);
      $("#occasionVariation"+id).html("<span class='fa fa-trash'></span> Delete ");
    });
  }
  function viewItems(id){
    window.location = 'index.php?page=category-variation-list'+id;
  }
  function addCategory(){
    var action = 'add';
    var name = $("#category_name").val();
    $.post("ajax/categories.php", {
      action: action,
      name: name
    }, function(data){
      $("#category").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Category Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Category Already Exist","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#category_name").val("");
      getCategory();
    })
  }
  function updateCat(id){
    var action = 'edit';
    var name = $("#catName"+id).val();
    $("#updateCat"+id).prop("disabled", true);
    $("#updateCat"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/categories.php", {
      action: action,
      id: id,
      name: name
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function deleteCat(id){
    var action = 'delete';
    $.post("ajax/categories.php", {
      id: id,
      action: action
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully deleted","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function getCategory(){
    $("#Categories").DataTable().destroy();
    $('#Categories').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/category.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"catNAME"
        }
        
    ]   
    });
  }
</script>