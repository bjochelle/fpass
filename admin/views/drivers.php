<h3><i class="fa fa-angle-right"></i> Users </h3>
<div class="row">
	<div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#driver"><span class='fa fa-plus-circle'></span> Add</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      <table id='drivers' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th>NAME</th>
                  <th>STATUS</th>
                  <th>ACTION</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_driver_modal.php'; ?>
<script type="application/javascript">
	$(document).ready( function(){
      getAllDrivers();
  });
  function updateStatus(id,status){
    $("#driver").prop("disabled", true);
    $("#driver").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/updateDriverStatus.php", {
      id: id,
      status: status
    }, function(data){
      $("#driver").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Driver Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      getAllDrivers();
    })
  }
  function addDriver(){
    var fname = $("#fname").val();
    var mname = $("#mname").val();
    var lname = $("#lname").val();

    $("#driverBtn").prop("disabled", true);
    $("#driverBtn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/addDriver.php", {
      fname: fname,
      mname: mname,
      lname: lname
    }, function(data){
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Driver Successfully Added","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#fname").val("");
      $("#lname").val("");
      $("#mname").val("");
      getAllDrivers();
    });
  }
  function getAllDrivers(){
    $("#drivers").DataTable().destroy();
    $('#drivers').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/driver_data.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"catNAME"
        },
        {
            "data":"status"
        },
        {
            "data":"action"
        }
        
    ]   
    });
  }
</script>