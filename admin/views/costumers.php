<h3><i class="fa fa-angle-right"></i> Customers </h3>
<div class="row">

  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' onclick="showmodalCode()"><span class='fa fa-plus-circle'></span> Send Promo Code</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      <table id='costumers' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th></th>
                  <th>#</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>CONTACT #</th>
                  <th>STATUS</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>  
<?php require 'modals/add_promo_code.php'; ?>
<script type="application/javascript">
	$(document).ready( function(){
      getAllCostumers();
  });
  function showmodalCode(){
    var count_checked = $('input[name="checkbox_costumer"]:checked').map(function() {
                                        return this.value;
                                    }).get();
    if(count_checked == ''){
      alertMe("fa fa-exclamation","Aw Snap", "Please Select/Check Customer.","warning");
    }else{
      $("#promocode").modal();
    }
  }
  function addpromoCodes(){
    var count_checked = $('input[name="checkbox_costumer"]:checked').map(function() {
                                        return this.value;
                                    }).get();
    var dateExpired = $("#dateExpired").val();
    var promocode = $("#promocode_val").val();
    var codeAmount = $("#codeAmount").val();
    customer = [];
    if(dateExpired == ''){
      alertMe("fa fa-exclamation","Aw Snap", "Please Select Expiration Date.","warning");
    }else{
      $("#addpromoCodes").prop("disabled", true);
      $("#addpromoCodes").html("<span class='fa fa-spin fa-spinner'></span> Loading");
      $.post("ajax/send_promo_code.php", {
        customer: count_checked,
        dateExpired: dateExpired,
        promocode: promocode,
        codeAmount: codeAmount
      }, function(data){
        $("#promocode").modal('hide');
        if(data > 0){
          alertMe("fa fa-check-circle","All Good","Promo Code Successfully Sent.","success");
        }else{
          alertMe("fa fa-close","Error","Something wrong while saving data.","error");
        }
        
        $("#codeAmount").val("");
        $("#dateExpired").val("");
        $("#addpromoCodes").prop("disabled", false);
        $("#addpromoCodes").html("<span class='fa fa-check-circle'></span> Save Changes");
      })
    }
  }
  function getAllCostumers(){
    $("#costumers").DataTable().destroy();
    $('#costumers').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/costumer_data.php",
        "dataSrc":"data"
    },
    "columns":[
        {
          "mRender": function(data,type,row){
              return "<input type='checkbox' name='checkbox_costumer' value='"+row.catID+"'>";     
          }
        },
        {
            "data":"count"
        },
        {
            "data":"catNAME"
        },
        {
            "data":"email"
        },
        {
            "data":"contact"
        },
        {
            "data":"log_status"
        }
        
    ]   
    });
  }
</script>