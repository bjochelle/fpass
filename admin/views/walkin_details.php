<?php 
$id = $_GET['id'];

$query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_walkin_transaction WHERE w_transaction_id = '$id'"));

$disabled = ($query['status'] > 0)?"disabled":"";

$status = mysql_fetch_array(mysql_query("SELECT * FROM tbl_transaction WHERE transaction_id = '$id'"));


$status = ($status['status'] == 0)?"<span style='color: orange'> PENDING </span>":(($status['status'] == 1)?"<span style='color: blue'> CHECKED OUT </span>":(($status['status'] == 2)?"<span style='color: blue'> ON DELIVERY </span>":(($status['status'] == '3')?"<span style='color: green'> FINISHED </span>":"<span style='color: red'> CANCELLED </span>")));
?>
<style type="text/css">
	.MultiCarousel { 
    float: left; 
    overflow: hidden; 
    padding: 15px; 
    width: 100%; 
    position:relative; }
  .MultiCarousel .MultiCarousel-inner { 
    transition: 1s ease all; 
    float: left; 
  }
  .MultiCarousel .MultiCarousel-inner .item { 
    float: left;
  }
  .MultiCarousel .MultiCarousel-inner .item > div {
   text-align: center; 
   padding:10px;
   margin:10px; 
   background:#f1f1f1; 
   color:#666;}
  .MultiCarousel .leftLst, .MultiCarousel .rightLst { 
    position:absolute; 
    border-radius:50%;
    top:calc(50% - 20px); 
  }
  .MultiCarousel .leftLst { 
    left:0; 
  }
  .MultiCarousel .rightLst { 
    right:0; 
  } 
  .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { 
    pointer-events: none; 
    background:#ccc; 
  }
  .larger{
    width: 20px !important;
    height: 20px !important;
    border-radius: 50% !important;
  }
   #ribbon-container {
        position: absolute;
        top: 8px;
        right: 5px;
        overflow: visible; /* so we can see the pseudo-elements we're going to add to the anchor */
        font-size: 18px; /* font-size and line-height must be equal so we can account for the height of the banner */
        line-height: 18px;
      }
      
      #ribbon-container:before {
        content: "";
        height: 0;
        width: 0;
        display: block;
        position: absolute;
        top: 3px;
        left: 0;
        border-top: 3px solid rgba(0,0,0,.3);
        border-bottom: 29px solid rgba(0,0,0,.3);
        border-right: 24px solid rgba(0,0,0,.3);
        border-left: 34px solid transparent;
      }
      
      #ribbon-container:after { /* This adds the second part of our dropshadow */
        content: "";
        height: 3px;
        background: rgba(0,0,0,.3);
        display: block;
        position: absolute;
        bottom: -3px;
        left: 58px;
        right: 3px;
      }
      
      #ribbon-container a {
          display: block;
          padding: 8px;
          position: relative;
          background: red;
          overflow: visible;
          height: 32px;
          margin-left: 29px;
          color: #fff;
          text-decoration: none;
      }
      
      #ribbon-container a:after { /* this creates the "folded" part of our ribbon */
          content: "";
          height: 0;
          width: 0;
          display: block;
          position: absolute;
          bottom: -16px;
          right: 0;
          border-top: 16px solid #610404;
          border-right: 15px solid transparent;
      }
      
      #ribbon-container a:before { /* this creates the "forked" part of our ribbon */
          content: "";
          height: 0px;
          width: 0;
          display: block;
          position: absolute;
          top: 0;
          left: -29px;
          border-top: 16px solid red;
          border-bottom: 16px solid red;
          border-right: 16px solid transparent;
          border-left: 29px solid transparent;
      }
      hr {
          margin-top: 31px;
          margin-bottom: 20px;
          border: 0;
          border-top: 1px solid #eeeeee;
      }
        /* image thumbnail */
.thumb {
    display: block;
  width: 100%;
  margin: 0;
}

/* Style to article Author */
.by-author {
  font-style: italic;
  line-height: 1.3;
  color: #aab6aa;
}

/* Main Article [Module]
-------------------------------------
* Featured Article Thumbnail
* have a image and a text title.
*/
.featured-article {
  width: 482px;
  height: 350px;
  position: relative;
  margin-bottom: 1em;
}

.featured-article .block-title {
  /* Position & Box Model */
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: 1;
  /* background */
  background: rgba(0,0,0,0.7);
  /* Width/Height */
  padding: .5em;
  width: 100%;
  /* Text color */
  color: #fff;
}

.featured-article .block-title h2 {
  margin: 0;
}

/* Featured Articles List [BS3]
--------------------------------------------
* show the last 3 articles post
*/

.main-list {
  padding-left: .5em;
}

.main-list .media {
  padding-bottom: 1.1em;
  border-bottom: 1px solid #e8e8e8;
}
</style>
<h3><i class="fa fa-angle-right"></i> View Walk-in Transactions Details </h3>
<div class="row">
  <div class="col-md-12">
      <span style="font-size: 24px;">STATUS: <?=$status?></span>
      <button class="btn btn-success btn-sm pull-right" onclick='printReceipt(<?=$id?>)'><span class="fa fa-print"></span> Print Receipt</button>
  </div>
	<input type="hidden" id="headerID" name="" value="<?=$id?>">
	<div class="col-md-6">
      <div class="input-group">
        <span class="input-group-addon">Reference #: </span>
        <input type="text" class="form-control" name="refnum" id='refnum' value="<?=$query['reference_num']?>" readonly>
      </div>
    </div>
    <div class="col-md-6">
      <div class="input-group">
        <span class="input-group-addon">Customer Name: </span>
        <input type="text" class="form-control" name="cust_name" id='cust_name' value="<?=$query['costumer']?>">
      </div>
    </div>
    <div class="col-md-6" style="margin-top: 10px">
      <div class="input-group">
        <span class="input-group-addon">Delivery Date: </span>
        <input type="date" class="form-control" name="delivery_date" id='delivery_date' value="<?=$query['delivery_date']?>">
      </div>
    </div>
    <div class="col-md-6" style="margin-top: 10px">
      <div class="input-group">
        <span class="input-group-addon">Delivery Time: </span>
        <input type="time" class="form-control" name="delivery_time" id='delivery_time' value="<?=$query['delivery_time']?>">
      </div>
    </div>
    <div class="col-md-6" style="margin-top: 10px">
      <div class="input-group">
        <span class="input-group-addon">Contact #: </span>
        <input type="number" class="form-control" name="contact_no" id='contact_no' value="<?=$query['contact_no']?>">
      </div>
    </div>
    <div class="col-md-6" style="margin-top: 10px">
      <div class="input-group">
        <span class="input-group-addon">Delivery Address: </span>
        <textarea style="resize: none;" rows="3" class="form-control" id="delivery_address"><?=$query['address_name']?></textarea>
      </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px">
      <div class="input-group">
        <span class="input-group-addon">Message: </span>
        <textarea rows="3" style="resize: none;" class="form-control" id="message"><?=$query['message']?></textarea>
      </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px">
      <button class="btn btn-primary btn-sm pull-right" id="btn_addHeader" onclick='updateHeader()'><span class="fa fa-plus-circle"></span> Update Header </button>
    </div>
      <div class="col-md-12" style="border: 1px solid gray; margin-top: 10px"></div>
        <div class="col-md-4" style="margin-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Options: </span>
            <select <?=$disabled?> class="form-control" id="options" onchange="getOptionslist()">
              <option value=""> &mdash; Please Select &mdash; </option>
              <option value="t"> Types </option>
              <option value="c"> Categories </option>
              <option value="ca"> Cakes </option>
            </select>
          </div>
        </div>
        <div class="col-md-4" id="list" style="margin-top: 10px">
          
        </div>
        <div class="col-md-4" style="margin-top: 10px">
          <button class="btn btn-primary" id="btn_view" onclick='viewList()'><span class="fa fa-gear"></span> Generate </button>
          <button class="btn btn-success" id="btn_cart" onclick='view_cart()'><span class="fa fa-eye"></span> View Cart </button>
        </div>
        <div class="col-md-12" id="list_div" style="margin-top: 10px;">
          
        </div>
</div>
<div id="receipt_modal" class="modal fade" role="dialog" data-backdrop='static'>
  <div class="modal-dialog modal-lg" style="width: 1000px">

    <div class="modal-content">
      <div class="modal-header">
      </div>
      
      <div class="modal-body">
        <div id='loader1' style='text-align: center;font-size: 2.5rem;'><span class='fa fa-spin fa-spinner'></span> Loading Data, Please Wait </div>
        <iframe id="JOframe1" name="JOframe1" width="100%" height="500px" style="margin-bottom:-10px;" frameborder="0">
        </iframe>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
             <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" onclick="printIframe('JOframe1');"><span class="fa fa-print"></span> Print</button>
                       <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </span>
        </div>
      </div>
  </div>
</div>
</div>
<?php require 'modals/view_saved_walkin_cart.php';?>
<script type="text/javascript">
  $(document).ready( function(){
    $('#JOframe1').on('load', function () {
      $('#loader1').hide();
    });
  });
  function printReceipt(id){
    $("#receipt_modal").modal('show');
    var url = 'views/print/print_receipt_walkin.php?id='+id;
      $('#JOframe1').attr('src', url);
  }
	function addtocartAddon(id,type,price){
    var headerID = $("#headerID").val();
    var quantity = $("#addons_quantity"+id).val();
    $("#btn_addCartAo"+id).prop("disabled", true);
    $("#btn_addCartAo"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/addtowalkincartAddon.php", {
      headerID: headerID,
      quantity: quantity,
      id: id,
      type: type,
      price: price
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Successfully Added to cart","success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Unable to save data","error");
      }
      $("#btn_addCartAo"+id).prop("disabled", false);
      $("#btn_addCartAo"+id).html("<span class='glyphicon glyphicon-shopping-cart'></span> Add to Cart");
    });
  }
  function addtocart(id,type,price){
    var headerID = $("#headerID").val();
    var quantity = $("#quantity"+id).val();
    $("#btn_addCart"+id).prop("disabled", true);
    $("#btn_addCart"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    
    $.post("ajax/addtowalkincart.php", {
      headerID: headerID,
      quantity: quantity,
      id: id,
      type: type,
      price: price
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Successfully Added to cart","success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Unable to save data","error");
      }

      $("#btn_addCart"+id).prop("disabled", false);
      $("#btn_addCart"+id).html("<span class='glyphicon glyphicon-shopping-cart'></span> Add to Cart");
    });
  }
	function view_cart(){
	    var headerID = $("#headerID").val();
	    $.post("ajax/getWalkinCart.php", {
	      headerID: headerID
	    }, function(data){
	      $("#view_saved_cart").modal();
	      $("#cart_wrapper").html(data);
	    })
	  }
	function viewList(){
	    var options = $("#options").val();
	    var listID = $("#listID").val();
	    $.post("ajax/getAllOptionList.php", {
	      options: options,
	      listID: listID
	    }, function(data){
	      $("#list_div").html(data);
	    })
	  }
	function getOptionslist(){
	    var options = $("#options").val();
	    $.post("ajax/getOptionLists.php", {
	      options: options
	    }, function(data){
	      $("#list").html(data);
	    });
	  }
	function updateHeader(){
		var refnum = $("#refnum").val();
	    var cust_name = $("#cust_name").val();
	    var message = $("#message").val();
	    var delivery_date = $("#delivery_date").val();
	    var delivery_time = $("#delivery_time").val();
	    var contact_no = $("#contact_no").val();
	    var delivery_address = $("#delivery_address").val();
	    var headerID = $("#headerID").val();
	    $("#btn_addHeader").prop("disabled", true);
	    $("#btn_addHeader").html("<span class='fa fa-spin fa-spinner'></span> Loading");
	      $.post("ajax/walkin_updateHeader.php", {
	        refnum: refnum,
	        cust_name: cust_name,
	        message: message,
	        delivery_date: delivery_date,
	        delivery_time: delivery_time,
	        contact_no: contact_no,
	        delivery_address: delivery_address,
	        headerID: headerID
	      }, function(data){
	        if(data > 0){
	          alertMe("fa fa-check-circle","All Good!","Successfully Updated","success");
	        }else{
	          alertMe("fa fa-exclamation","Aw Snap!","Unable to save data","error");  
	        }
	        $("#btn_addHeader").prop("disabled", false);
	        $("#btn_addHeader").html("<span class='fa fa-check-circle'></span> Add Header");
	      });
	    
	}
	function checkout_cart(){
    var headerID = $("#headerID").val();
    $.post("ajax/checkout_walkinCart.php", {
      headerID: headerID
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Transaction Successfully Finished",
            type: "success"
          }, function(){
            window.location = 'index.php?page=walkin-transaction';
          });
      }else{
        swal("Something went wrong, Please Contact administrator");
      }
    });
  }
  function deleteItemCart(id){
    $("#deleteItemCart"+id).prop("disabled", true);
      $("#deleteItemCart"+id).html("<span class='glyphicon glyphicon-refresh'></span> Loading");
      $.post("ajax/deleteIteminCartWalkin.php", {
        id: id
      }, function(data){
        if(data > 0){
          swal({
            title: "All Good!",
            text: "Item Successfully Deleted",
            type: "success"
          }, function(){
            view_cart();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      });
  }
  function update_qntty(id,action){
      $.post("ajax/update_quantity_walkin.php", {
        action: action,
        id: id
      }, function(data){
        if(data > 0){
          swal({
            title: "All Good!",
            text: "Quantity Successfully Updated",
            type: "success"
          }, function(){
            swal.close();
          });
          $("#cart_qntty_w"+id).val(data);
        }else if(data == 'EL'){
          swal({
            title: "Aw Snap!",
            text: "Exceeded to the remaining quantity",
            type: "warning"
          }, function(){
            swal.close();
          });
        }else if(data == 'NLO'){
          swal({
            title: "Aw Snap!",
            text: "Must not less than 1",
            type: "warning"
          }, function(){
            swal.close();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
        
      })
    
  }
  function addonminusQntty(id){
    var quantity = $("#addons_quantity"+id).val();
    var qntty = quantity - 1;
    if(qntty < 1){
      var final_qntty = 1;
    }else{
      var final_qntty = qntty;
    }
    $("#addons_quantity"+id).val(final_qntty);
  }
  function addonaddQntty(id){
    var quantity = $("#addons_quantity"+id).val();
    var qntty = parseFloat(quantity) + 1;
   
    $("#addons_quantity"+id).val(qntty);
  }
  function minusQntty(id){
    var quantity = $("#quantity"+id).val();
    var qntty = quantity - 1;
    if(qntty < 1){
      var final_qntty = 1;
    }else{
      var final_qntty = qntty;
    }
    $("#quantity"+id).val(final_qntty);
  }
  function addQntty(id){
    var quantity = $("#quantity"+id).val();
    var qntty = parseFloat(quantity) + 1;
   
    $("#quantity"+id).val(qntty);
  }
</script>