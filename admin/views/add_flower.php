<h3><a href="#" onclick="window.location='index.php?page=flower-list'">Flowers</a> <i class="fa fa-angle-right"></i> Add Flowers </h3>
<div class="row">
  <form id="addflowerList" method="POST" action="" enctype="multipart/form-data">
  <div class='col-md-12' style='margin-top:10px;'>
    <center>
    <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="assets/images/img_upload.png" style="object-fit: cover;width: 400px;height: 200px;">

    <div class="image-upload" style="margin-top: 22px;margin-left: 45px;">
    <input type="file" name="avatar" style='visibility: hidden' required id="files" class="btn-inputfile share" />
    <label for="files" class="btn default" style="font-size: 16px;margin-top:-40px;margin-right: 30px;"><i class="fa fa-file-image-o"></i> CHOOSE IMAGE </label>
    </div>
    </center>
  </div>
  <br>
  <div class="col-md-6">
    <div class="input-group">
      <span class="input-group-addon"> Name: </span>
      <input type="text" name="flowerName" id='flowerName' class="form-control">
    </div>
  </div>
  <br>
  <div class="col-md-6">
    <div class="input-group">
      <span class="input-group-addon"> Category: </span>
      <select class="form-control" id="categoryID" name='categoryID'>
        <?=getCategories()?>
      </select>
    </div>
  </div>
  <br>
  <!-- <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Size: </span>
      <select class="form-control" id="sizeID" name='sizeID'>
        <?=getSizes()?>
      </select>
    </div>
  </div> -->
  <br>
  <!-- <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Color: </span>
      <input type="color" id="colorVal" name="colorVal" class="form-control">
    </div>
  </div> -->
  <br>
  <div class="col-md-12" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Description: </span>
      <textarea rows="2" id="desc" class="form-control" name='desc' style="resize: none;"></textarea>
    </div>
  </div>
  <div class="col-md-12" style="margin-top: 10px">
    <button type="submit" id='addFlower' class="btn btn-sm btn-primary pull-right"><span class="fa fa-check-circle"></span> Save Changes</button>
  </div>
  </form>
</div>
<?php require 'modals/add_category.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    $("#addflowerList").on('submit',(function(e) {
        e.preventDefault();
            $("#addFlower").prop("disabled", true);
            $("#addFlower").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_flower.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                if(data > 0){
                    alertMe("fa fa-check-circle","All Good!","Successfully Added","success");
                    $("#addFlower").prop("disabled", false);
                    $("#addFlower").html("<span class='fa fa-check-circle'></span> Continue ");

                    $("#files").val("");
                    $("#flowerName").val("");
                    $("#categoryID").val("");
                    $("#sizeID").val("");
                    $('#colorVal').val("");
                    $('#desc').val("");
                }else{
                    alert("Error");
                }
                
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));
  });
  $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

    
  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
</script>