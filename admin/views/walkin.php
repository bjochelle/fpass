<h3><i class="fa fa-angle-right"></i> Walkin Transactions </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' onclick='window.location="index.php?page=add-walkin-transaction"'><span class='fa fa-plus-circle'></span> Add </button>
  </div>
	<div class='col-md-12' style='margin-top:10px;'>
      <table id='Categories' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th style="width: 56px;"></th>
                  <th>REFERENCE NUMBER</th>
                  <th>CUSTOMER</th>
                  <th>SCHEDULE</th>
                  <th>TOTAL PRICE</th>
                  <th>STATUS</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  	</div>
</div>
<div id="receipt_modal" class="modal fade" role="dialog" data-backdrop='static'>
  <div class="modal-dialog modal-lg" style="width: 1000px">

    <div class="modal-content">
      <div class="modal-header">
      </div>
      
      <div class="modal-body">
        <div id='loader1' style='text-align: center;font-size: 2.5rem;'><span class='fa fa-spin fa-spinner'></span> Loading Data, Please Wait </div>
        <iframe id="JOframe1" name="JOframe1" width="100%" height="500px" style="margin-bottom:-10px;" frameborder="0">
        </iframe>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
             <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" onclick="printIframe('JOframe1');"><span class="fa fa-print"></span> Print</button>
                       <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </span>
        </div>
      </div>
  </div>
</div>
</div>
<?php require 'modals/delivery_driver_modal.php';?>
<script type="application/javascript">
  $(document).ready( function(){
    getCategory();
    $('#JOframe1').on('load', function () {
      $('#loader1').hide();
    });
  });
  function finishTrans(id){
    var type = 'w';
    $.post("ajax/finish_transaction.php", {
      type: type,
      id: id
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Transactions Successfully Finished","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function printReceipt(id){
    $("#receipt_modal").modal('show');
    var url = 'views/print/print_receipt_walkin.php?id='+id;
      $('#JOframe1').attr('src', url);
  }
  function addDrivertoTrans(){

    var driverID = $("#driverID").val();
    var transID = $("#transID").val();
    var type = 'w';
    $.post("ajax/addDrivertoTrans.php", {
      driverID: driverID,
      transID: transID,
      type: type
    }, function(data){
      $("#delivery_driver").modal('hide');
      if(data == 1){
            alertMe("fa fa-check-circle","All Good","Driver Successfully Assigned","Success");
          }else{
            alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
          }
          getCategory();
    })
  }
  function onDelivery(transid){
    $("#delivery_driver").modal();
    $("#transID").val(transid);
  }
  function viewwalkinItems(id){
    window.location = 'index.php?page=view-walkin-details&id='+id;
  }
  function getCategory(){
      $("#Categories").DataTable().destroy();
      $('#Categories').dataTable({
      "processing":true,
      "ajax":{
          "url":"../admin/ajax/datatables/walkin_trans.php",
          "dataSrc":"data"
      },
      "columns":[
          {
              "data":"count"
          },
          {
              "data":"action"
          },
          {
              "data":"refNum"
          },
          {
              "data":"customer"
          },
          {
              "data":"sched"
          },
          {
              "data":"total"
          },
          {
              "data":"status"
          }
          
      ]   
      });
    }
</script>