<?php 
$msg = $_GET['id'];
$receiver_id = mysql_fetch_array(mysql_query("SELECT sender_id FROM tbl_chats WHERE chat_id = '$msg'"));
$rcvID = $receiver_id[0];
$name = getUser($rcvID);
?>
<style type="text/css">
        .cg :hover {
      background-color: #80868e;
  }

*,
*::before,
*::after {
  margin: 0;
  border: 0;
  padding: 0;
  word-wrap: break-word;
  box-sizing: border-box;
}

.message-sent,
.message-received {
  clear: both;
}
.message-sent::before,
.message-received::before,
.message-sent::after,
.message-received::after {
  content: '';
  display: table;
}

[class^='grid-'] {
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}
[class^='grid-'] {
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
}
.grid-message >[class^='col-'] {
  margin-top: 1em;
  margin-right: 1em;
}
.grid-message >[class^='col-']:nth-child(-n + 1) {
  margin-top: 0;
}
.grid-message >[class^='col-']:nth-child(1n) {
  margin-right: 0;
}
.col-message-sent {
  margin-left: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-received {
  margin-right: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-sent,
.col-message-received {
  width: calc(91.66666667% - 0.08235677em);
}
.message-sent,
.message-received {
  margin-top: 0.0625em;
  margin-bottom: 0.0625em;
  padding: 0.25em 1em;
}
.message-sent p,
.message-received p {
  margin: 0;
  line-height: 1.5;
}
.message-sent {
  float: right;
  color: white;
  background-color: gray;/*dodgerblue;*/
  border-radius: 1em 0.25em 0.25em 1em;
}
.message-sent:first-child {
  border-radius: 1em 1em 0.25em 1em;
}
.message-sent:last-child {
  border-radius: 1em 0.25em 1em 1em;
}
.message-sent:only-child {
  border-radius: 1em;
}
.message-received {
  float: left;
  color: black;
  background-color: #c5c5c5;
  border-radius: 0.25em 1em 1em 0.25em;
}
.message-received:first-child {
  border-radius: 1em 1em 1em 0.25em;
}
.message-received:last-child {
  border-radius: 0.25em 1em 1em 1em;
}
.message-received:only-child {
  border-radius: 1em;
}
.col-message-sent {
  margin-top: 0.25em !important;
}
.col-message-received {
  margin-top: 0.25em !important;
}
.message {
  min-height: 53.33203125em;
  max-width: 30em; /* !!!!!!!!!! COMMENT OUT THIS LINE TO MAKE IT FULL WIDTH !!!!!!!!!! */
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}
.btn_hover:hover{
  background-color: #80868e;
  color: white;
}
</style>
<h3><a href="#" onclick="window.location='index.php?page=dashboard'">Dashboard</a> <i class="fa fa-angle-right"></i> Messages </h3>
<div class="row">
<div class='row' style="height: 445px;margin-top: 10px;" >
  <div class="col-md-4">
    <div class="" style="padding:10px;border: 1px solid #ccc;">
        Online Users
    </div>
    <div class="" style="height: 400px;padding: 0px;border: 1px solid #ccc;">
        <div class="box box-warning direct-chat direct-chat-warning" style="height: 100%;border-top: 0px;">
            <div class="box-body" style="height: 100%;">
              <div class="direct-chat-messages" style="height: 100%;padding: 0px; overflow-y: scroll;">
                  <ul class="products-list product-list-in-box" style="">
                      <?php 
                      $getUserstoChat = mysql_query("SELECT * FROM tbl_users WHERE category = 1");
                    
                      while($fetchUsers = mysql_fetch_array($getUserstoChat)){
                          $userid = $fetchUsers['user_id'];
                          
                        $doctor_name = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));

                      ?>
                      <li class="item cg" style="padding:0px;border-bottom: 1px solid #ddd; list-style: none;" onclick="user_list('<?php echo $userid ?>','<?php echo $doctor_name[0] ?>')">
                        <div class="product-info" id='name<?php echo $userid ?>' style="padding: 10px;margin-left: 0px;">
                          <a href="javascript:void(0)" class="product-title"><?php echo $doctor_name[0]; ?></a>
                          <input type="hidden" id="cgID_checker" value="">

                          <?php if($fetchUsers['login_status'] == 1){ ?>
                          <small class="pull-right" id="labelofonlineusers">
                            <div style="height: 10px; width: 10px; background-color: #00ff00; border-radius: 50%;"></div>
                          </small>
                          <?php } else{ ?>

                          <?php } ?>
                          <span class="product-description" style="margin-top: -5px;">
                            </span>
                        </div>
                      </li>
                      <?php } ?>
                  </ul>
                </div>
            </div>
        </div>
      </div>
   </div>
  <div class="col-md-8" style='margin-left: -30px;'>
  <div class="" style="height:409px;padding: 0px;border: 1px solid #ccc;">
      <!-- DIRECT CHAT -->
      <div class="box box-warning direct-chat direct-chat-warning" style="height: 100%;border-top: 0px;">
        <!-- /.box-header -->
        <div class="box-body" style="height: 100%;">
          <div class='col-md-12' style="background-color: #c5c5c5;"><center><label id='names'></label></center></div>
          <!-- Conversations are loaded here -->
          <div class="grid-message" style="overflow-x: auto;height: 90%;padding: 10px;" id="msg">

          </div>
          <!--/.direct-chat-messages-->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="input-group">
              <input type="hidden" id="sender_id" value="<?php echo $user_id;?>">
              <input type="hidden" id="receiver_id" value='<?php echo $rcvID; ?>'>
              <input type="hidden" id="name" value='<?php echo $name; ?>'>
             
                  
    
                
        
              <input type="text" id="chatContent" row="3" name="message"  autocomplete="off" placeholder="Type a message ..." class="form-control">
              <span class="input-group-btn">  
                <label for="files" class="btn default" style="height: 34px;border: 1px solid #c5c5c5;"><i class="fa fa-file-image-o" style="margin-top: 4px;"></i></label>
                <button type="button" class="btn btn-primary btn-flat" id="send_btn" onclick="sendMsg()"><i class="fa fa-send"></i> Send</button>
              </span>
            </div>
        <!-- /.box-footer-->
      </div>
  </div>
</div>
</div>
</div> 
<form id="attachImg" method="POST" action="" enctype="multipart/form-data">
      <div class="modal fade" id="attachImg_modal" tabindex="-1" role="dialog" aria-labelledby="attachImg_modalLabel" data-backdrop='static'>
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title"><span class='fa fa-plus-circle'></span> Upload Image </h4>
                  </div>
                  <div class="modal-body">
                      <div class='row'>
                          <div class='col-md-12' style='text-align: center'>
                              <img id="img_wrap" alt='Recent 2x2 Picture' class="image-wrap previewImage01" style="object-fit: contain;width: 100%;height:200px;">
                              <input style="display: none;" type="file" name="avatar" id="files" class="btn-inputfile share" accept="image/x-png,image/gif,image/jpeg" />
                              <input type="hidden" name='attached_receiver' id="attached_receiver" value='<?php echo $receiver[0]; ?>'>
                          </div>
                          <div class='col-md-12' style="margin-top: 10px">
                             
                                  <div class="input-group">
                                      <span class="input-group-addon">
                                        Description:
                                      </span>
                                     <textarea class="form-control" style="resize: none;" rows="3" name='attach_img_desc' id="attach_img_desc" placeholder="Write something here..."></textarea>
                                  </div>
                              
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <span class="btn-group">
                <button class="btn btn-primary btn-sm" id="btn_attach_img" type="submit"><span class="fa fa-upload"></span> Upload</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
              </span>
                  </div>
              </div>
          </div>
      </div>
      </form>
      <div class="modal fade" id="view_attachImg" tabindex="-1" role="dialog" aria-labelledby="view_attachImgLabel" data-backdrop='static'>
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title"><span class='fa fa-eye'></span> Attached Image </h4>
                  </div>
                  <div class="modal-body">
                      <div class='row'>
                          <div class='col-md-12' style='text-align: center'>
                              <div id="img_wwrap">
                                
                              </div>
                              <input type="hidden" name='attached_receiver' id="attached_receiver" value='<?php echo $receiver[0]; ?>'>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <span class="btn-group">
                <!-- <button class="btn btn-primary btn-sm" id="del_attach_img" type="button"><span class="fa fa-trash"></span> Delete</button> -->
                <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
              </span>
                  </div>
              </div>
          </div>
      </div>
<script type="application/javascript">
  $(document).ready( function(){
    var receiver_id = $("#receiver_id").val();
    var name = $("#name").val();
    $("#names").html(name);
      loadChat(receiver_id);



      $("#attachImg").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"ajax/attach_img_chat_admin.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
              $("#attachImg_modal").modal('hide');
             loadChat(data);
            },
             error: function() 
         {
          failedAlert();
         }           
       });
        
    }));
  });

    $(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);
      
    }, false);
    if (file) {
      reader.readAsDataURL(file);
      var files = $("#files").val();
      $("#attach_img_upload").val(files);
      $("#attachImg_modal").modal();
    }
  }
  function viewImg(message_id){
    $("#view_attachImg").modal();
    $.post("ajax/get_attached_img.php", {
      message_id: message_id
    }, function(data){
        $("#img_wwrap").html(data);
    })
  }
 $(document).keypress(function(e) {
      if(e.which == 13) {
          sendMsg();
      }
      var receiver_id = $("#receiver_id").val();
      loadChat(receiver_id);
  });
//  function markedAllMessage(){
//   var receiver_id = $("#receiver_id").val();
//   var sender_id = $("#sender_id").val();
//   $.post("ajax/mark_all_message.php", {
//     receiver_id: receiver_id,
//     sender_id: sender_id
//   }, function(data){
    
//   });

//  }
 function scrolling(){
    var objDiv = document.getElementById("msg");
    objDiv.scrollTop = objDiv.scrollHeight;
  }
function user_list(id,name){
    $("#names").html(name);
    $("#msg").html("<h4 class='col-md-6 col-md-offset-3' style='text-align: center; margin-top: 180px;'><span class='fa fa-spinner'></span> Loading Messages...</h4>");
    $("#receiver_id").val(id);
    $("#attached_receiver").val(id);
    setTimeout(scrolling, 0);
    loadChat(id);
  }
  function loadChat(r_id){
   // var interval = setInterval( function(){
      viewMsg(r_id);
      setTimeout(scrolling, 500);
    //}, 10000);
  }
  function viewMsg(rec_id){
    var sender_id = $("#sender_id").val();
    $.post('ajax/admin_getMessage.php', {
      rec_id: rec_id,
      sender_id:sender_id
    },
    function(data){

      var msg = JSON.parse(data);
      $("#msg").html(msg.msg_content);
    });
  }
  function sendMsg(){
    var sender_id = $("#sender_id").val();
    var receiver_id = $("#receiver_id").val();
    var chatContent = $("#chatContent").val();
    $("#send_btn").prop("disabled", true);
    $("#send_btn").html("<span class='fa fa-spin fa-spinner'></span>");
    if(sender_id == '' || receiver_id == '' || chatContent == ''){
      alert('Failed');
    }else{
      $.post('ajax/send_message.php', {
      sender_id: sender_id,
      receiver_id: receiver_id,
      chatContent: chatContent
    }, function(data){
      if(data != 0){
        $("#chatContent").val("");
        loadChat(data);
      }else{
        alert('Failed');
      }
      setTimeout(scrolling, 10);
      $("#send_btn").prop("disabled", false);
      $("#send_btn").html("<span class=''></span> Send");
    });
    }
  }
  function updateScroll(){
       var element = document.getElementById("msg");
      element.scrollTop = element.scrollHeight;
  }
</script>