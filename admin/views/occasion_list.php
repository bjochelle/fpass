<h3><i class="fa fa-angle-right"></i> Categories </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#occasion"><span class='fa fa-plus-circle'></span> Add</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      <table id='Categories' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th></th>
                  <th>NAME</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_occasion_modal.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getCategory();
  });
  function viewItems(id){
    window.location = 'index.php?page=category-variation-list&occ_id='+id;
  }
  function addCategory(){
    var action = 'add';
    var name = $("#occasion_name").val();
    $.post("ajax/occasions.php", {
      action: action,
      name: name
    }, function(data){
      $("#occasion").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Occasion Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Occasion Already Exist","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#occasion_name").val("");
      getCategory();
    })
  }
  function updateCat(id){
    var action = 'edit';
    var name = $("#occasion_name"+id).val();
    $("#updateCat"+id).prop("disabled", true);
    $("#updateCat"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/occasions.php", {
      action: action,
      id: id,
      name: name
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Occasion Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function deleteCat(id){
    var action = 'delete';
    $.post("ajax/occasions.php", {
      id: id,
      action: action
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Occasion Successfully deleted","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function getCategory(){
    $("#Categories").DataTable().destroy();
    $('#Categories').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/occasion.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"catNAME"
        }
        
    ]   
    });
  }
</script>