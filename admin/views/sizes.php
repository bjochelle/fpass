<h3><i class="fa fa-angle-right"></i> Sizes </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#sizesModal"><span class='fa fa-plus-circle'></span> Add</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      <table id='sizes' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th></th>
                  <th>Size</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_size.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getCategory();
  });
  function addSizes(){
    var action = 'add';
    var name = $("#size_val").val();
    $.post("ajax/sizes.php", {
      action: action,
      name: name
    }, function(data){
      $("#sizesModal").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Size Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Size Already Exist","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#size_val").val("");
      getCategory();
    })
  }
  function updateCat(id){
    var action = 'edit';
    var name = $("#sizeName"+id).val();
    $("#updateCat"+id).prop("disabled", true);
    $("#updateCat"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/sizes.php", {
      action: action,
      id: id,
      name: name
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function deleteCat(id){
    var action = 'delete';
    $.post("ajax/sizes.php", {
      id: id,
      action: action
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully deleted","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function getCategory(){
    $("#sizes").DataTable().destroy();
    $('#sizes').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/size_list.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"sizeName"
        }
        
    ]   
    });
  }
</script>