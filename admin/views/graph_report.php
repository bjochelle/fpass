<?php 

?>
<h3><i class="fa fa-angle-right"></i> Sales Graph </h3>
<div class="row">
	<div class='col-md-12'>
		<div class="col-md-6" >
			<div class="input-group">
				<span class="input-group-addon"> Graph Type:</span>
				<select class="form-control" id="graphtype" onchange='typegraph()'>
					<option value=""> &mdash; Please Select &mdash; </option>
					<option value="d"> Daily Sales </option>
					<option value="m"> Monthly Sales </option>
				</select>
			</div>
		</div>
		
		
		<div class="col-md-6" style="margin-top: 5px">
			<button class="btn btn-primary" id="btn-rprt" onclick='genreport()'><span class="fa fa-gear"></span> Generate</button>
		</div>

		<div id="div_date" style="display: none;">
			<div class="col-md-5" style="margin-top: 10px">
				<div class="input-group">
					<span class="input-group-addon"> Date From:</span>
					<input type="date" id="date_from" class="form-control" name="">
				</div>
			</div>

			<div class="col-md-5" style="margin-top: 10px">
				<div class="input-group">
					<span class="input-group-addon"> Date to:</span>
					<input type="date" id="date_to" class="form-control" name="">
				</div>
			</div>
		</div>
        <div class="col-md-12" id='chart' style="margin-top: 10px"></div>
    </div>
</div>
<script src="assets/js/highcharts1.js"></script>
<script src="assets/js/highcharts-more.js"></script>
<script type="application/javascript">
	$(document).ready( function(){
	    //bot_chart();
	});
	function typegraph(){
		if($("#graphtype").val() == 'd'){
			$("#div_date").css("display","block");
		}else{
			$("#div_date").css("display","none");
		}
	}
	function genreport(){
		var graphtype = $("#graphtype").val();
		var date_from = $("#date_from").val();
		var date_to = $("#date_to").val();
		var text = (graphtype == 'd')? "TOTAL DAILY SALES" : "TOTAL MONTHLY SALES";
	  $.getJSON('ajax/chart.php?graphtype='+graphtype+'&datefrom='+date_from+'&dateto='+date_to+'', function(data){
	    Highcharts.chart("chart", {
	      chart: {
	        type: 'line'
	      },
	      title: {
	        text: text
	      },
	      xAxis: {
	        type: 'category',
	        title: {
	          text: text
	        }
	      },
	      yAxis: {
	        title: {
	          text: 'SALES'
	        }
	      },
	      plotOptions: {
	          column: {
	            dataLabels: {
	              enabled: true
	            },
	            enableMouseTracking: true
	          },
	          series: {
	            connectNulls: true
	          }
	      },

	      tooltip: {
	          headerFormat: '',
	          pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
	      },

	     series: data['series']
	    });

	  });
	}
</script>