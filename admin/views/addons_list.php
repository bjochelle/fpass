<h3><i class="fa fa-angle-right"></i> Add-ons </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#addons_variants"><span class='fa fa-plus-circle'></span> Add</button>
  </div>
  <div class='col-md-12' id='item_containter' style='margin-top:10px;'>
      <?=getAllAddons()?>
  </div>
</div>
<?php require 'modals/addons_modal.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getCategory();

    $("#addons_variation_add").on('submit',(function(e) {
    e.preventDefault();
        $("#variation_add").prop("disabled", true);
        $("#variation_add").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.ajax({
        url:"ajax/add_addons_variation.php",
        type: "POST",
        data:  new FormData(this),
        beforeSend: function(){},
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            $("#addons_variants").modal('hide');
            if(data == 1){
                alertMe("fa fa-check-circle","All Good!","Addons Variation Successfully Added","success");
            }else{
                alert("Error");
            }
            $("#variation_add").prop("disabled", false);
            $("#variation_add").html("<span class='fa fa-check-circle'></span> Save Changes ");
            $("#item_containter").load(location.href + " #item_containter");
        },error: function(){
            alert("Error");
        }
        
    });
        
    }));


  });
  $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  function deleteaddonsVariation(id){
    $("#addonsVariation"+id).prop("disabled", true);
    $("#addonsVariation"+id).html("<span class='fa fa-check-circle'></span> Loading... ");
    $.post("ajax/delete_addons_variants.php",{
      id: id
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Item Variation Successfully Deleted","success");
      }else{
        alertMe("fa fa-close","Aw Snap!","Unable to Delete Data, Please try again","danger");
      }

      $("#item_containter").load(location.href + " #item_containter");
      $("#addonsVariation"+id).prop("disabled", false);
      $("#addonsVariation"+id).html("<span class='fa fa-trash'></span> Delete ");
    });
  }
  function viewItems(id){
    window.location = 'index.php?page=category-variation-list'+id;
  }
  function deleteCatVariation(id){
    $("#catVariation"+id).prop("disabled", true);
    $("#catVariation"+id).html("<span class='fa fa-check-circle'></span> Loading... ");
    $.post("ajax/delete_category_variants.php",{
      id: id
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Item Variation Successfully Deleted","success");
      }else{
        alertMe("fa fa-close","Aw Snap!","Unable to Delete Data, Please try again","danger");
      }

      $("#item_containter").load(location.href + " #item_containter");
      $("#catVariation"+id).prop("disabled", false);
      $("#catVariation"+id).html("<span class='fa fa-trash'></span> Delete ");
    });
  }
  function addCategory(){
    var action = 'add';
    var name = $("#category_name").val();
    $.post("ajax/categories.php", {
      action: action,
      name: name
    }, function(data){
      $("#category").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Category Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Category Already Exist","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#category_name").val("");
      getCategory();
    })
  }
  function updateCat(id){
    var action = 'edit';
    var name = $("#catName"+id).val();
    $("#updateCat"+id).prop("disabled", true);
    $("#updateCat"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/categories.php", {
      action: action,
      id: id,
      name: name
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function deleteCat(id){
    var action = 'delete';
    $.post("ajax/categories.php", {
      id: id,
      action: action
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully deleted","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function getCategory(){
    $("#Categories").DataTable().destroy();
    $('#Categories').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/category.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"catNAME"
        }
        
    ]   
    });
  }
</script>