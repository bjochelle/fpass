<link href="assets/css/style.css" rel="stylesheet">
<h3><i class="fa fa-angle-right"></i> Flowers </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' onclick='window.location="index.php?page=add-flower"'><span class='fa fa-plus-circle'></span> Add</button>
       <button class='btn btn-sm btn-danger pull-right' id='deleteFlower' onclick='deleteFlower()'><span class='fa fa-trash'></span> Delete</button>
  </div>
  <div class="col-md-6">
    <div class="input-group">
        <span class="input-group-addon"> Category: </span>
        <select class="form-control" id='category_id' onchange="getFlower_category()">
          <?=getCategories()?>
        </select>
      </div>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      
      <table id='flowers' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th></th>
                  <th>#</th>
                  <th></th>
                  <th>IMAGE</th>
                  <th>NAME</th>
                  <th>DESCRIPTION</th>
                  <th>SIZE</th>
                  <!-- <th>COLOR</th> -->
                  <th>QUANTITY</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_qntty.php'; ?>
<?php require 'modals/add_sizes.php'; ?>
<?php require 'modals/view_image.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getFlower_category();
    
  });
  function showImage(img, id){
    $("#viewImage").modal();
    $("#imgViewer").html("<img width='400' id='imagetorotate"+id+"' height='350' style='object-fit: contain;' src='assets/images/"+img+"'>");
  }
  function addQntty(flowerid){
    $.post("ajax/getRemainingQntty.php", {
      flowerid: flowerid
    }, function(data){
      $("#addQuantity").modal();
      $("#qnnty_remain").val(data);
      $("#flowerid_val").val(flowerid);
    })
  }
  function addQuantity(){
    var category_id = $("#category_id").val();
    var qnnty = $("#qnnty").val();
    var flowerid = $("#flowerid_val").val();
    var qnnty_remain = $("#qnnty_remain").val();
    $.post("ajax/add_quantity.php", {
      qnnty: qnnty,
      flowerid: flowerid,
      qnnty_remain: qnnty_remain
    }, function(data){
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Quantity Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Can't add Quantity, Please try Again","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#addQuantity").modal('hide');
      getflowers(category_id);
    })
  }
  function addSizes(id){
    $("#addSizes").modal();
    $("#flowerid_val_size").val(id);
    
  }
  function addSize_flower(){
    var id = $("#flowerid_val_size").val();
    var size_val = $("#size_val").val();
    var action = 'add';
    $.post("ajax/size.php", {
      id: id,
      size_val,
      ac
    }, function(data){

    });
  }
  function getFlower_category(){
    var category_id = $("#category_id").val();
    getflowers(category_id)
  }
  function addCategory(){
    
  }
  function updateCat(id){
    
  }
  function deleteFlower(){
    var count_checked = $('input[name="checkbox_flower"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        $.post("ajax/deleteFlowers.php", {
          checked_array: count_checked
        }, function(data){
          alert(data);
        })
  }
  function getflowers(category_id){
    $("#flowers").DataTable().destroy();
    $('#flowers').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/flowers.php",
        "dataSrc":"data",
        "data":{
          category_id: category_id
        },
        "type":"POST"
    },
    "columns":[
        {
          "mRender": function(data,type,row){
              return "<input type='checkbox' name='checkbox_flower' value='"+row.flower_id+"'>";    
          }
        },
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"flower_img"
        },
        {
            "data":"flower_name"
        },
        {
            "data":"flower_desc"
        },
        {
            "data":"flower_size"
        },
        // {
        //     "data":"flower_color"
        // },
        {
          "data":"quantity"
        }
        
    ]   
    });
  }
</script>