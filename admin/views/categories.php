<h3><i class="fa fa-angle-right"></i> Types </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#category"><span class='fa fa-plus-circle'></span> Add</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      <table id='Categories' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th></th>
                  <th>NAME</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_category.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getCategory();
  });
  function viewItems(id){
    window.location = 'index.php?page=type-variation-list&cat_id='+id;
  }
  function addCategory(){
    var action = 'add';
    var name = $("#category_name").val();
    $.post("ajax/categories.php", {
      action: action,
      name: name
    }, function(data){
      $("#category").modal('hide');
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Category Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Category Already Exist","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#category_name").val("");
      getCategory();
    })
  }
  function updateCat(id){
    var action = 'edit';
    var name = $("#catName"+id).val();
    $("#updateCat"+id).prop("disabled", true);
    $("#updateCat"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/categories.php", {
      action: action,
      id: id,
      name: name
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully Updated","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function deleteCat(id){
    var action = 'delete';
    $.post("ajax/categories.php", {
      id: id,
      action: action
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good","Category Successfully deleted","Success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }

      getCategory();
    })
  }
  function getCategory(){
    $("#Categories").DataTable().destroy();
    $('#Categories').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/category.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"catNAME"
        }
        
    ]   
    });
  }
</script>