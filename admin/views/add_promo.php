<?php 
$id = $_GET['id'];
?>
<?php if($id == 0) { ?>
<h3><a href="#" onclick="window.location='index.php?page=promotions'"> Promotions</a> <i class="fa fa-angle-right"></i> Add Promotion </h3>
<div class="row">
  <br>
  <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Name: </span>
      <input type="text" name="promoName" id='promoName' class="form-control">
    </div>
  </div>
  <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Description: </span>
      <textarea rows="1" id="promoDesc" class="form-control" name='promoDesc' style="resize: none;"></textarea>
    </div>
  </div>
  <div class="col-md-12" style="margin-top: 10px">
    <button type="button" onclick="addPromoHeader()" id='addPromoHeader' class="btn btn-sm btn-primary pull-right"><span class="fa fa-check-circle"></span> Continue</button>
  </div>
  <div class="col-md-12" style="border: 1px solid white;margin-top: 10px;"></div>
  <div class="col-md-12" id='flower_lists' style="margin-top: 10px;display: none;">
    <div class='col-md-12'>
        <input type="hidden" id="promoID" name="">
       <span class="btn-group">
           <button class='btn btn-lg btn-info pull-right' onclick="viewAddedToPpromo()"><span class='fa fa-eye'></span> View Promo </button>
       </span>
    </div> 
    <?=getFlowers_promo()?>
  </div>
</div>
<?php }else {  
  $details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_promo WHERE promo_id = '$id'"));
  ?>
<h3><a href="#" onclick="window.location='index.php?page=promotions'"> Promotions</a> <i class="fa fa-angle-right"></i> Update Promotion </h3>
<div class="row">
  <br>
  <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Name: </span>
      <input type="text" name="promoName" id='promoName' value='<?=$details["promo_name"]?>' class="form-control">
    </div>
  </div>
  <div class="col-md-6" style="margin-top: 10px">
    <div class="input-group">
      <span class="input-group-addon"> Description: </span>
      <textarea rows="1" id="promoDesc" class="form-control" name='promoDesc' style="resize: none;"><?=$details['promo_desc']?></textarea>
    </div>
  </div>
  <div class="col-md-12" style="margin-top: 10px">
    <button type="button" onclick="updateheader()" id='updateheader' class="btn btn-sm btn-primary pull-right"><span class="fa fa-check-circle"></span> Update </button>
  </div>
  <div class="col-md-12" style="border: 1px solid white;margin-top: 10px;"></div>
  <div class="col-md-12" id='flower_lists' style="margin-top: 10px;">
    <div class='col-md-12'>
        <input type="hidden" id="promoID" value='<?=$id?>' name="">
       <span class="btn-group">
           <button class='btn btn-lg btn-info pull-right' onclick="viewAddedToPpromo()"><span class='fa fa-eye'></span> View Promo </button>
       </span>
    </div> 
    <?=getFlowers_promo()?>
  </div>
</div>
<?php } ?>

<?php require 'modals/included_to_promo.php'; ?>
<script type="application/javascript">
  function addPromoHeader(){
    var promoName = $("#promoName").val();
    var promoDesc = $("#promoDesc").val();
    $("#addPromoHeader").prop("disabled", true);
    $("#addPromoHeader").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/add_promo_header.php", {
      promoName: promoName,
      promoDesc: promoDesc
    }, function(data){
      if(data > 0){
        $("#promoID").val(data);
        $("#flower_lists").css("display","block");
        $("#addPromoHeader").prop("disabled", true);
        $("#addPromoHeader").html("<span class='fa fa-check-circle'></span> Continue");
      }
    });
  }
  function updateheader(){
    var promoID = $("#promoID").val();
    var promoName = $("#promoName").val();
    var promoDesc = $("#promoDesc").val();
    $.post("ajax/update_header.php", {
      promoID: promoID,
      promoName: promoName,
      promoDesc: promoDesc
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Successfully Updated","success");
        
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Error while deleting data","danger");
      }
    });
  }
  function deleteFlowertoPromo(id){
    $.post("ajax/deleteFlower_promo.php", {
      id: id
    }, function(data){
        if(data > 0){
          alertMe("fa fa-check-circle","All Good!","Successfully Delete","success");
          
        }else{
          alertMe("fa fa-exclamation","Aw Snap!","Error while deleting data","danger");
        }
        promoDetails();
    })
  }
  function viewAddedToPpromo(){
    $("#promo_included").modal();
    promoDetails();
  }
  function promoDetails(){
    var headerID = $("#promoID").val();
    $("#flower_in_promo").DataTable().destroy();
        $('#flower_in_promo').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/flower_in_promo.php",
            "dataSrc":"data",
            "data":{
                headerID: headerID
            },
            "type":"POST",
        },
        "columns":[
            {
                "data":"count"
            },
            {
                "data":"flower_img"
            },
            {
                "data":"action"
            }

            
        ]   
        });
}
  function addItemTopromo(id){
    var promoID = $("#promoID").val();
    $.post("ajax/add_flower_to_promo.php", {
      promoID: promoID,
      id: id
    }, function(data){
      if(data > 0){
        alertMe("fa fa-check-circle","All Good!","Successfully Added","success");
      }else{
        alertMe("fa fa-exclamation","Aw Snap!","Error while saving data","danger");
      }
    })
  }
  $(document).ready(function() {
    $("#addflowerList").on('submit',(function(e) {
        e.preventDefault();
            $("#addFlower").prop("disabled", true);
            $("#addFlower").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_flower.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                if(data > 0){
                    alertMe("fa fa-check-circle","All Good!","Successfully Added","success");
                    $("#addFlower").prop("disabled", false);
                    $("#addFlower").html("<span class='fa fa-check-circle'></span> Continue ");

                    $("#files").val("");
                    $("#flowerName").val("");
                    $("#categoryID").val("");
                    $("#sizeID").val("");
                    $('#colorVal').val("");
                    $('#desc').val("");
                }else{
                    alert("Error");
                }
                
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));
  });
  $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

    
  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
</script>