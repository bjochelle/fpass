<style type="text/css">
  .fc-center{
    color: black;
  }
  .fc-day-number{
    color: black;
  }
  .fc-day-header{
    color: black;
  }
</style>
<h3><i class="fa fa-angle-right"></i> Calendar </h3>
<div class="row">
	<div class="col-lg-12" style='margin-top: 10px;'>
      <h3> Inventory For Today </h3>
      <table id='inventory' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th>#</th>
                  <th>ITEM</th>
                  <th>QUANTITY</th>
              </tr>
          </thead>
          <tbody>
              <tr style="text-align: center;background-color: #343940;color: white;">
                <td colspan="3">TYPES</td>
                <?php 
                $getCateg = mysql_query("SELECT * FROM tbl_category");
               
                while($row_1 = mysql_fetch_array($getCateg)){
                  $catID = $row_1['category_id'];
                ?>
                <tr style="background-color: #34394087;color: white;">
                  <td colspan="3"><?=$row_1['category_name']?></td>
                </tr>
                <?php 
                $getItems = mysql_query("SELECT * FROM tbl_category_items WHERE category_id = '$catID'");
                 $count = 1;
                while($row_2 = mysql_fetch_array($getItems)){
                  $item_id = $row_2['item_id'];
                  $color = (getRemainingQuantity($item_id, 'C') < 10)?"background-color: #ff000042":"";
                ?>
                <tr style='<?=$color?>'>
                  <td><?=$count++;?></td>
                  <td><?=$row_2['item_name']?></td>
                  <td><?=getRemainingQuantity($item_id, 'C')?></td>
                </tr>
                <?php } ?>
                <?php } ?>
              </tr>
              <tr style="text-align: center;background-color: #343940;color: white;">
                <td colspan="3">CATEGORIES</td>
                <?php 
                $getOcc = mysql_query("SELECT * FROM tbl_occasion_list");
               
                while($row_3 = mysql_fetch_array($getOcc)){
                  $occID = $row_3['occasion_id'];
                ?>
                <tr style="background-color: #34394087;color: white;">
                  <td colspan="3"><?=$row_3['occasion_name']?></td>
                </tr>
                <?php 
                $getItems = mysql_query("SELECT * FROM tbl_occasion_items WHERE occasion_id = '$occID'");
                 $count2 = 1;
                while($row_2 = mysql_fetch_array($getItems)){
                  $item_id = $row_2['occasion_item_id'];
                  $color = (getRemainingQuantity($item_id, 'O') < 10)?"background-color: #ff000042":"";
                ?>
                <tr style='<?=$color?>'>
                  <td><?=$count2++;?></td>
                  <td><?=$row_2['occasion_item_name']?></td>
                  <td><?=getRemainingQuantity($item_id, 'O')?></td>
                </tr>
                <?php } ?>
                <?php } ?>
              </tr>
              <tr style="text-align: center;background-color: #343940;color: white;">
                <td colspan="3">CAKES</td>
                <?php 
                $getcakes = mysql_query("SELECT * FROM tbl_cakes");
                 $count3 = 1;
                while($row_3 = mysql_fetch_array($getcakes)){
                  $item_id = $row_3['cake_id'];
                  $color = (getRemainingQuantity($item_id, 'CE') < 10)?"background-color: #ff000042;":"";
                ?>
                <tr style='<?=$color?>'>
                  <td><?=$count3++;?></td>
                  <td><?=$row_3['cake_name']?></td>
                  <td><?=getRemainingQuantity($item_id, 'CE')?></td>
                </tr>
                <?php } ?>
              </tr>
          </tbody>
      </table>
  </div>
</div>
<script type="application/javascript">
  function gotoTrans(){
    var id = $("#transID").val();
    var type = $("#transtype").val();

    if(type == 'online'){
      window.location = 'index.php?page=online-transaction-details&id='+id;
    }else{
      window.location = 'index.php?page=view-walkin-details&id='+id;
    }
  }
	$(document).ready( function(){
  });
</script>