<div class="modal fade" id="discount" tabindex="-1" role="dialog" aria-labelledby="discountLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="discountLabel"><span class="fa fa-plus-circle"></span> Add Discount to Items </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">Items: </span>
              <select class="form-control" style="width: 100% !important" id="select_search_all" multiple>
                <?=getCategoryVariation($cat_id)?>
              </select>
            </div>
          </div>
          <div class="col-md-12" style="padding-top: 10px">
              <label><input type="checkbox" name="usePercentages" onchange='usePercentage()'> Use Percentage ( % )</label>
          </div>
          <div class="col-md-12" id="amnt" style="padding-top: 10px">
            <div class="input-group">
              <span class="input-group-addon">Discount: </span>
              <input type="number" id="amnt_discount" class="form-control" name="">
            </div>
          </div>
          <div class="col-md-12" id="perc" style="padding-top: 10px;display: none">
            <div class="input-group">
              <span class="input-group-addon">Discount: </span>
              <input type="number" id="perc_discount" class="form-control" name="">
              <span class="input-group-addon"> % </span>
            </div>
          </div>
          <div class="col-md-12" style="padding-top: 10px;">
            <div class="input-group">
              <span class="input-group-addon"> Discount Duration Date: </span>
              <input type="date" id="duration_date" class="form-control" name="">
             <!--  <span class="input-group-addon"> Time </span>
              <input type="time" id="duration_time" class="form-control" name=""> -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addDiscountPrice()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>