<div class="modal fade" id="promocode" tabindex="-1" role="dialog" aria-labelledby="promocodeLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="promocode"><span class="fa fa-send"></span> Send Promo Code</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <label style="font-size: 18px;">Code: <i style="font-weight: bolder"><?=generatemd5Code()?></i></label>
            <input type="hidden" id="promocode_val" value="<?=generatemd5Code()?>" name="">
          </div>
          <br>
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon"> Amount: </span>
              <input type="number" id="codeAmount" class="form-control" name="">
            </div>
          </div>
          <br>
          <div class="col-md-12" style="margin-top: 10px">
            <div class="input-group">
              <span class="input-group-addon"> Date Expired: </span>
              <input type="date" id="dateExpired" class="form-control" name="">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" id="addpromoCodes" onclick='addpromoCodes()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>