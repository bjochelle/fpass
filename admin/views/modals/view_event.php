<div class="modal fade" id="viewEvent" tabindex="-1" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="viewEvent"><span class="fa fa-eye"></span> View Transactions </h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <input type="hidden" id="transID" name="">
            <input type="hidden" id="transtype" name="">
            <div class="col-md-12">
              <div class="input-group">
                  <span class="input-group-addon"><strong> Customer Name:</strong></span>
                  <input type="text" class="form-control" id="customer" readonly>
              </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
              <div class="input-group">
                  <span class="input-group-addon"><strong> Delivery Date:</strong></span>
                  <input type="text" class="form-control" id="deliverydate" readonly>
              </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px">
              <div class="input-group">
                  <span class="input-group-addon"><strong> Delivery Time :</strong></span>
                  <input type="text" class="form-control" id="deliverytime" readonly>
              </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <span class="btn-group">
            <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        </span>
         <span class="btn-group">
            <button class="btn btn-primary btn-sm" onclick='gotoTrans()'>Go to Details <span class="fa fa-arrow-right"></span> </button>
        </span>
    </div>
    </div>
  </div>
</div>