<div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="categoryLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="categoryLabel"><span class="fa fa-plus-circle"></span> Add Category</h4>
      </div>
      <div class="modal-body">
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Category</span>
            <input type="text" class="form-control" name="" id='category_name'>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addCategory()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>