<div class="modal fade" id="occasion" tabindex="-1" role="dialog" aria-labelledby="occasionLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="occasionLabel"><span class="fa fa-plus-circle"></span> Add Occasion</h4>
      </div>
      <div class="modal-body">
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Name</span>
            <input type="text" class="form-control" name="" id='occasion_name'>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addCategory()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>