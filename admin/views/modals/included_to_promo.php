<div class="modal fade" id="promo_included" tabindex="-1" role="dialog" aria-labelledby="promo_includedLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="promo_includedLabel"><span class="fa fa-eye"></span> Promo </h4>
      </div>
      <div class="modal-body">
            <table id='flower_in_promo' class="table" style='margin-top:10px;width: 100%;'>
                <thead>
                    <tr>
                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>FLOWER</th>
                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
      </div>
    </div>
  </div>
</div>