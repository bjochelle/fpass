<div class="modal fade" id="addQuantity" tabindex="-1" role="dialog" aria-labelledby="categoryLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="categoryLabel"><span class="fa fa-plus-circle"></span> Add Quantity </h4>
      </div>
      <div class="modal-body">
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Remaining Quantity</span>
            <input type="number" readonly class="form-control" name="" id='qnnty_remain'>
          </div>
        </div>
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Add Quantity</span>
            <input type="number" class="form-control" name="" id='qnnty'>
            <input type="hidden" class="form-control" name="" id='flowerid_val'>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addQuantity()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>