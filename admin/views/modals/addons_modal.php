<form id="addons_variation_add" method="POST" action="" enctype="multipart/form-data">
<div class="modal fade" id="addons_variants" tabindex="-1" role="dialog" aria-labelledby="addons_variantsLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="addons_variants"><span class="fa fa-plus-circle"></span> Add Add-ons </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class='col-md-12' style='text-align: center'>
            <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="assets/images/img_upload.png" style="object-fit: contain;width:320px;height:200px;">

            <div class="image-upload" style="margin-top: 20px;margin-left: 52px;">
            <input type="file" name="avatar" style='visibility: hidden' id="files" class="btn-inputfile share" />
            <label for="files" class="btn default" style="font-size: 16px;margin-top:-40px;margin-right: 30px;"><i class="fa fa-file-image-o"></i> CHOOSE IMAGE </label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon">Name</span>
              <input type="text" class="form-control" name="addons_name" id='addons_name'>
            </div>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon">Price</span>
              <input type="text" class="form-control" name="addons_price" id='addons_price'>
            </div>
          </div>
          <div class="col-md-12" style="margin-top: 10px">
            <div class="input-group">
              <span class="input-group-addon">Description</span>
              <input type="text" class="form-control" name="addons_desc" id='addons_desc'>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="submit" id='variation_add' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>