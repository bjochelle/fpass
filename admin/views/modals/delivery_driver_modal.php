<div class="modal fade" id="delivery_driver" tabindex="-1" role="dialog" aria-labelledby="delivery_driverLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="delivery_driverLabel"><span class="fa fa-plus-circle"></span> Choose Driver </h4>
      </div>
      <div class="modal-body">
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Driver</span>
            <select class="form-control" id="driverID">
              <option value="">&mdash; Please Choose &mdash; </option>
            <?php
            $getDriver = mysql_query("SELECT * FROM tbl_users WHERE category = 2 AND status = 'A'");
            while($row = mysql_fetch_array($getDriver)){
            ?>
              <option value="<?=$row['user_id']?>"><?=$row['firstname'].' '.$row['lastname']?></option>
            <?php } ?>
            </select>
          </div>
          <input type="hidden" id="transID" name="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addDrivertoTrans()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>