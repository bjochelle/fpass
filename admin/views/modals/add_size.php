<div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="sizesModalLabel"><span class="fa fa-plus-circle"></span> Add Sizes </h4>
      </div>
      <div class="modal-body">
        <div class="form-group ">
          <div class="input-group">
            <span class="input-group-addon">Size</span>
            <input type="text" class="form-control" name="" id='size_val'>
            <input type="hidden" class="form-control" name="" id='flowerid_val_size'>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" onclick='addSizes()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>