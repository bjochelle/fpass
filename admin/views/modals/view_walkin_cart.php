<div class="modal fade" id="view_cart" tabindex="-1" role="dialog" aria-labelledby="view_cartLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="view_cart"><span class="fa fa-shopping-cart"></span> Cart </h4>
      </div>
      <div class="modal-body">
        
          <div id="cart_wrapper">
            
          </div>
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button <?=$disabled?> type="button" id='checkout' onclick='checkout_cart()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Checkout</button>
      </div>
    </div>
  </div>
</div>