<div class="modal fade" id="driver" tabindex="-1" role="dialog" aria-labelledby="driverLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="driverLabel"><span class="fa fa-plus-circle"></span> Add Driver</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="input-group">
              <span class="input-group-addon">Fullname</span>
              <input type="text" class="form-control" placeholder="Firstname" name="" id='fname'>
              <input type="text" class="form-control" placeholder="Middlename" name="" id='mname'>
              <input type="text" class="form-control" placeholder="Lastname" name="" id='lname'>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
        <button type="button" id="driverBtn" onclick='addDriver()' class="btn btn-primary"><span class="fa fa-check-circle"></span> Save changes</button>
      </div>
    </div>
  </div>
</div>