<link href="assets/css/style.css" rel="stylesheet">
<h3><i class="fa fa-angle-right"></i> Promotions </h3>
<div class="row">
  <div class='col-md-12'>
      <button class='btn btn-sm btn-primary pull-right' onclick='window.location="index.php?page=add-promo&id=0"'><span class='fa fa-plus-circle'></span> Add</button>
       <button class='btn btn-sm btn-danger pull-right' id='deletePromos' onclick='deletePromos()'><span class='fa fa-trash'></span> Delete</button>
  </div>
  <div class='col-md-12' style='margin-top:10px;'>
      
      <table id='promos' class="table table-bordered table-hover" style='margin-top:10px;'>
          <thead style='background-color: #343940;color: white;'>
              <tr>
                  <th></th>
                  <th>#</th>
                  <th></th>
                  <th>NAME</th>
                  <th>DESCRIPTION</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div>
</div>
<?php require 'modals/add_qntty.php'; ?>
<?php require 'modals/view_image.php'; ?>
<script type="application/javascript">
  $(document).ready(function() {
    getFlower_category();
    
  });
  function deletePromos(){
    var count_checked = $('input[name="checkbox_promo"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        if(count_checked == ''){
          alertMe("fa fa-exclamation","Aw Snap","No promo selected","warning");
        }else{
          $.post("ajax/delete_promo.php", {
            checked_array: count_checked
          }, function(data){
            if(data == 1){
              alertMe("fa fa-check-circle","All Good","Successfully Deleted","Success");
            }else{
              alertMe("fa fa-exclamation","Aw Snap","Error while deleting the data","error");
            }
            getflowers();
          })
        }
  }
  function showImage(img, id){
    $("#viewImage").modal();
    $("#imgViewer").html("<img width='400' id='imagetorotate"+id+"' height='350' style='object-fit: contain;' src='assets/images/"+img+"'>");
    $('#imagetorotate'+id).tsRotate();
  }
  function addQntty(flowerid){
    $.post("ajax/getRemainingQntty.php", {
      flowerid: flowerid
    }, function(data){
      $("#addQuantity").modal();
      $("#qnnty_remain").val(data);
      $("#flowerid_val").val(flowerid);
    })
  }
  function addQuantity(){
    var category_id = $("#category_id").val();
    var qnnty = $("#qnnty").val();
    var flowerid = $("#flowerid_val").val();
    var qnnty_remain = $("#qnnty_remain").val();
    $.post("ajax/add_quantity.php", {
      qnnty: qnnty,
      flowerid: flowerid,
      qnnty_remain: qnnty_remain
    }, function(data){
      if(data == 1){
        alertMe("fa fa-check-circle","All Good","Quantity Successfully Added","Success");
      }else if(data == 2){
        alertMe("fa fa-exclamation","Aw Snap","Can't add Quantity, Please try Again","warning");
      }else{
        alertMe("fa fa-exclamation","Aw Snap","Error while saving data","error");
      }
      $("#addQuantity").modal('hide');
      getflowers(category_id);
    })
  }
  function getFlower_category(){
    var category_id = $("#category_id").val();
    getflowers(category_id)
  }
  function addCategory(){
    
  }
  function updateCat(id){
    
  }
  function deleteFlower(){
    var count_checked = $('input[name="checkbox_flower"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        $.post("ajax/deleteFlowers.php", {
          checked_array: count_checked
        }, function(data){
          alert(data);
        })
  }
  function viewDetails(id){
    window.location = 'index.php?page=add-promo&id='+id;
  }
  function getflowers(){
    $("#promos").DataTable().destroy();
    $('#promos').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/promos.php",
        "dataSrc":"data"
    },
    "columns":[
        {
          "mRender": function(data,type,row){
              return "<input type='checkbox' name='checkbox_promo' value='"+row.promo_id+"'>";    
          }
        },
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"promo_name"
        },
        {
            "data":"promo_desc"
        }
        
    ]   
    });
  }
</script>