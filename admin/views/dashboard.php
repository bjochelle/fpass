<h3><i class="fa fa-angle-right"></i> Dashboard </h3>
<div class="row">
	<!-- <div class="col-lg-12" style='margin-top: 10px;'>
        <div id="calendar"></div>
  </div> -->
  <div class="col-lg-6" style='margin-top: 10px;'>
      <div class="darkblue-panel pn">
        <div class="darkblue-header">
          <h5>Calendar</h5>
        </div>
        <span class="fa fa-calendar" style="font-size: 10em;"></span>
        
        <h3 style="cursor: pointer" onclick="window.location='index.php?page=calendar'">Go to Calendar <span class="fa fa-hand-o-right"></span></h3>
      </div>
  </div>
  <div class="col-lg-6" style='margin-top: 10px;'>
      <div class="darkblue-panel pn">
        <div class="darkblue-header">
          <h5>Inventory for Today</h5>
        </div>
        <span class="fa fa-clipboard" style="font-size: 10em;"></span>
        
        <h3 style="cursor: pointer" onclick="window.location='index.php?page=inv-today'">Check Inventory <span class="fa fa-hand-o-right"></span></h3>
      </div>
  </div>
</div>
<?php require 'modals/view_event.php';?>
<?php 

  $query_getEvents = mysql_query("SELECT 
                                  'online' as type, 
                                  transaction_id as id, 
                                  CONCAT(u.firstname,' ',u.lastname) as customer,
                                  reference_num as rnum,
                                  delivery_date as ddate,        
                                  delivery_time as dtime 
                                  FROM tbl_transaction as t,tbl_users as u
                                  WHERE t.user_id = u.user_id
                                  UNION 
                                  SELECT 
                                  'walkin' as type, 
                                  w_transaction_id as id, 
                                  costumer as customer,
                                  reference_num as rnum,
                                  delivery_date as ddate,
                                  delivery_time as dtime 
                                  FROM tbl_walkin_transaction");
  $count_events = mysql_num_rows($query_getEvents);
?>
<script type="application/javascript">
  function gotoTrans(){
    var id = $("#transID").val();
    var type = $("#transtype").val();

    if(type == 'online'){
      window.location = 'index.php?page=online-transaction-details&id='+id;
    }else{
      window.location = 'index.php?page=view-walkin-details&id='+id;
    }
  }
	$(document).ready( function(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($query_getEvents)){
          $event_title = $fetch_events['customer'];
          $start_date = $fetch_events['ddate'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['ddate'])));
          $event_time = date("g:ia", strtotime($fetch_events['dtime']));
          $e_time = date("g:ia", strtotime($fetch_events['dtime']));
        ?>
        {
          title: '<?php echo strtoupper($fetch_events['customer'])." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['id']; ?>',
          event_date: '<?php echo date("F d, Y", strtotime($fetch_events['ddate'])); ?>',
          event_time: '<?php echo date("h:i A", strtotime($fetch_events['dtime'])); ?>',
          event_title: '<?php echo strtoupper($fetch_events['customer']); ?>',
          trans_type: '<?php echo $fetch_events['type']?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventClick: function(callEvent , jsEvent, view){
        $("#viewEvent").modal();

        $("#customer").val(callEvent.event_title);
        $("#deliverydate").val(callEvent.event_date);
        $("#deliverytime").val(callEvent.event_time);
        $("#transID").val(callEvent.id);
        $("#transtype").val(callEvent.trans_type);
       
      },
        eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
        }
    });
});
</script>