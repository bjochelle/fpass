<?php 
$id = $_GET['id'];

$status = mysql_fetch_array(mysql_query("SELECT * FROM tbl_transaction WHERE transaction_id = '$id'"));


$status = ($status['status'] == 0)?"<span style='color: orange'> PENDING </span>":(($status['status'] == 1)?"<span style='color: blue'> CHECKED OUT </span>":(($status['status'] == 2)?"<span style='color: blue'> ON DELIVERY </span>":(($status['status'] == '3')?"<span style='color: green'> FINISHED </span>":"<span style='color: red'> CANCELLED </span>")));
?>
<h3><i class="fa fa-angle-right"></i> Online Transaction Details </h3>
<div class="row">
  <div class="col-md-6">
      <span style="font-size: 24px;">STATUS: <?=$status?></span>
  </div>
  <div class="col-md-6">
      <button class="btn btn-success btn-sm pull-right" onclick='printReceipt(<?=$id?>)'><span class="fa fa-print"></span> Print Receipt</button>
  </div>
	<div class="col-lg-12" style='margin-top: 10px;'>
        <?php 
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $cart = mysql_query("SELECT * FROM tbl_transaction as h, tbl_cart as d WHERE h.transaction_id = d.transaction_id AND h.transaction_id = '$id'");
    $totalPrice = 0;
    while($row = mysql_fetch_array($cart)){
    $disabled = ($row['status'] > 0)?"disabled":"";
      $itemid = $row['item_id'];
      $price = $row['item_price'] * $row['item_quantity'];
      $cat = $row['item_type'];
      $totalPrice += $price;
      $sql = ($row['item_type'] == 'C')?"SELECT item_img,item_name,item_price FROM tbl_category_items WHERE item_id = '$itemid'":(($row['item_type'] == 'O')?"SELECT occasion_item_img,occasion_item_name,occasion_item_price FROM tbl_occasion_items WHERE occasion_item_id = '$itemid'":(($row['item_type'] == 'CE')?"SELECT cake_img,cake_name,cake_price FROM tbl_cakes WHERE cake_id = '$itemid'":"SELECT addon_img, addon_name, addon_price FROM tbl_addons WHERE addon_id = '$itemid'"));

      $query = mysql_fetch_array(mysql_query($sql));

      $check_discount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemid' AND item_type = '$cat' AND status = 0 AND discount_duration_date >= '$curdate'"));

      $itmPrc = (!empty($check_discount['item_id']))?$check_discount['new_price']:$query[2];
  ?>
  <div class="col-md-4"  id="row_div">
<div class="thumbnail">
  <img style='width: 100%;height: 200px;object-fit: cover;border-radius: 5px' class="media-object" src="../admin/assets/images/<?=$query[0]?>" alt="...">
  <div class="row">
    <div class="col-md-12"><?=$query[1]?></div>
    <div class="col-md-12">&#8369; <?=$row['item_price']?></div>
  </div>
</div>
</div>
<?php } ?>
  </div>
</div>
<div id="receipt_modal" class="modal fade" role="dialog" data-backdrop='static'>
  <div class="modal-dialog modal-lg" style="width: 1000px">

    <div class="modal-content">
      <div class="modal-header">
      </div>
      
      <div class="modal-body">
        <div id='loader1' style='text-align: center;font-size: 2.5rem;'><span class='fa fa-spin fa-spinner'></span> Loading Data, Please Wait </div>
        <iframe id="JOframe1" name="JOframe1" width="100%" height="500px" style="margin-bottom:-10px;" frameborder="0">
        </iframe>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
             <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" onclick="printIframe('JOframe1');"><span class="fa fa-print"></span> Print</button>
                       <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </span>
        </div>
      </div>
  </div>
</div>
</div>
<script type="application/javascript">
	$(document).ready( function(){
    $('#JOframe1').on('load', function () {
      $('#loader1').hide();
    });
  });
  function printReceipt(id){
    $("#receipt_modal").modal('show');
    var url = 'views/print/print_receipt.php?id='+id;
      $('#JOframe1').attr('src', url);
  }
</script>