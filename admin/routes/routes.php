<?php
switch ($page) {
	case 'dashboard':
		require 'views/dashboard.php';
		break;
	case 'types':
		require 'views/categories.php';
		break;
	case 'categories':
		require 'views/occasion_list.php';
		break;
	case 'type-variation-list':
		require 'views/category_variation.php';
		break;
	case 'category-variation-list':
		require 'views/occasion_variation.php';
		break;
	case 'cake-list':
		require 'views/cakes.php';
		break;
	case 'add-ons':
		require 'views/addons_list.php';
		break;
	case 'walkin-transaction':
		require 'views/walkin.php';
		break;
	case 'add-walkin-transaction':
		require 'views/add_walkin_trans.php';
		break;
	case 'online-transaction':
		require 'views/online.php';
		break;
	case 'inventory-report':
		require 'views/inventory_report.php';
		break;
	case 'graph-report':
		require 'views/graph_report.php';
		break;
	case 'drivers':
		require 'views/drivers.php';
		break;
	case 'costumers':
		require 'views/costumers.php';
		break;
	case 'view-walkin-details':
		require 'views/walkin_details.php';
		break;
	case 'online-transaction-details':
		require 'views/online_transaction_details.php';
		break;
	case 'messages':
		require 'views/messages.php';
		break;
	case 'calendar':
		require 'views/calendar.php';
		break;
	case 'inv-today':
		require 'views/inv_today.php';
		break;
	default:
		# code...
		break;
}
?>