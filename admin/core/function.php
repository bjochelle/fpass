<?php 
function checkSession(){
	if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	}
}
function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}
function generatemd5Code(){
    $string = "FPASS".date("YmdHis", strtotime(getCurrentDate()));

    $md = md5($string);

    return $md;
}
function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}
function select_query($type , $table , $params){

	$select_query = mysql_query("SELECT $type FROM $table WHERE $params")or die(mysql_error());
	$fetch = mysql_fetch_assoc($select_query);
	return $fetch;

}
function insert_query($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            $val = $lastID;
        }else{
            $val = 0;
        }
    }else{
        if($return_insert){
            $val = 1;
        }else{
            $val = 0;
        }
    }

    return $val;
}

function delete_query($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);
    
    if($return_delete){
    	$result = 1;
    }else{
    	$result = 0;
    }

    return $result;
}

function update_query($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql);
    if($return_query){
    	$result =  1;
    }else{
    	$result =  0;
    }
    return $result;
}
function getCategories(){
    $data = "<option value=''>&mdash; Please Choose &mdash;</option>";
    $query = mysql_query("SELECT * FROM tbl_category");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['category_id']."'>".$row['category_name']."</option>";
    }
    return $data;
}
function getSizes(){
    $data = "<option value=''>&mdash; Please Choose &mdash;</option>";
    $query = mysql_query("SELECT * FROM tbl_size");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['size_id']."'>".$row['size']."</option>";
    }
    return $data;
}
function getSize($sizeid){
    $query = mysql_fetch_array(mysql_query("SELECT size FROM tbl_size WHERE size_id = '$sizeid'"));

    return $query[0];
}
function getCategoryName($catid){
    $query = mysql_fetch_array(mysql_query("SELECT category_name FROM tbl_category WHERE category_id = '$catid'"));

    return $query[0];
}
function getAllFlowers(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_flower");
    while($row = mysql_fetch_array($query)){
        $img = $row['flower_img'];
        $name = $row['flower_name'];
        $desc = $row['flower_desc'];
        $data .= "<div class='col-md-4 col-sm-4 col-xs-12 filtr-item' data-sort='Busy streets'>
                    <div class='agileits-img'>
                        <a href='assets/images/$img' class='swipebox' title='$desc'>
                            <img class='img-responsive' src='../assets/images/$img' alt=''  /> 
                            <div class='wthree-pcatn'>
                                <h4>$name</h4>  
                            </div> 
                        </a> 
                    </div>
                </div>";
    }
    return $data;
}
function getFlowers_promo(){
    $equipments = mysql_query("SELECT * FROM tbl_category");
    while($e_row = mysql_fetch_array($equipments)){
        $name = $e_row['category_name'];
        $id = $e_row['category_id'];
        echo "<div class='col-md-12' style='max-width: 900px;overflow-y: auto;margin-top:20px;'>";
                echo "<h5 style='font-size: 20px;text-align: center;background: #cccccc;padding: 5px;'>$name</h5>";
                echo "<ul class='thumbnails' style='display: flex;list-style: none;'>";
            $designs = mysql_query("SELECT * FROM tbl_flower WHERE flower_cat = '$id'");
                while($d_row = mysql_fetch_array($designs)){
                    $flower_id = $d_row['flower_id'];
                    $flower_img = $d_row['flower_img'];
                    $flower_name = strtoupper($d_row['flower_name']);
                    echo "<li style='max-width: 200px;'>";
                        echo "<div class='thumbnail'>";
                            echo "<img src='assets/images/$flower_img' style='height: 120px;object-fit:fill;width: 190px;' alt='ALT NAME'>";
                            echo "<div class=''>";
                              echo "<h3 style='background: #cccccc;text-align: center;padding: 5px;font-size:15px'>$flower_name</h3>";
                              echo "<p align='center'><a href='#' onclick='addItemTopromo(\"".$flower_id."\")' class='btn btn-primary btn-block'><span class='fa fa-plus-circle'></span> Add to Promo</a></p>";
                            echo "</div>";
                          echo "</div>";
                    echo "<li>";
                }

                echo "</ul>"; 
        echo "</div>";
    }
}
function getCategoryDetails($name, $id){
    $query = mysql_fetch_array(mysql_query("SELECT $name FROM tbl_category WHERE category_id = '$id'"));

    return $query[0];;
}
function getOccasionDetails($name, $id){
    $query = mysql_fetch_array(mysql_query("SELECT $name FROM tbl_occasion_list WHERE occasion_id = '$id'"));

    return $query[0];;
}
function getAllItemsPerCategory($category_id){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $curtime = date("H:i", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_category_items WHERE category_id = '$category_id'");
    $counter = mysql_num_rows($query);
    if($counter > 0){
        while($row = mysql_fetch_array($query)){
            $item_id = $row['item_id'];
            $getDiscount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$item_id' AND item_type = 'C' AND status = 0 AND discount_duration_date >= '$curdate'"));
            if(!empty($getDiscount['item_id'])){
                $discount = ($getDiscount['discount_type'] == 1)?$getDiscount['discounted_price'].' % Off':number_format($getDiscount['discounted_price'], 2).' Off';
                $new_price = $getDiscount['new_price'];
                $until = date("M d, Y", strtotime($getDiscount['discount_duration_date'])).' '.date("h:i A", strtotime($getDiscount['discount_duration_time']));
                $disable_btn = "";
            }else{
                $discount = "N/A";
                $new_price = "N/A";
                $until = "N/A";
                $disable_btn = "disabled";
            }
            $remainQntty = getRemainingQuantity($item_id, 'C');
            
            $item_id = $row['item_id'];
            $data .= ' <div class="col-xs-18 col-sm-6 col-md-4">';
                $data .= '<div class="thumbnail">';
                    $data .= '<img style="height: 200px;object-fit: cover;width: 100%;" src="assets/images/'.$row['item_img'].'" alt="">';
                    $data .= '<div class="caption">';
                        $data .= '<h4>'.$row['item_name'].'</h4>';

                        $data .= '<p>'.$row['item_desc'].'</p>';
                        $data .= '<hr>';
                        $data .= '<div class="input-group">';
                            $data .= '<div class="input-group-addon">Remaining Quantity</div>';
                            $data .= '<input value="'.$remainQntty.'" readonly type="text" class="form-control" name="remainQntty" id="remainQntty">';
                            $data .= '<div class="input-group-addon" style="cursor: pointer;" onclick="addQntty('.$row["item_id"].')"><span class="fa fa-plus-circle"></span></div>';
                        $data .= '</div>';
                        $data .= '<hr>';
                        $data .= '<table class="table bordered">';
                            $data .= '<tbody>';
                                $data .= '<tr>';
                                    $data .= '<td>Price</td>';
                                    $data .= '<td>'.$row['item_price'].'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discount</td>';
                                    $data .= '<td>'.$discount.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discounted Price</td>';
                                    $data .= '<td>'.$new_price.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Until</td>';
                                    $data .= '<td>'.$until.'</td>';
                                $data .= '</tr>';
                            $data .= '</tbody>';
                        $data .= '</table>';
                        $data .= '<hr>';
                        $data .= '<button class="btn btn-danger" type="button" onclick="deleteCatVariation('.$item_id.')"  id="catVariation'.$item_id.'"><span class="fa fa-trash"></span> Delete</button><button class="btn btn-warning" type="button" onclick="removeDiscount('.$item_id.')" '.$disable_btn.'  id="removeDiscount'.$item_id.'"><span class="fa fa-minus-circle"></span> Remove Discount</button>';
                    $data .= '</div>';
                $data .= '</div>';
            $data .= '</div>';         
             
        }
    }else{
        $data = "<center><h4 style='color: #797979;font-size: 30px;'>No Items Found</h4></center>";
    }

    return $data;
}
function getAllItemsPerOccasion($occasion_id){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_occasion_items WHERE occasion_id = '$occasion_id'");
    $counter = mysql_num_rows($query);
    if($counter > 0){
        while($row = mysql_fetch_array($query)){
            $item_id = $row['occasion_id'];
            $itemid = $row['occasion_item_id'];
            $getDiscount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemid' AND item_type = 'O' AND status = 0 AND discount_duration_date >= '$curdate'"));
            if(!empty($getDiscount['item_id'])){
                $discount = ($getDiscount['discount_type'] == 1)?$getDiscount['discounted_price'].' % Off':number_format($getDiscount['discounted_price'], 2).' Off';
                $new_price = number_format($getDiscount['new_price'], 2);
                $until = date("M d, Y", strtotime($getDiscount['discount_duration_date']));
                $disable_btn = "";
            }else{
                $discount = "N/A";
                $new_price = "N/A";
                $until = "N/A";
                $disable_btn = "disabled";
            }
            $remainQntty = getRemainingQuantity($itemid, 'O');
            $color = ($remainQntty > 0)?"":"border: 1px solid red;";
            $data .= ' <div class="col-xs-18 col-sm-6 col-md-4">';
                $data .= '<div class="thumbnail">';
                    $data .= '<img style="height: 200px;object-fit: cover;width: 100%;" src="assets/images/'.$row['occasion_item_img'].'" alt="">';
                    $data .= '<div class="caption">';
                        $data .= '<h4>'.$row['occasion_item_name'].'</h4>';

                        $data .= '<p>'.$row['occasion_item_desc'].'</p>';
                        $data .= '<hr>';
                        $data .= '<div class="input-group" style="'.$color.'">';
                            $data .= '<div class="input-group-addon">Remaining Quantity</div>';
                            $data .= '<input value="'.$remainQntty.'" readonly type="text" class="form-control" name="remainQntty" id="remainQntty" >';
                            $data .= '<div class="input-group-addon" style="cursor: pointer;" onclick="addQntty('.$row["occasion_item_id"].')"><span class="fa fa-plus-circle"></span></div>';
                        $data .= '</div>';
                        $data .= '<hr>';
                        $data .= '<table class="table bordered">';
                            $data .= '<tbody>';
                                $data .= '<tr>';
                                    $data .= '<td>Price</td>';
                                    $data .= '<td>'.$row['occasion_item_price'].'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discount</td>';
                                    $data .= '<td>'.$discount.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discounted Price</td>';
                                    $data .= '<td>'.$new_price.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Until</td>';
                                    $data .= '<td>'.$until.'</td>';
                                $data .= '</tr>';
                            $data .= '</tbody>';
                        $data .= '</table>';
                        $data .= '<hr>';
                        $data .= '<button onclick="deleteOccasionVariation('.$itemid.')"  id="occasionVariation'.$itemid.'" class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button><button class="btn btn-warning" type="button" onclick="removeDiscount('.$itemid.')" '.$disable_btn.'  id="removeDiscount'.$itemid.'"><span class="fa fa-minus-circle"></span> Remove Discount</button>';
                    $data .= '</div>';
                $data .= '</div>';
            $data .= '</div>';         
             
        }
    }else{
        $data = "<center><h4 style='color: #797979;font-size: 30px;'>No Items Found</h4></center>";
    }

    return $data;
}
function getAllCakes(){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_cakes");
    $counter = mysql_num_rows($query);
    if($counter > 0){
        while($row = mysql_fetch_array($query)){
            $item_id = $row['cake_id'];
            $getDiscount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$item_id' AND item_type = 'CE' AND status = 0 AND discount_duration_date >= '$curdate'"));
            if(!empty($getDiscount['item_id'])){
                $discount = ($getDiscount['discount_type'] == 1)?$getDiscount['discounted_price'].' % Off':number_format($getDiscount['discounted_price'], 2).' Off';
                $new_price = number_format($getDiscount['new_price'], 2);
                $until = date("M d, Y", strtotime($getDiscount['discount_duration_date']));
                $disable_btn = "";
            }else{
                $discount = "N/A";
                $new_price = "N/A";
                $until = "N/A";
                $disable_btn = "disabled";
            }
            $remainQntty = getRemainingQuantity($item_id, 'CE');
            $data .= ' <div class="col-xs-18 col-sm-6 col-md-4">';
                $data .= '<div class="thumbnail">';
                    $data .= '<img style="height: 200px;object-fit: cover;width: 100%;" src="assets/images/'.$row['cake_img'].'" alt="">';
                    $data .= '<div class="caption">';
                        $data .= '<h4>'.$row['cake_name'].'</h4>';

                        $data .= '<p>'.$row['cake_desc'].'</p>';
                        $data .= '<hr>';
                        $data .= '<div class="input-group">';
                            $data .= '<div class="input-group-addon">Remaining Quantity</div>';
                            $data .= '<input value="'.$remainQntty.'" readonly type="text" class="form-control" name="remainQntty" id="remainQntty">';
                            $data .= '<div class="input-group-addon" style="cursor: pointer;" onclick="addQntty('.$row["cake_id"].')"><span class="fa fa-plus-circle"></span></div>';
                        $data .= '</div>';
                        $data .= '<hr>';
                        $data .= '<table class="table bordered">';
                            $data .= '<tbody>';
                                $data .= '<tr>';
                                    $data .= '<td>Price</td>';
                                    $data .= '<td>'.$row['cake_price'].'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discount</td>';
                                    $data .= '<td>'.$discount.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discounted Price</td>';
                                    $data .= '<td>'.$new_price.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Until</td>';
                                    $data .= '<td>'.$until.'</td>';
                                $data .= '</tr>';
                            $data .= '</tbody>';
                        $data .= '</table>';
                        $data .= '<hr>';
                        $data .= '<button onclick="deleteCakeVariation('.$item_id.')"  id="cakeVariation'.$item_id.'" class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button><button class="btn btn-warning" type="button" onclick="removeDiscount('.$item_id.')" '.$disable_btn.'  id="removeDiscount'.$item_id.'"><span class="fa fa-minus-circle"></span> Remove Discount</button>';
                    $data .= '</div>';
                $data .= '</div>';
            $data .= '</div>';         
             
        }
    }else{
        $data = "<center><h4 style='color: #797979;font-size: 30px;'>No Items Found</h4></center>";
    }

    return $data;
}
function getAllAddons(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_addons");
    $counter = mysql_num_rows($query);
    if($counter > 0){
        while($row = mysql_fetch_array($query)){
            $item_id = $row['addon_id'];
            $data .= ' <div class="col-xs-18 col-sm-6 col-md-3">';
                $data .= '<div class="thumbnail">';
                    $data .= '<img style="height: 150px;object-fit: cover;" src="assets/images/'.$row['addon_img'].'" alt="">';
                    $data .= '<div class="caption">';
                        $data .= '<h4>'.$row['addon_name'].'</h4>';

                        $data .= '<p>'.$row['addon_desc'].'</p>';

                        $data .= '<button onclick="deleteaddonsVariation('.$item_id.')"  id="addonsVariation'.$item_id.'" class="btn btn-block btn-danger"><span class="fa fa-trash"></span> Delete</button>';
                    $data .= '</div>';
                $data .= '</div>';
            $data .= '</div>';         
             
        }
    }else{
        $data = "<center><h4 style='color: #797979;font-size: 30px;'>No Items Found</h4></center>";
    }

    return $data;
}
function countCartItems($userid){
    $query = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_cart WHERE user_id = '$userid' AND status = 0"));

    return $query[0];
}
function getCategoryVariation($category){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_category_items WHERE category_id = '$category'");
    while($row = mysql_fetch_array($query)){
        $item_id = $row['item_id'];
        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_price_discount WHERE item_id = '$item_id' AND item_type = 'C' AND status = 0 AND discount_duration_date >= '$curdate'"));
        if($checker[0] == 0){
            $data .= "<option value='".$row['item_id']."'>".$row['item_name']."</option>";
        }
        
    }

    return $data;
}
function getOccasionVariation($category){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_occasion_items WHERE occasion_id = '$category'");
    while($row = mysql_fetch_array($query)){
        $item_id = $row['occasion_item_id'];
        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_price_discount WHERE item_id = '$item_id' AND item_type = 'O' AND status = 0 AND discount_duration_date >= '$curdate'"));
        if($checker[0] == 0){
            $data .= "<option value='".$row['occasion_item_id']."'>".$row['occasion_item_name']."</option>";
        }
        
    }

    return $data;
}
function getCakesVariation(){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_cakes");
    while($row = mysql_fetch_array($query)){
        $item_id = $row['cake_id'];
        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_price_discount WHERE item_id = '$item_id' AND item_type = 'CE' AND status = 0 AND discount_duration_date >= '$curdate'"));
        if($checker[0] == 0){
            $data .= "<option value='".$row['cake_id']."'>".$row['cake_name']."</option>";
        }
        
    }

    return $data;
}
function getTransactionDetails($trans_id){
    $data = "";
    $query = mysql_query("SELECT * FROM `tbl_transaction` as t, tbl_cart as c WHERE t.transaction_id = c.transaction_id AND t.transaction_id = '$trans_id' AND c.item_type != 'A'");
    $counter = mysql_num_rows($query);
    if($counter > 0){
        while($row = mysql_fetch_array($query)){
            $item_id = $row['item_id'];

            $item_id = $row['item_id'];

            $sql = ($row['item_type'] == 'C')?"SELECT * FROM tbl_category_items WHERE item_id = '$item_id'":(($row['item_type'] == 'O')?"SELECT * FROM tbl_occasion_items WHERE occasion_item_id = '$item_id'":"SELECT * FROM tbl_cakes WHERE cake_id = '$item_id'");
            
            $data .= ' <div class="col-xs-18 col-sm-6 col-md-4">';
                $data .= '<div class="thumbnail">';
                    $data .= '<img style="height: 200px;object-fit: cover;width: 100%;" src="assets/images/'.$row['item_img'].'" alt="">';
                    $data .= '<div class="caption">';
                        $data .= '<h4>'.$row['item_name'].'</h4>';

                        $data .= '<p>'.$row['item_desc'].'</p>';
                        $data .= '<hr>';
                        $data .= '<div class="input-group">';
                            $data .= '<div class="input-group-addon">Remaining Quantity</div>';
                            $data .= '<input value="'.$row['quantity'].'" readonly type="text" class="form-control" name="remainQntty" id="remainQntty">';
                            $data .= '<div class="input-group-addon" style="cursor: pointer;" onclick="addQntty('.$row["item_id"].')"><span class="fa fa-plus-circle"></span></div>';
                        $data .= '</div>';
                        $data .= '<hr>';
                        $data .= '<table class="table bordered">';
                            $data .= '<tbody>';
                                $data .= '<tr>';
                                    $data .= '<td>Price</td>';
                                    $data .= '<td>'.$row['item_price'].'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discount</td>';
                                    $data .= '<td>'.$discount.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Discounted Price</td>';
                                    $data .= '<td>'.$new_price.'</td>';
                                $data .= '</tr>';
                                $data .= '<tr>';
                                    $data .= '<td>Until</td>';
                                    $data .= '<td>'.$until.'</td>';
                                $data .= '</tr>';
                            $data .= '</tbody>';
                        $data .= '</table>';
                        $data .= '<hr>';
                        $data .= '<button class="btn btn-danger" type="button" onclick="deleteCatVariation('.$item_id.')"  id="catVariation'.$item_id.'"><span class="fa fa-trash"></span> Delete</button><button class="btn btn-warning" type="button" onclick="removeDiscount('.$item_id.')" '.$disable_btn.'  id="removeDiscount'.$item_id.'"><span class="fa fa-minus-circle"></span> Remove Discount</button>';
                    $data .= '</div>';
                $data .= '</div>';
            $data .= '</div>';         
             
        }
    }else{
        $data = "<center><h4 style='color: #797979;font-size: 30px;'>No Items Found</h4></center>";
    }

    return $data;
}
function getRemainingQuantity($item_id, $item_type){
    $ItemOut = mysql_fetch_array(mysql_query("SELECT IF(o.o_t IS NULL,0,o.o_t) + IF(w.w_t IS NULL,0,w.w_t) as ft FROM (SELECT sum(item_quantity) as o_t FROM `tbl_transaction` as t, tbl_cart as c WHERE t.transaction_id = c.transaction_id  AND c.item_id = '$item_id' AND c.item_type = '$item_type' AND t.status = 3) as o, (SELECT SUM(item_quantity) as w_t FROM tbl_walkin_transaction as wt , tbl_walkin_cart as wc WHERE wt.w_transaction_id = wc.trans_id AND wc.item_id = '$item_id' AND wc.item_type = '$item_type' AND wt.status = 3) as w"));

    $sql = ($item_type == 'C')?"SELECT quantity FROM tbl_category_items WHERE item_id = '$item_id'":(($item_type == 'O')?"SELECT quantity FROM tbl_occasion_items WHERE occasion_item_id = '$item_id'":"SELECT quantity FROM tbl_cakes WHERE cake_id = '$item_id'");

    $itemIn = mysql_fetch_array(mysql_query($sql));

    $remainQntty = intval($itemIn[0]) - intval($ItemOut[0]);

    return $remainQntty;//$item_id.' : '.$item_type;
}
function getTotalReviews($itemID,$itemType){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_ratings WHERE item_id = '$itemID' AND item_type = '$itemType'"));

    return $count[0];
}
function getOverallRatings($itemID,$itemType){
    $get = mysql_fetch_array(mysql_query("SELECT SUM(rate_val) as t, count(*) as c FROM tbl_ratings WHERE item_id = '$itemID' AND item_type = '$itemType'"));

    return number_format($get['t'] / $get['c'],2);
}
function getRatingspercustomer($user_id,$type,$item_id){
    $count = mysql_fetch_array(mysql_query("SELECT rate_val FROM tbl_ratings WHERE item_id = '$item_id' AND item_type = '$type' AND user_id = '$user_id'"));

    return "<span class='fa fa-star' style='color: gold;'></span> ".number_format($count[0],0)." Star/s";
}
function countNotif(){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_notifications WHERE notif_status = 0"));

    return $count[0];
}
function countNotif_msg(){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_notifications as n,tbl_users as u WHERE notif_status = 0 AND notif_type = 'M' AND n.user_id = u.user_id AND u.category != 0"));

    return $count[0];
}
function notifs(){
    $data = "";
     $count = mysql_query("SELECT * FROM tbl_notifications WHERE notif_status = 0");
     $num = mysql_num_rows($count);
     if($num > 0){
     while($row = mysql_fetch_array($count)){
        $userid = $row['user_id'];
        $trans_id = $row['trans_id'];
        $notif_id = $row['notif_id'];
        $user = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));
        $data .= '<li>';
            $data .= '<a href="#" onclick="gotoTransaction('.$trans_id.','.$notif_id.')">';
            $data .= '<span class="label label-success"><i class="fa fa-check-circle"></i></span>';
            $data .= strtoupper($user[0]). " has checked out his/her order.";
            // $data .= '<span class="small italic">'.date("M d, Y h:i A", strtotime($row['notif_date'])).'</span>';
            $data .= '</a>';
        $data .= '</li>';
     }
    }else{
        $data .= "<h4>No New Notifications</h4>";
    }

    return $data;
}
function notifs_msg(){
    $data = "";
     $count = mysql_query("SELECT * FROM tbl_notifications as n,tbl_users as u WHERE notif_status = 0 AND notif_type = 'M' AND n.user_id = u.user_id AND u.category != 0");
     $num = mysql_num_rows($count);
     if($num > 0){
     while($row = mysql_fetch_array($count)){
        $userid = $row['user_id'];
        $trans_id = $row['trans_id'];
        $notif_id = $row['notif_id'];
        $user = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));
        $data .= '<li>';
            $data .= '<a href="#" onclick="gotoMsg('.$userid.','.$notif_id.')">';
            $data .= '<span class="label label-success"><i class="fa fa-check-circle"></i></span>';
            $data .= strtoupper($user[0]). " has new message for you.";
            // $data .= '<span class="small italic">'.date("M d, Y h:i A", strtotime($row['notif_date'])).'</span>';
            $data .= '</a>';
        $data .= '</li>';
     }
    }else{
        $data .= "<h4>No New Notifications</h4>";
    }

    return $data;
}
function getUser($userid){
    $query = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));


    return $query[0];
}
function getTime(){
    $array = array("8" => "am", "9" => "am", "10" => "am", "11" => "am", "1" => "pm", "2" => "pm", "3" => "pm", "4" => "pm", "5" => "pm", "6" => "pm");

    return $array;
}
function getTimeAvail(){
    $data .= "<option value=''>&mdash; Please Select &mdash; </option>";
    $time = getTime();
    $getCurrentTime = date("H:00", strtotime(getCurrentDate()));
    foreach ($time as $time => $cat) {
        $time_Val = date("H:i", strtotime($time.':00'.$cat));
        // if($getCurrentTime < $time_Val){
            $data .= "<option value='".$time_Val."'>".date("h:i A", strtotime($time.':00'.$cat))."</option>";
        //}
    }

    return $data;
}
function getActivePromoCode($user_id){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $codes = mysql_query("SELECT * FROM tbl_promo_codes WHERE status = 0 AND DATE_FORMAT(date_expired, '%Y-%m-%d') >= '$curdate' AND user_id = '$user_id'");
    while($row = mysql_fetch_array($codes)){
        $data .= "<option value='".$row['code_id']."'>".$row['code_name']." (&#8369 ".$row['amount'].")</option>";
    }

    return $data;

}