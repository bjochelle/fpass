<aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h5 class="centered">Navigation</h5>
          <?php if($category_id == 0){ ?>
          <li class="mt">
            <a class="" href="index.php?page=dashboard">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th-list"></i>
              <span>Users</span>
              </a>
            <ul class="sub">
              <li><a href="index.php?page=drivers">Driver</a></li>
              <li><a href="index.php?page=costumers">Customer</a></li>
              <!-- <li><a href="index.php?page=colors">Colors</a></li> -->
            </ul>
          </li>
         <!--  <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th-list"></i>
              <span>Master Data</span>
              </a>
            <ul class="sub">
              <li><a href="index.php?page=categories">Categories</a></li>
              <li><a href="index.php?page=categories">Occasions</a></li>
            </ul>
          </li> -->
         <!--  <li class="">
            <a class="" href="index.php?page=categories">
              <i class="fa fa-th-list"></i>
              <span>Categories</span>
              </a>
          </li> -->
          <li class="">
            <a class="" href="index.php?page=types">
              <i class="fa fa-th-list"></i>
              <span>Types</span>
              </a>
          </li>
          <li class="">
            <a class="" href="index.php?page=categories">
              <i class="fa fa-th-list"></i>
              <span>Categories</span>
              </a>
          </li>
          <li class="">
            <a class="" href="index.php?page=cake-list">
              <i class="fa fa-th-list"></i>
              <span>Cakes</span>
              </a>
          </li>
          <li class="">
            <a class="" href="index.php?page=add-ons">
              <i class="fa fa-th-list"></i>
              <span>Add-ons</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th-list"></i>
              <span>Transactions</span>
              </a>
            <ul class="sub">
              <li><a href="index.php?page=walkin-transaction">Walkin</a></li>
              <li><a href="index.php?page=online-transaction">Online</a></li>
              <!-- <li><a href="index.php?page=colors">Colors</a></li> -->
            </ul>
          </li>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class=" fa fa-print"></i>
              <span>Reports</span>
              </a>
            <ul class="sub">
              <!-- <li><a href="index.php?page=inventory-report">Inventory</a></li> -->
              <li><a href="index.php?page=graph-report">Graph</a></li>
            </ul>
          </li>
          <?php } else { ?>
            <li class="">
              <a class="" href="index.php?page=categories">
                <i class="fa fa-th-list"></i>
                <span>Categories</span>
                </a>
            </li>
          <?php } ?>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>