<footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>FPASS</strong>. All Rights Reserved
        </p>
        <a href="#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>