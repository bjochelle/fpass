<header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <a href="#" class="logo"><b>FPA<span>SS</span></b></a>
      <div class="nav notify-row" id="top_menu">
        <ul class="nav top-menu">
          <!-- notification dropdown start-->
          <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-bell-o"></i>
              <span class="badge bg-warning"><?=countNotif()?></span>
              </a>
            <ul class="dropdown-menu extended notification">
              <div class="notify-arrow notify-arrow-yellow"></div>
              <li>
                <p class="yellow">You have <?=countNotif()?> new notifications</p>
              </li>
              <?=notifs()?>
              <li>
                
              </li>
            </ul>
          </li>
          <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-envelope"></i>
              <span class="badge bg-warning"><?=countNotif_msg()?></span>
              </a>
            <ul class="dropdown-menu extended notification">
              <div class="notify-arrow notify-arrow-yellow"></div>
              <li>
                <p class="yellow">You have <?=countNotif_msg()?> new notifications</p>
              </li>
              <?=notifs_msg()?>
              <li>
                <a href="#" onclick='window.location="index.php?page=messages&id=0"'>See all messages</a>
              </li>
            </ul>
          </li>
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="#" onclick='logout()'><span class="fa fa-power-off"></span></a></li>
        </ul>
      </div>
    </header>