<?php 
include('../core/config.php');

$ratings = $_POST['ratings'];
$comments = $_POST['comments'];
$item_id = $_POST['item_id'];
$item_type = $_POST['type'];
$trans_id = $_POST['trans_id'];
$cart_id = $_POST['cart_id'];
$date_added = date("Y-m-d H:i:s", strtotime(getCurrentDate()));
$user_id = $_SESSION['cust_user_id'];

$array = array("item_id" => $item_id, "item_type" => $item_type, "rate_val" => $ratings, "comments" => $comments,"transaction_id" => $trans_id, "cart_id" => $cart_id, "date_added" => $date_added, "user_id" => $user_id);
$insert = insert_query("tbl_ratings", $array, "N");

echo $insert;