<?php
include('../core/config.php');

$options = $_POST['options'];
$listID = $_POST['listID'];
$where = ($options == 't')?" WHERE category_id = '$listID'":(($options == 'c')?" WHERE occasion_id = '$listID'":" WHERE cake_id = '$listID'");

$sql = ($options == 't')?"SELECT * FROM tbl_category_items $where":(($options == 'c')?"SELECT * FROM tbl_occasion_items $where":"SELECT * FROM tbl_cakes $where");

$query = mysql_query($sql);
while($row = mysql_fetch_array($query)){
	$id = ($options == 't')?$row['item_id']:(($options == 'c')?$row['occasion_item_id']:$row['cake_id']);
	$item_name = ($options == 't')?$row['item_name']:(($options == 'c')?$row['occasion_item_name']:$row['cake_name']);
	$type = ($options == 't')?"C":(($options == 'c')?"O":"CE");
	$price = ($options == 't')?$row['item_price']:(($options == 'c')?$row['occasion_item_price']:$row['cake_price']);
	$img = ($options == 't')?$row['item_img']:(($options == 'c')?$row['occasion_item_img']:$row['cake_img']);

	$color = (getRemainingQuantity($id, $type) == 0)?"color:red":"";
      $remain_qntty = "<h5 style='padding: 7px;background-color: #10c2ffa6;
    border-radius: 5px;$color'>Remaining Quantity: ".getRemainingQuantity($id, $type)."</h5>";

	$discountCheck = mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$id' AND item_type = '$type' AND status = 0");
      $countRows = mysql_num_rows($discountCheck);
      $mfa_rows = mysql_fetch_array($discountCheck);
      $discount = ($mfa_rows['discount_type'] == 0)?"&#8369; ".number_format($mfa_rows['discounted_price'], 2)." Off":"".number_format($mfa_rows['discounted_price'])."% Off";
      if($countRows > 0){
        $ribbon = "<div id='ribbon-container'><a href='#'>".$discount."</a></div>";
        $hr = "<hr>";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($mfa_rows['new_price'], 2)."</h4>";

        $original_price = " <h4 style='font-weight: bolder;text-decoration: line-through;background-color: red;color: white;border-radius: 5px;'>&#8369; ".number_format($price, 2)."</h4>";
      }else{
        $ribbon = "";
        $hr = "";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($price, 2)."</h4>";

        $original_price = "";
      }
?>
<div class="col-md-3">        
  <div class="thumbnail">
      <?=$ribbon?>
      <div class="caption" style="background-color: #f3c1d4;color: black;font-weight: bolder;">
        <hr>
          <h5 style=""><?=$item_name?></h5>
      </div>
      <img src="assets/images/<?=$img?>" alt="..." style='height: 170px;width: 100%;object-fit: cover;'>
      <div class="row">
        <div class="col-md-6"><?=$original_price?></div>
        <div class="col-md-6"><?=$discounted_price?></div>
        <div class="col-md-12"><?=$remain_qntty?></div>
        <div class="col-md-12">
           <div class="input-group">
  	          <div class="input-group-addon"> Quantity: </div>
  	          <div class="input-group-addon" style="cursor: pointer;" onclick="minusQntty(<?=$id?>)"> <span class="glyphicon glyphicon-minus"></span> </div>
  	          <input type="number" min='1' max='' id='quantity<?=$id?>' value="1" readonly class="form-control" name="">
  	          <div class="input-group-addon" style="cursor: pointer;" onclick="addQntty(<?=$id?>)"> <span class="glyphicon glyphicon-plus"></span> </div>
  	        </div>
        </div>
        <div class="col-md-12">
        
          <button class="btn btn-sm btn-success btn-block" id="btn_addCart<?=$id?>" onclick='addtocart(<?=$id?>,"<?=$type?>",<?=$price?>)'><span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart</button>
     
      	</div>
      </div>
  </div>
</div>
<?php } ?>
<div class="col-md-12" style="border: 1px solid gray; margin-top: 10px"></div>
<div class="col-md-12" style="margin-top: 10px;">
  <h4 class="panel-title">
  ADD-ONS TO MAKE IT EXTRA SPECIAL</a>
</h4>
  <div class="MultiCarousel row" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
    <div class="MultiCarousel-inner">
        <?php
        $addons = mysql_query("SELECT * FROM tbl_addons");
        while($addon_row = mysql_fetch_array($addons)){
        ?>
        <div class="item">
            <div class="pad15">
                <p class="lead">
                  <img src="../admin/assets/images/<?=$addon_row['addon_img']?>" style="height: 200px;width: 100%;object-fit: cover;">
                </p>
                <p><?=$addon_row['addon_name']?></p>
                <p>&#8369; <?=$addon_row['addon_price']?></p>
                <p style="text-align: center;">
                  
                    <div class="input-group">
                      <div class="input-group-addon"> Quantity: </div>
                      <div class="input-group-addon" style="cursor: pointer;" onclick="addonminusQntty(<?=$addon_row['addon_id']?>)"> <span class="glyphicon glyphicon-minus"></span> </div>
                      <input type="number" min='1' max='' id='addons_quantity<?=$addon_row["addon_id"]?>' value="1" readonly class="form-control" name="">
                      <div class="input-group-addon" style="cursor: pointer;" onclick="addonaddQntty(<?=$addon_row['addon_id']?>)"> <span class="glyphicon glyphicon-plus"></span> </div>
                    </div>
                
                </p>
                <p style="text-align: center;">
                    <button class="btn btn-sm btn-success btn-block" id="btn_addCartAo<?=$addon_row['addon_id']?>" onclick='addtocartAddon(<?=$addon_row['addon_id']?>,"A",<?=$addon_row['addon_price']?>)'><span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart</button>
                </p>
            </div>
        </div>
        <?php } ?>
    </div>
    <button class="btn btn-primary leftLst"><span class="glyphicon glyphicon-arrow-left"></span></button>
    <button class="btn btn-primary rightLst"><span class="glyphicon glyphicon-arrow-right"></span></button>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1600) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 1392) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 1168) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
</script>