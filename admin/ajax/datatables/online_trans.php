
<?php
// Status = 0 - Pending , 1 - On Delivery, 2 - Finish 
include '../../core/config.php';

$query = mysql_query("SELECT * FROM tbl_transaction ORDER BY transaction_id DESC");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
	$transaction_id = $row['transaction_id'];
	$user_id = $row['user_id'];

	$getPrice = mysql_fetch_array(mysql_query("SELECT SUM(item_quantity * item_price) FROM `tbl_cart` WHERE transaction_id = '$transaction_id'"));

	$customer = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname, ' ', lastname) FROM tbl_users WHERE user_id = '$user_id'"));

    $list = array();
    $list['transaction_id'] = $row['transaction_id'];
    $list['count'] = $count++;
    $list['refNum'] = $row['reference_num'];
    $list['total'] = $getPrice[0];
    $list['customer'] = $customer[0];

    $list['sched'] = date("M d, Y", strtotime($row['delivery_date'])).' '.date("h:i A", strtotime($row['delivery_time']));
    $deliver = ($row['status'] == 1)?"":"display:none";
    $finish = ($row['status'] == 2)?"":"display:none";
    $receipt = ($row['status'] == 3)?"":"display:none";
    $list['action'] = "<center>
                        <li class='dropdown' style='list-style: none; font-size: 18px; color: #FFF;'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown' style='color: #607D8B;'><strong><span class='fa fa-gear'></span></strong></a>
                                <ul class='dropdown-menu'>
                                    <ul style='background: #444; border: 1px solid #333; padding: 5px 10px;width: 200px;'>
                                        <li style='list-style:none;color:#FFF;'><a href='#' onclick='viewItems(".$row['transaction_id'].")' style='color: #fff;'><span class='fa fa-eye' style='font-size: 14px;'></span> View Details </a></li>
                                        <li style='list-style:none;color:#FFF;$deliver'><a href='#' onclick='onDelivery(".$row['transaction_id'].")' style='color: #fff;'><span class='fa fa-car' style='font-size: 14px;'></span> Ready for Deliver </a></li>
                                        <li style='list-style:none;color:#FFF;$finish'><a href='#' onclick='finishTrans(".$row['transaction_id'].")' style='color: #fff;'><span class='fa fa-eye' style='font-size: 14px;'></span> Finish Transaction </a></li>
                                        <li style='list-style:none;color:#FFF;$receipt'><a href='#' onclick='printReceipt(".$row['transaction_id'].")' style='color: #fff;'><span class='fa fa-print' style='font-size: 14px;'></span> Print Receipt </a></li>
                                    </ul>
                                </ul>
                        </li>
                    </center>";

    $list['status'] = ($row['status'] == 0)?"<span style='color: orange'> PENDING </span>":(($row['status'] == 1)?"<span style='color: blue'> CHECKED OUT </span>":(($row['status'] == 2)?"<span style='color: blue'> ON DELIVERY </span>":(($row['status'] == '3')?"<span style='color: green'> FINISHED </span>":"<span style='color: red'> CANCELLED </span>")));

    array_push($response['data'],$list);
}
	echo json_encode($response);