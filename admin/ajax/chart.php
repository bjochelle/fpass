<?php

include '../core/config.php';
$type = $_GET['graphtype'];
$datefrom = date("Y-m-d", strtotime($_GET['datefrom']));
$dateto = date("Y-m-d", strtotime("+1 day", strtotime($_GET['dateto'])));

$header['series'] = array();
$data['name'] = ($type == 'd')? "Total Daily Sales" : "Total Monthly Sales";
$data['colorByPoint'] = true;
$data['data'] = array();
        if($type == 'd'){
            $date1 = new DateTime($datefrom);
            $date2 = new DateTime($dateto);

            $daterange = new DatePeriod($date1, new DateInterval('P1D'), $date2);

            foreach ($daterange as $dates) {
                $day = $dates->format('d');
                $name = $dates->format('M d, Y');
                $list = array();
                $list['name'] = $name;
                $query = mysql_query("SELECT IF(w.wt IS NULL,0,w.wt) + IF(o.ot IS NULL,0,o.ot) as total FROM
                        (SELECT sum(total_payment) as wt FROM tbl_walkin_transaction WHERE status = 3 AND DAY(delivery_date) = '$day') as w,
                        (SELECT sum(total_payment) as ot FROM tbl_transaction WHERE status = 3 AND DAY(delivery_date) = '$day') as o");
                $row2 = mysql_fetch_array($query);

                if($row2[0] == 0 or $row2[0] == null or $row2[0] == '')
                {
                    $total = 0;
                }
                    else
                {   
                    $total = $row2[0] * 1;
                }
                    
                    $list['y'] = round($total);
                    array_push($data['data'], $list);
            }
        }else{
            $array = array("January" => 1, "February" => 2,"March" => 3, "April" => 4,"May" => 5, "June" => 6,"July" => 7, "August" => 8,"September" => 9, "October" => 10,"November" => 11, "December" => 12);
            foreach ($array as $key => $value) {
                $list = array();
                $list['name'] = $key;

                $query = mysql_query("SELECT IF(w.wt IS NULL,0,w.wt) + IF(o.ot IS NULL,0,o.ot) as total FROM
                        (SELECT sum(total_payment) as wt FROM tbl_walkin_transaction WHERE status = 3 AND MONTH(delivery_date) = '$value') as w,
                        (SELECT sum(total_payment) as ot FROM tbl_transaction WHERE status = 3 AND MONTH(delivery_date) = '$value') as o");
                $row2 = mysql_fetch_array($query);

                if($row2[0] == 0 or $row2[0] == null or $row2[0] == '')
                {
                    $total = 0;
                }
                    else
                {   
                    $total = $row2[0] * 1;
                }
                    
                    $list['y'] = round($total);
                    array_push($data['data'], $list);
                
                
            }
        }
       
        array_push($header['series'], $data);
		echo json_encode($header);
?>