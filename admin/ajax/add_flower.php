<?php 
include('../core/config.php');

$flowerName = clean($_POST['flowerName']);
$categoryID = $_POST['categoryID'];
$desc = clean($_POST['desc']);

if(is_array($_FILES)) {
    if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
        $sourcePath = $_FILES['avatar']['tmp_name'];
        $targetPath = "../assets/images/".$_FILES['avatar']['name'];
        if(move_uploaded_file($sourcePath,$targetPath)) {
            $img = $_FILES['avatar']['name'];
            $array_data = array(
                                "flower_img" => $img,
                                "flower_name" => $flowerName,
                                "flower_desc" => $desc,
                                "flower_cat" => $categoryID

            );
            $result = insert_query("tbl_flower",$array_data,"N");

           
        }
    }
}
echo $result;