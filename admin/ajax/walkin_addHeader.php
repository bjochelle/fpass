<?php
include('../core/config.php');

$refnum = $_POST['refnum'];
$cust_name = $_POST['cust_name'];
$message = $_POST['message'];
$delivery_date = $_POST['delivery_date'];
$delivery_time = $_POST['delivery_time'];
$contact_no = $_POST['contact_no'];
$delivery_address = $_POST['delivery_address'];

$restrict_period = str_replace(".","",$delivery_address);
$formattedAddr = str_replace(' ','+',$restrict_period);

$geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0');

$output= json_decode($geocode);

$addr_latitude = $output->results[0]->geometry->location->lat;
$addr_longitude = $output->results[0]->geometry->location->lng;

$array = array("costumer" => $cust_name, "reference_num" => $refnum, "delivery_date" => $delivery_date, "delivery_time" => $delivery_time, "contact_no" => $contact_no, "address_name" => $delivery_address, "delivery_lat" => $addr_latitude, "delivery_long" => $addr_longitude, "message" => $message, "total_payment" => 0, "status" => 0);

$insert = insert_query("tbl_walkin_transaction", $array , "Y");

echo $insert;