<?php 
$cart_count = countCartItems($userID);
// $getName = mysql_fetch_array(mysql_query("SELECT * FROM tbl_cakes WHERE cake_id = '$item_id'"));
?>
<style type="text/css">
  /* image thumbnail */
.thumb {
    display: block;
  width: 100%;
  margin: 0;
}

/* Style to article Author */
.by-author {
  font-style: italic;
  line-height: 1.3;
  color: #aab6aa;
}

/* Main Article [Module]
-------------------------------------
* Featured Article Thumbnail
* have a image and a text title.
*/
.featured-article {
  width: 482px;
  height: 350px;
  position: relative;
  margin-bottom: 1em;
}

.featured-article .block-title {
  /* Position & Box Model */
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: 1;
  /* background */
  background: rgba(0,0,0,0.7);
  /* Width/Height */
  padding: .5em;
  width: 100%;
  /* Text color */
  color: #fff;
}

.featured-article .block-title h2 {
  margin: 0;
}

/* Featured Articles List [BS3]
--------------------------------------------
* show the last 3 articles post
*/

.main-list {
  padding-left: .5em;
}

.main-list .media {
  padding-bottom: 1.1em;
  border-bottom: 1px solid #e8e8e8;
}

</style>
<input type="hidden" id="itemID" value="<?=$item_id?>" name="">
<div class="container">  
<div class="container text-center">    
  <h3> My Cart </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
<div class="row">
  <?php 
  if($cart_count > 0){
  ?>
  <div class="col-md-7">
    <ul class="media-list main-list">
      <?php 
        $curdate = date("Y-m-d", strtotime(getCurrentDate()));
        $cart = mysql_query("SELECT * FROM tbl_cart WHERE user_id = '$userID' AND status = 0");
        $totalPrice = 0;
        while($row = mysql_fetch_array($cart)){
          $itemid = $row['item_id'];
          $price = $row['item_price'] * $row['item_quantity'];
          $cat = $row['item_type'];
          $totalPrice += $price;
          $sql = ($row['item_type'] == 'C')?"SELECT item_img,item_name,item_price FROM tbl_category_items WHERE item_id = '$itemid'":(($row['item_type'] == 'O')?"SELECT occasion_item_img,occasion_item_name,occasion_item_price FROM tbl_occasion_items WHERE occasion_item_id = '$itemid'":(($row['item_type'] == 'CE')?"SELECT cake_img,cake_name,cake_price FROM tbl_cakes WHERE cake_id = '$itemid'":"SELECT addon_img, addon_name, addon_price FROM tbl_addons WHERE addon_id = '$itemid'"));

          $query = mysql_fetch_array(mysql_query($sql));

          $check_discount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemid' AND item_type = '$cat' AND status = 0 AND discount_duration_date >= '$curdate'"));

          $itmPrc = (!empty($check_discount['item_id']))?$check_discount['new_price']:$query[2];
      ?>
        <li class="media" style="border-right: 2px solid #faa4c4;">
          <a class="pull-left" href="#">
            <img style='width: 200px;height: 200px;object-fit: cover;border-radius: 5px' class="media-object" src="../admin/assets/images/<?=$query[0]?>" alt="...">
            <button class="btn btn-sm btn-block btn-danger" onclick='deleteItemCart(<?=$row['cart_id']?>)' id="deleteItemCart<?=$row['cart_id']?>"><span class="glyphicon glyphicon-trash"></span> Delete</button>
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?=$query[1]?></h4>
            <h4 class="media-heading">&#8369; <?=$itmPrc?></h4>
            <div class="col-md-6">
               <div class="input-group">
                  <div class="input-group-addon" style="cursor: pointer;" onclick="update_qntty(<?=$row['cart_id']?>,'M')"><span class="glyphicon glyphicon-minus"></span></div>
                  <input type="number" value='<?=$row["item_quantity"]?>' class="form-control" name="" id='cart_qntty' readonly>
                  <div class="input-group-addon" style="cursor: pointer;" onclick="update_qntty(<?=$row['cart_id']?>,'A')"><span class="glyphicon glyphicon-plus"></span></div>
                </div>
            </div>
           
          </div>
        </li>
      <?php } ?>
      </ul>
  </div>
  <div class="col-md-5">
    <div class="input-group">
      <div class="input-group-addon">
        Delivery Date
      </div>
      <input type="date" class='form-control' id="delivery_date" name="">
    </div>
    <br>
    <div class="input-group">
      <div class="input-group-addon">
        Delivery Time
      </div>
      <select class='form-control' id="delivery_time">
        <?=getTimeAvail();?>
      </select>
    </div>
    <br>
    <div class="input-group">
      <div class="input-group-addon">
        Personal Message (Optional)
      </div>
      <textarea style="resize: none;" rows="5" id="personal_message" class="form-control"></textarea>
    </div>
    <br>
    <div class="input-group">
      <div class="input-group-addon">
        Sender Name (Optional)
      </div>
      <input type="text" class='form-control' id="sender_name" name="">
    </div>
    
    <br>
    <!-- <div class="input-group">
      <div class="input-group-addon"> Delivery Location </div>
      <div class="input-group-addon"><input onchange='locateME()' type="radio" name="locateMe" class='locateMe' value='rl' id='locateMe'> Use My registered location </div>
      <div class="input-group-addon"><input onchange='locateME()' type="radio" name="locateMe" class='locateMe' value='pl' id='locateMe'> Pin My location </div>
   
     <input type="hidden" name="lat" id='lat'>
     <input type="hidden" name="long" id='long'>
   </div> -->
   <br>
   <div class="col-md-12">
     <label class="alert alert-info">Total Price : &#8369; <?=number_format($totalPrice, 2); ?></label>
     <input type="hidden" id="totalpayment" value="<?=$totalPrice?>" name="">
   </div>
    
   <br>
    <div class="">
      <button class="btn btn-block btn-success pull-right" onclick='continuetodelivery()'><span class="glyphicon glyphicon-check"></span> Continue to delivery location </button>
      <button class="btn btn-block btn-success pull-right" onclick='window.location="../index.php"'><span class="glyphicon glyphicon-arrow-left"></span> Continue Shopping </button>
    </div>
  </div>
  <?php } else { ?>
    <h3>No Items in Cart</h3>
  <?php } ?>
</div>
</div><br>
<div class='modal fade' id='map_modal' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        
      </div>
      <div class='modal-body'>
          <div id="map"></div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success btn-simple" data-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function deleteItemCart(id){
      $("#deleteItemCart"+id).prop("disabled", true);
      $("#deleteItemCart"+id).html("<span class='glyphicon glyphicon-refresh'></span> Loading");
      $.post("../admin/ajax/deleteIteminCart.php", {
        id: id
      }, function(data){
        if(data > 0){
          swal({
            title: "All Good!",
            text: "Item Successfully Deleted",
            type: "success"
          }, function(){
            window.location.reload();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      });
  }
  function update_qntty(id,action){
      $.post("../admin/ajax/update_quantity.php", {
        action: action,
        id: id
      }, function(data){
        if(data == 1){
          swal({
            title: "All Good!",
            text: "Quantity Successfully Updated",
            type: "success"
          }, function(){
            window.location.reload();
          });
        }else if(data == 3){
          swal({
            title: "Aw Snap!",
            text: "Exceeded to the remaining quantity",
            type: "warning"
          }, function(){
            swal.close();
          });
        }else if(data == 4){
          swal({
            title: "Aw Snap!",
            text: "Must not less than 1",
            type: "warning"
          }, function(){
            swal.close();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      })
    
  }
  function continuetodelivery(){
    var delivery_date = $("#delivery_date").val();
    var delivery_time = $("#delivery_time").val();
    var personal_message = $("#personal_message").val();
    var sender_name = $("#sender_name").val();
    var totalpayment = $("#totalpayment").val();
    if(delivery_date == '' || delivery_time == ''){
     
          swal({
            title: "Aw Snap!",
            text: "Unable to checkout, Please check the other fields if empty",
            type: "warning"
          }, function(){
            swal.close();
          });
       
    }else{
      $.post("../admin/ajax/checkout_cart.php", {
        delivery_date: delivery_date,
        delivery_time: delivery_time,
        personal_message: personal_message,
        sender_name: sender_name,
        totalpayment: totalpayment
      }, function(data){
        if(data > 0){
          swal({
            title: "Almost Done!",
            text: "Please complete the next step",
            type: "success"
          }, function(){
            window.location = 'item_view.php?view=delivery-location&id='+data;
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      });
    }
  }
  function locateME(){
    var optionChecked = $("input[name='locateMe']:checked").val();
    if(optionChecked == 'rl'){
      getRegisteredLocation();
    }else{
      showMap();
    }
  }

  function getRegisteredLocation(){
    $.ajax({
      url:"../ajax/getLatLangRL.php",
      type:"POST",
      success:function(data){
        var splt_data = data.split("-");
        $("#lat").val(splt_data[0]);
        $("#long").val(splt_data[1]);
      }
    });
  }

  function showMap(){
    $("#map_modal").modal();
    showPosition();
  }

</script>
