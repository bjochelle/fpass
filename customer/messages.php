<?php
  $receiver = mysql_fetch_array(mysql_query("SELECT user_id FROM tbl_users WHERE category = 0"));
?>
<style type="text/css">
		    .cg :hover {
      background-color: #80868e;
  }

*,
*::before,
*::after {
  margin: 0;
  border: 0;
  padding: 0;
  word-wrap: break-word;
  box-sizing: border-box;
}

.message-sent,
.message-received {
  clear: both;
}
.message-sent::before,
.message-received::before,
.message-sent::after,
.message-received::after {
  content: '';
  display: table;
}

[class^='grid-'] {
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}
[class^='grid-'] {
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
}
.grid-message >[class^='col-'] {
  margin-top: 1em;
  margin-right: 1em;
}
.grid-message >[class^='col-']:nth-child(-n + 1) {
  margin-top: 0;
}
.grid-message >[class^='col-']:nth-child(1n) {
  margin-right: 0;
}
.col-message-sent {
  margin-left: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-received {
  margin-right: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-sent,
.col-message-received {
  width: calc(91.66666667% - 0.08235677em);
}
.message-sent,
.message-received {
  margin-top: 0.0625em;
  margin-bottom: 0.0625em;
  padding: 0.25em 1em;
}
.message-sent p,
.message-received p {
  margin: 0;
  line-height: 1.5;
}
.message-sent {
  float: right;
  color: white;
  background-color: gray;/*dodgerblue;*/
  border-radius: 1em 0.25em 0.25em 1em;
}
.message-sent:first-child {
  border-radius: 1em 1em 0.25em 1em;
}
.message-sent:last-child {
  border-radius: 1em 0.25em 1em 1em;
}
.message-sent:only-child {
  border-radius: 1em;
}
.message-received {
  float: left;
  color: black;
  background-color: #c5c5c5;
  border-radius: 0.25em 1em 1em 0.25em;
}
.message-received:first-child {
  border-radius: 1em 1em 1em 0.25em;
}
.message-received:last-child {
  border-radius: 0.25em 1em 1em 1em;
}
.message-received:only-child {
  border-radius: 1em;
}
.col-message-sent {
  margin-top: 0.25em !important;
}
.col-message-received {
  margin-top: 0.25em !important;
}
.message {
  min-height: 53.33203125em;
  max-width: 30em; /* !!!!!!!!!! COMMENT OUT THIS LINE TO MAKE IT FULL WIDTH !!!!!!!!!! */
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}
.btn_hover:hover{
  background-color: #80868e;
  color: white;
}


</style>
<div class="container">
  <div class="container text-center">    
    <h3> Messages </h3><br>
  </div>
  <div class="col-md-12" style="border: 1px solid #d53974;"></div><br>
  <div class="row">
    <div class="col-md-12"> 
      <div class="">
        <div class="" style="height:350px;padding: 0px;border: 1px solid #ccc;">
        <!-- DIRECT CHAT -->
        <div class="box box-warning direct-chat direct-chat-warning" style="height:100%;border-top: 0px;background-image: linear-gradient(to right, #faa4c4 , white);box-shadow: -3px 3px 5px #faaac8ad;">
        <!-- /.box-header -->
        <div class="box-body" style="height: 100%;">
          <div class='col-md-12' style="background-color: #c5c5c5;"></div>
          <!-- Conversations are loaded here -->
          <div class="grid-message" style="overflow-x: auto;height: 90%;padding: 10px;" id="msg">

          </div>
          <!--/.direct-chat-messages-->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="input-group">
            <input type="hidden" id="sender_id" value="<?php echo $userID;?>">
            <input type="hidden" id="receiver_id" value='<?php echo $receiver[0]; ?>'>
            <!-- <input type="file" class="" id="send_btn" onclick="uploadimg()"> <span class="fa fa-paperclip"></span></button> -->
            
            <textarea style="resize: none;" rows="1" id="chatContent" row="3" name="message"  autocomplete="off" placeholder="Type a message ..." class="form-control"></textarea>
            <span class="input-group-btn">
             <label for="files" class="btn default" style="border:1px solid #d4d4d4"><i class="fa fa-file-image-o" ></i></label>
               <button type="button" class="btn btn-primary btn-flat" id="send_btn" onclick="sendMsg()"><span class="fa fa-send"></span> Send</button>
            </span>
          </div>
        </div>
        <!-- /.box-footer-->
        </div>
        </div>
      </div>
    </div>
  </div>
  <form id="attachImg" method="POST" action="" enctype="multipart/form-data">
      <div class="modal fade" id="attachImg_modal" tabindex="-1" role="dialog" aria-labelledby="attachImg_modalLabel" data-backdrop='static'>
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title"><span class='fa fa-plus-circle'></span> Upload Image </h4>
                  </div>
                  <div class="modal-body">
                      <div class='row'>
                          <div class='col-md-12' style='text-align: center'>
                              <img id="img_wrap" alt='Recent 2x2 Picture' class="image-wrap previewImage01" style="object-fit: contain;width: 100%;height:200px;">
                              <input style="display: none;" type="file" name="avatar" id="files" class="btn-inputfile share" accept="image/x-png,image/gif,image/jpeg" />
                              <input type="hidden" name='attached_receiver' id="attached_receiver" value='<?php echo $receiver[0]; ?>'>
                          </div>
                          <div class='col-md-12' style="margin-top: 10px">
                             
                                  <div class="input-group">
                                      <span class="input-group-addon">
                                        Description:
                                      </span>
                                     <textarea class="form-control" style="resize: none;" rows="3" name='attach_img_desc' id="attach_img_desc" placeholder="Write something here..."></textarea>
                                  </div>
                              
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <span class="btn-group">
                <button class="btn btn-primary btn-sm" id="btn_attach_img" type="submit"><span class="fa fa-upload"></span> Upload</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
              </span>
                  </div>
              </div>
          </div>
      </div>
      </form>
      <div class="modal fade" id="view_attachImg" tabindex="-1" role="dialog" aria-labelledby="view_attachImgLabel" data-backdrop='static'>
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title"><span class='fa fa-eye'></span> Attached Image </h4>
                  </div>
                  <div class="modal-body">
                      <div class='row'>
                          <div class='col-md-12' style='text-align: center'>
                              <div id="img_wwrap">
                                
                              </div>
                              <input type="hidden" name='attached_receiver' id="attached_receiver" value='<?php echo $receiver[0]; ?>'>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <span class="btn-group">
                <!-- <button class="btn btn-primary btn-sm" id="del_attach_img" type="button"><span class="fa fa-trash"></span> Delete</button> -->
                <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
              </span>
                  </div>
              </div>
          </div>
      </div>
</div><br><br><br>  
<script type="text/javascript">
  
  $(document).ready( function(){
    var receiver_id = $("#receiver_id").val();
    loadChat(receiver_id);

    $("#attachImg").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"../admin/ajax/attach_img_chat.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
              $("#attachImg_modal").modal('hide');
             loadChat(receiver_id);
            },
             error: function() 
         {
          failedAlert();
         }           
       });
        
    }));
  });
  $(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);
      
    }, false);
    if (file) {
      reader.readAsDataURL(file);
      var files = $("#files").val();
      $("#attach_img_upload").val(files);
      $("#attachImg_modal").modal();
    }
  }
  function viewImg(message_id){
    $("#view_attachImg").modal();
    $.post("../admin/ajax/get_attached_img.php", {
      message_id: message_id
    }, function(data){
        $("#img_wwrap").html(data);
    })
  }
  $(document).keypress(function(e) {
    if(e.which == 13) {
      sendMsg();
    }
     var receiver_id = $("#receiver_id").val();
     loadChat(receiver_id);
  });

  function scrolling(){
    var objDiv = document.getElementById("msg");
    objDiv.scrollTop = objDiv.scrollHeight;
  }
  function loadChat(r_id){
  // var interval = setInterval( function(){
    viewMsg(r_id);
    setTimeout(scrolling, 500);
    //}, 10000);
  }
  function viewMsg(rec_id){
    var sender_id = $("#sender_id").val();
    $.post('../admin/ajax/getMessage.php', {
    rec_id: rec_id,
    sender_id:sender_id
    },
    function(data){

    var msg = JSON.parse(data);
    $("#msg").html(msg.msg_content);
    });
  }

  function sendMsg(){
    var sender_id = $("#sender_id").val();
    var receiver_id = $("#receiver_id").val();
    var chatContent = $("#chatContent").val();
    $("#send_btn").prop("disabled", true);
    $("#send_btn").html("<span class='fa fa-spin fa-spinner'></span>");
    if(sender_id == '' || receiver_id == '' || chatContent == ''){
    alert('Failed');
    }else{
    $.post('../admin/ajax/send_message.php', {
    sender_id: sender_id,
    receiver_id: receiver_id,
    chatContent: chatContent
    }, function(data){
    if(data != 0){
      $("#chatContent").val("");
      loadChat(data);
    }else{
      alert('Failed');
    }
    setTimeout(scrolling, 10);
    $("#send_btn").prop("disabled", false);
    $("#send_btn").html("<span class='fa fa-send'></span> Send");
    });
    }
  }


  function updateScroll(){
    var element = document.getElementById("msg");
    element.scrollTop = element.scrollHeight;
  }
</script>