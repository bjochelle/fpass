<?php 
$transID = $_GET['id'];
$getPrice = mysql_fetch_array(mysql_query("SELECT * FROM tbl_transaction WHERE transaction_id = '$transID'"));
?>
<style type="text/css">
#map {
  height: 450px;;  /* The height is 400 pixels */
  width: 100%;  /* The width is the width of the web page */
 }
</style>
<input type="hidden" id="itemID" value="<?=$item_id?>" name="">
<div class="container">  
<div class="container text-center">    
  <h3> Delivery Location </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
  <div class="row">
    <div class="col-md-7">
      <label class="alert alert-info"><h5>Total Payment: &#8369; <b id="fnl_amnt"><?=number_format($getPrice['total_payment'], 2)?></b></h5>
        <div class="input-group">
        <div class="input-group-addon">
          Promo Code:
        </div>
        <select class="form-control" id="promoCode" onchange='minusPromoCode()' disabled>
          <?=getActivePromoCode($userID)?>
        </select>
        <div class="input-group-addon">
          <input type="checkbox" id="usepcode" onchange='usepromocode()' name="usepcode"> Use Promo Code
        </div>
        <input type="hidden" name="" id="tmp_amnt" value="<?=$getPrice['total_payment']?>">
      </div>
      </label>
      
    </div>
    <div class="col-md-6" style="padding-top: 10px">
      <div class="input-group">
        <div class="input-group-addon">
          Payment Mode
        </div>
        <select class="form-control" id="payment_mode">
          <option value=""> &mdash; Please Choose &mdash; </option>
          <option value="Gcash"> Gcash </option>
          <option value="Remittance"> Remittance </option>
          <option value="COD"> Cash on Delivery </option>
        </select>
      </div>
    </div>
    <div class="col-md-6" style="padding-top: 10px">
      <div class="input-group">
        <div class="input-group-addon">
          Location
        </div>
        <select class="form-control" id="delivery_location" onchange='getDeliveryLocation()'>
          <option value=""> &mdash; Please Choose &mdash; </option>
          <option value="r"> Use My Registerd Location </option>
          <option value="p"> Pin My Location </option>
        </select>
      </div>
    </div>
    <hr>
    <input type="hidden" id="lat" name="">
    <input type="hidden" id="long" name="">
    <input type="hidden" id="transID" value="<?=$transID?>" name="">
    <div class="col-md-12" id="map_cont" style="display: none;margin-top: 20px">
      <div id="map">
        
      </div>
    </div>
    <div class="col-md-12" style="padding-top: 10px">
      <button class="btn btn-success btn-sm pull-right" id="checkout_finish" onclick='checkout_finish()'><span class="glyphicon glyphicon-check"></span> Checkout </button>
    </div>
  </div>
</div><br>
<script type="text/javascript">
  function usepromocode(){
    var tmp_amnt = $("#tmp_amnt").val();
    if($("#usepcode").is(':checked')){
      $("#promoCode").attr("disabled", false);
    }else{
      $("#promoCode").attr("disabled", true);
      $("#promoCode").val("");
      $("#fnl_amnt").html(tmp_amnt);
    }
  }
  function minusPromoCode(){
    var codeid = $("#promoCode").val();
    var tmp_amnt = $("#tmp_amnt").val();
    $.post("../admin/ajax/getFinalAmnt.php", {
      codeid: codeid,
      tmp_amnt: tmp_amnt
    }, function(data){
      $("#fnl_amnt").html(data);
    })

  }
  function checkout_finish(){
    var payment_mode = $("#payment_mode").val();
    var delivery_location = $("#delivery_location").val();
    var lat = $("#lat").val();
    var long = $("#long").val();
    var transID = $("#transID").val();
    var fnl_amnt = $("#fnl_amnt").text();
    var promoCode = $("#promoCode").val();
    var check_c = ($("#usepcode").is(':checked')) ? 1 : 0;
    alert(promoCode)
    if(payment_mode == '' || delivery_location == ''){
      swal("Unable to checkout. Please Check other fields if not empty");
    }else{
      $("#checkout_finish").prop("disabled", true);
      $("#checkout_finish").html("<span class='glyphicon glyphicon-refresh'></span> Loading");
      $.post("../admin/ajax/finish_checkout.php", {
        payment_mode: payment_mode,
        lat: lat,
        long: long,
        transID: transID,
        fnl_amnt: fnl_amnt,
        check_c: check_c,
        promoCode: promoCode
      }, function(data){
        if(data > 0){
          swal({
            title: "All Good!",
            text: "Checkout Successfully Finished",
            type: "success"
          }, function(){
            window.location = "../index.php";
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      })
    }
   
    
  }
 function getDeliveryLocation(){
  var delivery_location = $("#delivery_location").val();
  if(delivery_location == 'r'){
    getRegisteredLocation();
    $("#map_cont").css("display","none");
  }else{
    $("#map_cont").css("display","block");
  }
 }
 function getRegisteredLocation(){
  $.post("../admin/ajax/getRegisteredLocation.php", {

  }, function(data){
    var loc = data.split('-');
    $("#lat").val(loc[0]);
    $("#long").val(loc[1]);
  })
 }

   var position = [10.6840, 122.9563];

  function initialize() { 
      var latlng = new google.maps.LatLng(position[0], position[1]);
      var myOptions = {
          zoom: 20,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("map"), myOptions);

      marker = new google.maps.Marker({
          position: latlng,
          map: map,
          title: "Latitude:"+position[0]+" | Longitude:"+position[1]
      });

      google.maps.event.addListener(map, 'click', function(event) {
          var result = [event.latLng.lat(), event.latLng.lng()];
          transition(result);
      });
  }



  var numDeltas = 100;
  var delay = 10; //milliseconds
  var i = 0;
  var deltaLat;
  var deltaLng;

  function transition(result){
      i = 0;
      deltaLat = (result[0] - position[0])/numDeltas;
      deltaLng = (result[1] - position[1])/numDeltas;
      moveMarker();
      $("#lat").val(position[0]);
      $("#long").val(position[1]);
  }

  function moveMarker(){
      position[0] += deltaLat;
      position[1] += deltaLng;
      var latlng = new google.maps.LatLng(position[0], position[1]);
      marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
      marker.setPosition(latlng);
      if(i!=numDeltas){
          i++;
          setTimeout(moveMarker, delay);
      }
  }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initialize"> </script>