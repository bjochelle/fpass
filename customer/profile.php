<?php
$id = $_GET['id'];

$getDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$id'"));
?>
<style type="text/css">
  .thumb {
    display: block;
  width: 100%;
  margin: 0;
}

/* Style to article Author */
.by-author {
  font-style: italic;
  line-height: 1.3;
  color: #aab6aa;
}

/* Main Article [Module]
-------------------------------------
* Featured Article Thumbnail
* have a image and a text title.
*/
.featured-article {
  width: 482px;
  height: 350px;
  position: relative;
  margin-bottom: 1em;
}

.featured-article .block-title {
  /* Position & Box Model */
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: 1;
  /* background */
  background: rgba(0,0,0,0.7);
  /* Width/Height */
  padding: .5em;
  width: 100%;
  /* Text color */
  color: #fff;
}

.featured-article .block-title h2 {
  margin: 0;
}

/* Featured Articles List [BS3]
--------------------------------------------
* show the last 3 articles post
*/

.main-list {
  padding-left: .5em;
}

.main-list .media {
  padding-bottom: 1.1em;
  border-bottom: 1px solid #e8e8e8;
}

h1 span{
  font-weight: 300;
  color: #Fd4;
}

div.stars{
  width: 100%;
  display: inline-block;
}

input.star{
  display: none;
}

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content:'\e006';
  color: #FD4;
  transition: all .25s;
}


input.star-5:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before {
  color: #F62;
}

label.star:hover{
  transform: rotate(-15deg) scale(1.3);
}

label.star:before{
  content:'\e006';
  font-family: Glyphicons Halflings;
}

.rev-box{
  overflow: hidden;
  height: 0;
  width: 100%;
  transition: all .25s;
}

/*textarea.review1{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}*/

label.review1{
  display: block;
  transition:opacity .25s;
}

/*textarea.review{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}*/

label.review{
  display: block;
  transition:opacity .25s;
}



input.star:checked ~ .rev-box{
  height: 125px;
  overflow: visible;
}

</style>
<div class="container">  
<div class="container text-center">    
  <h3> Profile </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
  <div class="row">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" style="color: #333333;" href="#home"> Personal Information </a></li>
    <li><a data-toggle="tab" href="#menu1" style="color: #333333;"> Pending Transactions </a></li>
    <li><a data-toggle="tab" href="#menu2" style="color: #333333;"> Finished Transactions </a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Firstname </span>
            <input type="text" id="firstname" value="<?=$getDetails['firstname']?>" class="form-control" name="">
          </div>
        </div>
        <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Middlename </span>
            <input type="text" id="middlename" value="<?=$getDetails['middlename']?>" class="form-control" name="">
          </div>
        </div>
        <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Lastname </span>
            <input type="text" id="lastname" value="<?=$getDetails['lastname']?>" class="form-control" name="">
          </div>
        </div>
        <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Contact # </span>
            <input type="text" id="connum" value="<?=$getDetails['contact_no']?>" class="form-control" name="">
          </div>
        </div>
        <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Email Address </span>
            <input type="text" id="emailadd" value="<?=$getDetails['email_address']?>" class="form-control" name="">
          </div>
        </div>
        <div class="col-md-4" style="padding-top: 10px">
          <div class="input-group">
            <span class="input-group-addon"> Address </span>
            <textarea class="form-control" id="address" rows="2" style="resize: none"><?=$getDetails['address']?></textarea>
          </div>
        </div>
        <div class="col-md-12" style="padding-top: 10px">
          <button class="btn btn-sm btn-primary pull-right" id="updateBtn" onclick='updateProfile()'><span class="glyphicon glyphicon-check"></span> Save Changes </button>
        </div>
        </div>
      </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="row">
        <div class="col-md-12">
      <?php
        $getTransaction = mysql_query("SELECT * FROM tbl_transaction WHERE user_id = '$userID' AND status < 3");
        $counter = mysql_num_rows($getTransaction);
        if($counter > 0){
        while($t_row = mysql_fetch_array($getTransaction)){
          $trasactionid = $t_row['transaction_id'];
          $status = ($t_row['status'] == 0)?"<span style='color: orange'> PENDING </span>":(($t_row['status'] == 1)?"<span style='color: blue'> CHECKED OUT </span>":(($t_row['status'] == 2)?"<span style='color: blue'> ON DELIVERY </span>":(($t_row['status'] == '3')?"<span style='color: green'> FINISHED </span>":"<span style='color: red'> CANCELLED </span>")));
      ?>
        <div class="col-md-4" style="padding-top: 20px">
          <label class="alert alert-info">Reference Number: <?=$t_row['reference_num']?></label>
        </div>
        <div class="col-md-4 " style="padding-top: 20px">
          <label class="alert alert-info">Status: <?=$status?>
             <button class="btn btn-success btn-sm" onclick='showLocation(<?=$trasactionid?>)'><span class="glyphicon glyphicon-eye-open"></span> Show Location</button>
          </label>

        </div>
        <div class="col-md-4 " style="padding-top: 20px">
          <label class="alert alert-info">Total Payment: &#8369; <?=number_format($t_row['total_payment'], 2)?></label>
        </div>
        <div class="col-md-12" style="padding-top: 20px">
          <label class="alert alert-info"><h5 style='color: red'>Note: Please Click this when you receive the items </h5> <button class="btn btn-sm btn-success" id="finishBtn<?=$trasactionid?>" onclick='finishTrans(<?=$trasactionid?>)'><span class="glyphicon glyphicon-check"></span> I Already Received the Items </button></label>
        </div>
         <div class="col-md-12">
      <ul class="media-list main-list">
        <?php 
          $cart = mysql_query("SELECT * FROM tbl_cart WHERE user_id = '$userID' AND status = 1 AND transaction_id = '$trasactionid'");
          $totalPrice = 0;
          while($row = mysql_fetch_array($cart)){
            $itemid = $row['item_id'];
            $price = $row['item_price'] * $row['item_quantity'];
            $cat = $row['item_type'];
            $totalPrice += $price;
            $sql = ($row['item_type'] == 'C')?"SELECT item_img,item_name,item_price FROM tbl_category_items WHERE item_id = '$itemid'":(($row['item_type'] == 'O')?"SELECT occasion_item_img,occasion_item_name,occasion_item_price FROM tbl_occasion_items WHERE occasion_item_id = '$itemid'":(($row['item_type'] == 'CE')?"SELECT cake_img,cake_name,cake_price FROM tbl_cakes WHERE cake_id = '$itemid'":"SELECT addon_img, addon_name, addon_price FROM tbl_addons WHERE addon_id = '$itemid'"));

            $query = mysql_fetch_array(mysql_query($sql));

            $check_discount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemid' AND item_type = '$cat' AND status = 0"));

            $itmPrc = (!empty($check_discount['item_id']))?$check_discount['new_price']:$query[2];
        ?>
          <li class="media" style="border-right: 2px solid #faa4c4;">
            <a class="pull-left" href="#">
              <img style='width: 200px;height: 200px;object-fit: cover;border-radius: 5px' class="media-object" src="../admin/assets/images/<?=$query[0]?>" alt="...">
            </a>
            <div class="media-body">
              <h4 class="media-heading"><?=$query[1]?></h4>
              <h4 class="media-heading">&#8369; <?=$itmPrc?></h4>
              <div class="col-md-6">
                 <div class="input-group">
                    <div class="input-group-addon" style="cursor: pointer;"> Quantity </div>
                    <input type="number" value='<?=$row["item_quantity"]?>' class="form-control" name="" id='cart_qntty' readonly>
                  </div>
              </div>
             
            </div>
          </li>
        <?php } ?>
        </ul>
    </div>
      <?php } }else{ ?>
        <div class="col-md-12">
          <label><h3>NO PENDING ITEMS</h3></label>
        </div>
      <?php } ?>
        </div>
      </div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <div class="row">
        <div class="col-md-12">
      <?php
        $getTransaction = mysql_query("SELECT * FROM tbl_transaction WHERE user_id = '$userID' AND status = 3");
        $counter = mysql_num_rows($getTransaction);
        if($counter > 0){
        while($t_row = mysql_fetch_array($getTransaction)){
          $trasactionid = $t_row['transaction_id'];
          $status = ($t_row['status'] == 0)?"<span style='color: orange'>PENDING</span>":(($t_row['status'] == 1)?"<span style='color: blue'>ON DELIVERY</span>":"<span style='color: green'>FINISHED</span>");
      ?>
        <div class="col-md-4" style="padding-top: 20px">
          <label class="alert alert-info">Reference Number: <?=$t_row['reference_num']?></label>
        </div>
        <div class="col-md-4 " style="padding-top: 20px">
          <label class="alert alert-info">Status: <?=$status?></label>
        </div>
        <div class="col-md-4 " style="padding-top: 20px">
          <label class="alert alert-info">Total Payment: &#8369; <?=number_format($t_row['total_payment'], 2)?></label>
        </div>
         <div class="col-md-12">
          <ul class="media-list main-list">
        <?php 
          $cart = mysql_query("SELECT * FROM tbl_cart WHERE user_id = '$userID' AND status = 1 AND transaction_id = '$trasactionid'");
          $totalPrice = 0;
          while($row = mysql_fetch_array($cart)){
            $cartID = $row['cart_id'];
            $itemid = $row['item_id'];
            $price = $row['item_price'] * $row['item_quantity'];
            $cat = $row['item_type'];
            $totalPrice += $price;
            $sql = ($row['item_type'] == 'C')?"SELECT item_img,item_name,item_price FROM tbl_category_items WHERE item_id = '$itemid'":(($row['item_type'] == 'O')?"SELECT occasion_item_img,occasion_item_name,occasion_item_price FROM tbl_occasion_items WHERE occasion_item_id = '$itemid'":(($row['item_type'] == 'CE')?"SELECT cake_img,cake_name,cake_price FROM tbl_cakes WHERE cake_id = '$itemid'":"SELECT addon_img, addon_name, addon_price FROM tbl_addons WHERE addon_id = '$itemid'"));

            $query = mysql_fetch_array(mysql_query($sql));

            $check_discount = mysql_fetch_array(mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemid' AND item_type = '$cat' AND status = 0"));

            $itmPrc = (!empty($check_discount['item_id']))?$check_discount['new_price']:$query[2];
            $rate_checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_ratings WHERE item_id = '$itemid' AND cart_id = '$cartID' AND item_type = '$cat' AND transaction_id = '$trasactionid'"));
            $btn_none = ($rate_checker[0] > 0 || $cat == 'A')?"display:none":"";
        ?>
          <li class="media" style="border-right: 2px solid #faa4c4;">
            <a class="pull-left" href="#">
              <img style='width: 200px;height: 200px;object-fit: cover;border-radius: 5px' class="media-object" src="../admin/assets/images/<?=$query[0]?>" alt="...">
            </a>
            <div class="media-body">
              <h4 class="media-heading"><?=$query[1]?></h4>
              <h4 class="media-heading">&#8369; <?=$itmPrc?></h4>
              <div class="col-md-4">
                 <div class="input-group">
                    <div class="input-group-addon" style="cursor: pointer;"> Quantity </div>
                    <input type="number" value='<?=$row["item_quantity"]?>' class="form-control" name="" id='cart_qntty' readonly>
                  </div>
              </div>
              <div class="col-md-6" style="<?=$btn_none?>">
                 <button class="btn btn-success" id="" onclick='send_ratings("<?=$cat?>",<?=$trasactionid?>, <?=$cartID?>, <?=$itemid?>)'><span class="glyphicon glyphicon-send"></span> Send Ratings</button>
              </div>
            </div>
          </li>
        <?php } ?>
        </ul>
    </div>
    
    </div>
      <?php } }else{ ?>
        <div class="col-md-12">
          <label><h3>NO TRANSACTIONS FOUND</h3></label>
        </div>
      <?php } ?>
        </div>
      </div>
    </div>
  </div>
   
  </div>
</div><br>
<div class="modal fade" id="send_rate" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="send_rate"><span class="glyphicon glyphicon-send"></span> Send Ratings </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <input id="rateValue1" type="hidden" readonly="">
            <input id="transid" type="hidden">
            <input id="cartid" type="hidden">
            <input id="type" type="hidden">
            <input id="itemid" type="hidden">
              <div class="stars">
                <input class="star star-5" id="star-5-2" type="radio" name="star" onclick="rateValue1(5)"/>
                <label class="star star-5" for="star-5-2"></label>
                <input class="star star-4" id="star-4-2" type="radio" name="star" onclick="rateValue1(4)"/>
                <label class="star star-4" for="star-4-2"></label>
                <input class="star star-3" id="star-3-2" type="radio" name="star" onclick="rateValue1(3)"/>
                <label class="star star-3" for="star-3-2"></label>
                <input class="star star-2" id="star-2-2" type="radio" name="star" onclick="rateValue1(2)"/>
                <label class="star star-2" for="star-2-2"></label>
                <input class="star star-1" id="star-1-2" type="radio" name="star" onclick="rateValue1(1)"/>
                <label class="star star-1" for="star-1-2"></label>
              </div>
          </div>   
          <div class="col-md-12">
             <div class="input-group">
                <span class="input-group-addon"> Comments/Remarks </span>
                 <textarea class="form-control" id="remarks" rows="2" style="resize: none"></textarea>
              </div>
          </div>  
        </div>
      </div>
      <div class="modal-footer">
        <span class="btn-group">
          <button class="btn btn-sm btn-success pull-right" onclick='sendRatings()'><span class="glyphicon glyphicon-send"></span> Rate Now</button>
        </span>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function showLocation(id){
    window.location = "item_view.php?view=view-map&id="+id;
  }
  function send_ratings(type,transid,cartid,itemid){
    $("#send_rate").modal();
    $("#transid").val(transid);
    $("#cartid").val(cartid);
    $("#type").val(type);
    $("#itemid").val(itemid);
  }
  function sendRatings(){
    var item_id = $("#itemid").val();
    var trans_id = $("#transid").val();
    var type = $("#type").val();
    var cart_id = $("#cartid").val();
    var ratings = $("#rateValue1").val();
    var comments = $("#remarks").val();

    $.post("../admin/ajax/send_ratings.php", {
      ratings: ratings,
      comments: comments,
      item_id: item_id,
      type: type,
      trans_id: trans_id,
      cart_id: cart_id
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Ratings Successfully Sent.",
            type: "success"
          }, function(){
            window.location.reload();
          });
      }else{
        swal("Something went wrong, Please Contact administrator");
      }
    })
  }
   function rateValue1(id){
    $("#rateValue1").val(id);
  }
  function finishTrans(id){
    $("#finishBtn"+id).prop("disabled", true);
    $("#finishBtn"+id).html("<span class='glyphicon glyphicon-check'></span> Loading");
    $.post("../admin/ajax/finish_trans.php", {
      id: id
    }, function(data){
      if(data > 0){
          swal({
            title: "All Good!",
            text: "Transaction Successfully Finished.",
            type: "success"
          }, function(){
            window.location.reload();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
    })
  }
  function updateProfile(){
    var firstname = $("#firstname").val();
    var middlename = $("#middlename").val();
    var lastname = $("#lastname").val();
    var emailadd = $("#emailadd").val();
    var connum = $("#connum").val();
    var address = $("#address").val();
    $("#updateBtn").prop("disabled", true);
    $("#updateBtn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("../admin/ajax/update_profile.php", {
      firstname: firstname,
      middlename: middlename,
      lastname: lastname,
      emailadd: emailadd,
      connum: connum,
      address: address
    }, function(data){
      if(data > 0){
          swal({
            title: "All Good!",
            text: "Profile Successfully Updated.",
            type: "success"
          }, function(){
            window.location.reload();
          });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
    })
  }
</script>