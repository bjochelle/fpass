<style type="text/css">
   #ribbon-container {
        position: absolute;
        top: 8px;
        right: 5px;
        overflow: visible; /* so we can see the pseudo-elements we're going to add to the anchor */
        font-size: 18px; /* font-size and line-height must be equal so we can account for the height of the banner */
        line-height: 18px;
      }
      
      #ribbon-container:before {
        content: "";
        height: 0;
        width: 0;
        display: block;
        position: absolute;
        top: 3px;
        left: 0;
        border-top: 3px solid rgba(0,0,0,.3);
        border-bottom: 29px solid rgba(0,0,0,.3);
        border-right: 24px solid rgba(0,0,0,.3);
        border-left: 34px solid transparent;
      }
      
      #ribbon-container:after { /* This adds the second part of our dropshadow */
        content: "";
        height: 3px;
        background: rgba(0,0,0,.3);
        display: block;
        position: absolute;
        bottom: -3px;
        left: 58px;
        right: 3px;
      }
      
      #ribbon-container a {
          display: block;
          padding: 8px;
          position: relative;
          background: red;
          overflow: visible;
          height: 32px;
          margin-left: 29px;
          color: #fff;
          text-decoration: none;
      }
      
      #ribbon-container a:after { /* this creates the "folded" part of our ribbon */
          content: "";
          height: 0;
          width: 0;
          display: block;
          position: absolute;
          bottom: -16px;
          right: 0;
          border-top: 16px solid #610404;
          border-right: 15px solid transparent;
      }
      
      #ribbon-container a:before { /* this creates the "forked" part of our ribbon */
          content: "";
          height: 0px;
          width: 0;
          display: block;
          position: absolute;
          top: 0;
          left: -29px;
          border-top: 16px solid red;
          border-bottom: 16px solid red;
          border-right: 16px solid transparent;
          border-left: 29px solid transparent;
      }
</style>
<div class="container text-center">    
  <h3> Cake List </h3><br>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>
<div class="row">
   <?php 
      $curdate = date("Y-m-d", strtotime(getCurrentDate()));
     $itemList = mysql_query("SELECT * FROM tbl_cakes");
     $count_c = mysql_num_rows($itemList);
     if($count_c > 0){
     while($itemfetch = mysql_fetch_array($itemList)){
      $itemID = $itemfetch['cake_id'];

      $color = (getRemainingQuantity($itemID, 'CE') == 0)?"color:red":"";
      $remain_qntty = "<h4 style='padding: 7px;background-color: #10c2ffa6;
    border-radius: 5px;$color'>Remaining Quantity: ".getRemainingQuantity($itemID, 'CE')."</h4>";

      $discountCheck = mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$itemID' AND item_type = 'CE' AND status = 0 AND discount_duration_date >= '$curdate'");
      $countRows = mysql_num_rows($discountCheck);
      $mfa_rows = mysql_fetch_array($discountCheck);
      $discount = ($mfa_rows['discount_type'] == 0)?"&#8369; ".number_format($mfa_rows['discounted_price'], 2)." Off":"".number_format($mfa_rows['discounted_price'])."% Off";
      if($countRows > 0){
        $ribbon = "<div id='ribbon-container'><a href='#'>".$discount."</a></div>";
        $hr = "<hr>";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($mfa_rows['new_price'], 2)."</h4>";

        $original_price = " <h4 style='font-weight: bolder;text-decoration: line-through;background-color: red;color: white;border-radius: 5px;'>&#8369; ".number_format($itemfetch['cake_price'], 2)."</h4>";
      }else{
        $ribbon = "";
        $hr = "";
        $discounted_price = "<h4 style='font-weight: bolder;background-color: #10c2ffa6;
    border-radius: 5px;'>&#8369; ".number_format($itemfetch['cake_price'], 2)."</h4>";

        $original_price = "";
      }
   ?>
    <div class="col-md-3">            
          <div class="thumbnail">
              <img src="../admin/assets/images/<?=$itemfetch['cake_img']?>" alt="..." style='height: 170px; width: 100%; object-fit: cover;'>
              <?=$ribbon?>
              <div class="caption">
                <h4><?=$itemfetch['cake_name']?></h4>
                <p><?=$itemfetch['cake_desc']?></p>
                <hr>
                <div class="row">
                  <div class="col-md-6"><?=$original_price?></div>
                  <div class="col-md-6"><?=$discounted_price?></div>
                  <div class="col-md-12"><?=$remain_qntty?></div>
                </div>
                <?php if($userID != 0) { ?>
                  <hr>
                 <a href="#" onclick='addTocartcakeChecker(<?=$itemID?>, "CE")' class="btn btn-info btn-sm btn-block" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart</a>
               <?php } ?>
              </div>
          </div>
      </div>
  <?php } }else{ ?>
    <div class="col-md-12">
      <h5>No Items Found</h5>
    </div>
  <?php } ?>
         
</div>
</div><br>
<script type="text/javascript">
  function addTocartcakeChecker(id, type){
    $.post("../admin/ajax/quantityChecker.php", {
      id: id,
      type: type
    }, function(data){
      if(data > 0){
          window.location = 'item_view.php?view=cake-add-to-cart&id='+id;
      }else{
        swal({
            title: "Aw Snap!",
            text: "Unable to add this item to your cart",
            type: "warning"
          }, function(){
            swal.close();
          });
      }
    }) 
  }
</script>