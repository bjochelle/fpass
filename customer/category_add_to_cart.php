<?php 
$item_id = $_GET['id'];
 $curdate = date("Y-m-d", strtotime(getCurrentDate()));

$getName = mysql_fetch_array(mysql_query("SELECT * FROM tbl_category_items WHERE item_id = '$item_id'"));

$discount = mysql_query("SELECT * FROM tbl_price_discount WHERE item_id = '$item_id' AND status = 0 AND item_type = 'C' AND discount_duration_date >= '$curdate'");
$count = mysql_num_rows($discount);
$fetch = mysql_fetch_array($discount);
if($count > 0){
  $discounted_price = "<span class='pull-right' style='font-size: 20px;font-weight: bolder;'>&#8369; ".number_format($fetch['new_price'], 2)."</span>";

  $original_price = "<span class='pull-right' style='font-size: 20px;font-weight: bolder;padding-right: 40px;text-decoration: line-through;'>&#8369; ".number_format($getName['item_price'], 2)."</span>";
}else{
  $discounted_price = "<span class='pull-right' style='font-size: 20px;font-weight: bolder;'>&#8369; ".number_format($getName['item_price'], 2)."</span>";

  $original_price = "";
}
$remain = getRemainingQuantity($item_id, 'C');
?>
<style type="text/css">
    .MultiCarousel { 
    float: left; 
    overflow: hidden; 
    padding: 15px; 
    width: 100%; 
    position:relative; }
  .MultiCarousel .MultiCarousel-inner { 
    transition: 1s ease all; 
    float: left; 
  }
  .MultiCarousel .MultiCarousel-inner .item { 
    float: left;
  }
  .MultiCarousel .MultiCarousel-inner .item > div {
   text-align: center; 
   padding:10px;
   margin:10px; 
   background:#f1f1f1; 
   color:#666;}
  .MultiCarousel .leftLst, .MultiCarousel .rightLst { 
    position:absolute; 
    border-radius:50%;
    top:calc(50% - 20px); 
  }
  .MultiCarousel .leftLst { 
    left:0; 
  }
  .MultiCarousel .rightLst { 
    right:0; 
  } 
  .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { 
    pointer-events: none; 
    background:#ccc; 
  }
  .larger{
    width: 20px !important;
    height: 20px !important;
    border-radius: 50% !important;
  }
    ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
</style>
<input type="hidden" id="itemID" value="<?=$item_id?>" name="">
<div class="container">  
<div class="container text-center">    
  <h3> Add Item to Cart </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
<div class="row">
  <div class="col-md-6">
      <img src="../admin/assets/images/<?=$getName['item_img']?>" style='width: 100%;height: auto;object-fit: cover;'>
  </div>
  <hr>

  <div class="col-md-6">
      <div class="col-md-6">
        <span class="" style="font-size: 25px;color: #f5669b;"> <?=$getName['item_name']?> </span> 
        
      </div>
      <div class="col-md-6">
        <?=$discounted_price?>
        <?=$original_price?>
      </div>
      
      <br>
      <hr>
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
              Product Description</a>
            </h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body"><?=$getName['item_desc']?></div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
              ADD-ONS TO MAKE IT EXTRA SPECIAL</a>
            </h4>
          </div>
          <div id="collapse2" class="panel-collapse collapse in">
            <div class="panel-body">
              <div class="MultiCarousel row" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                    <?php
                    $addons = mysql_query("SELECT * FROM tbl_addons");
                    while($addon_row = mysql_fetch_array($addons)){
                    ?>
                    <div class="item">
                        <div class="pad15">
                            <p class="lead">
                              <img src="../admin/assets/images/<?=$addon_row['addon_img']?>" style="height: 100px;width: 100%;object-fit: cover;">
                            </p>
                            <p><?=$addon_row['addon_name']?></p>
                            <p>&#8369; <?=$addon_row['addon_price']?></p>
                            <p style="text-align: center;">
                              
                                <input type="checkbox" value="<?=$addon_row['addon_id']?>" class="larger" name="add_addons">
                            
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <button class="btn btn-primary leftLst"><span class="glyphicon glyphicon-arrow-left"></span></button>
                <button class="btn btn-primary rightLst"><span class="glyphicon glyphicon-arrow-right"></span></button>
            </div>
            </div>
          </div>
        </div>
      </div>
       <div class="col-xs-6">
        <div class="input-group">
          <div class="input-group-addon"> Quantity: </div>
          <div class="input-group-addon" style="cursor: pointer;" onclick="minusQntty()"> <span class="glyphicon glyphicon-minus"></span> </div>
          <input type="number" min='1' max='<?=$remain?>' id='quantity' value="1" readonly class="form-control" name="">
          <div class="input-group-addon" style="cursor: pointer;" onclick="addQntty()"> <span class="glyphicon glyphicon-plus"></span> </div>
        </div>
      </div>
      <div class="col-xs-6">
        
        <button class="btn btn-sm btn-success pull-right" onclick='addtocart(<?=$remain?>)'><span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart</button>
   
      </div>
  </div>
  <div class="col-xs-12" style="border:1px solid #f1f1f1;margin-top: 10px"></div>
 
    <div class="col-md-8" style="margin-top: 20px">
      <h4 style="text-align: center;">Comments / Reviews</h4>
      <ul class="timeline">
        <?php 
        $rates = mysql_query("SELECT * FROM tbl_ratings WHERE item_id = '$item_id' AND item_type = 'C' ORDER BY date_added DESC");
        $counter = mysql_num_rows($rates);
        if($counter > 0){
        while($row_rates = mysql_fetch_array($rates)){
          $user_id = $row_rates['user_id'];
          $getCustomer = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname, ' ' , lastname) as fname FROM tbl_users WHERE user_id = '$user_id'"));
        ?>
        <li>
          <a style="color: #00b8ff;font-size: 14px;" href="#"><?=strtoupper($getCustomer['fname'])?></a>
          <a style="color: #00b8ff;font-size: 14px;" href="#" class="pull-right">
            <?=date('M d, Y h:i A', strtotime($row_rates['date_added']))?>

          </a><br>
          <a style="color: #00b8ff;font-size: 14px;" href="#" class="pull-right">
            <?=getRatingspercustomer($user_id,'C',$item_id)?>
          </a>
          <p>
            <?=$row_rates['comments']?>
          </p>
        </li>
        <?php } }else{ ?>
            <h3>No Ratings / Comments</h3>
        <?php } ?>
      </ul>
    </div>
    <?php 
      $total = getTotalReviews($item_id,'C');
      if($total > 0){
    ?>
    <div class="col-md-4" style="margin-top: 20px">
      <input type="hidden" value="<?=$item_id?>" id='itemID' name="">
      <h4 style="text-align: center;">Summary</h4>
      <div class="card">
        <div class="card-body">
          <ul style="list-style: none;text-align: center;">
            <div id="cont" style="background-color: #f1f1f1;padding: 10px;">
              <li style="font-size: 3em;"><?=getOverallRatings($item_id,'C')?></li>
              <li style="font-size: 1em;">Overall rating</li>
              <li>
                <h3><div id="stars" style="text-align: center;"></div><h3>
              </li>
              <li>Total Reviews (<?=getTotalReviews($item_id,'C')?>)</li>
            </div>
          </ul>
        </div>
      </div>
    </div>
    <?php } ?>
</div>
</div><br>
<script type="text/javascript">
   function averageRatings(){
    var itemID = $("#itemID").val();
    var type = 'C';
        $.post("../admin/ajax/getAverageRatings.php",{
          itemID: itemID,
          type: type
        }, function(data){
           document.getElementById("stars").innerHTML = getStars(data);
        })
    }
    function getStars(rating) {

      // Round to nearest half
      rating = Math.round(rating * 2) / 2;
      let output = [];

      // Append all the filled whole stars
      for (var i = rating; i >= 1; i--)
        output.push('<i class="fa fa-star" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      // If there is a half a star, append it
      if (i == .5) output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      // Fill the empty stars
      for (let i = (5 - rating); i >= 1; i--)
        output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      return output.join('');

    }
   function minusQntty(){
    var quantity = $("#quantity").val();
    var qntty = quantity - 1;
    if(qntty < 1){
      var final_qntty = 1;
    }else{
      var final_qntty = qntty;
    }
    $("#quantity").val(final_qntty);
  }
  function addQntty(){
    var quantity = $("#quantity").val();
    var qntty = parseFloat(quantity) + 1;
   
    $("#quantity").val(qntty);
  }
  $(document).ready(function () {
    averageRatings();
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1600) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 1392) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 1168) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
  function addtocart(remain){
    var itemID = $("#itemID").val();
    var quantity = $("#quantity").val();
    var checkedAddons = $('input[name="add_addons"]:checked').map(function() {
                            return this.value;
                        }).get();
    if(checkedAddons == ''){
      var array_null = "N";
    }else{
      var array_null = "NN";
    }
    arrayAddons = [];
    if(quantity <= remain){
      $.post("../admin/ajax/addtocart_category.php", {
        itemID: itemID,
        quantity: quantity,
        arrayAddons: checkedAddons,
        array_null: array_null
      }, function(data){
        if(data > 0){
          swal({
              title: "All Good!",
              text: "Successfully Added to Cart",
              type: "success"
            }, function(){
              window.location = 'item_view.php?view=view-cart';
            });
        }else{
          swal("Something went wrong, Please Contact administrator");
        }
      });
    }else{
      swal({
        title: "Aw Snap!",
        text: "Quantity Should be less than or equal to "+remain,
        type: "warning"
      }, function(){
        swal.close();
      });
    }
    
  }
</script>