<?php 
include('../admin/core/config.php');
if(!isset($_SESSION['cust_user_id'])){
  $display = "display: none";
  $signin = "display: block";
  $viewMore = "data-toggle='modal' data-target='#signIN'";
  $cart = "display: none";
  $auth = "display: block";
  $userID = 0;
}else{
  $display = "display: block";
  $signin = "display: none";
  $userID = $_SESSION['cust_user_id'];
  $cart = "display: block";
  $auth = "display: none";
}
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Flower PauER Arrangement Shop Syste,</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/font-awesome.css" rel="stylesheet">
  <link href="../admin/assets/css/sweetalert.min.css" rel="stylesheet">

  <script src="../assets/js/jquery.min.js"></script>
  <script src="../assets/js/bootstrap.min.js"></script>
  <script src="../admin/assets/js/sweetalert.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      max-height: 400px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  .badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 12px;
    font-weight: bold;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    /* vertical-align: middle; */
    background-color: #fb6fcc;
    border-radius: 10px;
    position: absolute;
    bottom: 7px;
    left: 10px;
}

  </style>
</head>
<body>
<nav class="navbar navbar-inverse" style="background-color: #f31b6c66;">
  <div class="container-fluid">
    <center><div class="navbar-header">
      <img src="../assets/images/logo13.png" style="height: 50px;width: 50px;object-fit: cover;">
    </div></center>
  </div>
</nav>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="../assets/images/banner1.png" style="object-fit: cover;" alt="Image">
        <div class="carousel-caption">
          <p></p>
        </div>      
      </div>

      <div class="item">
        <img src="../assets/images/banner2.png" alt="Image">
        <div class="carousel-caption">
          <p></p>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
<nav class="navbar" style="background-color: #f31b6c66;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color: black;"></span>
        <span class="icon-bar" style="background-color: black;"></span>
        <span class="icon-bar" style="background-color: black;"></span>                        
      </button>
      <a class="navbar-brand" href="#" onclick='window.location="../index.php"'><img src="../assets/images/logo13.png" style="height: 20px;width: 32px;object-fit: cover;"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="#" onclick='window.location="item_view.php?view=cake-list"'> Cakes </a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Types <span class="caret"></span></a>
          <ul class="dropdown-menu">
          	<?php 
          	$category = mysql_query("SELECT * FROM tbl_category");
          	while($row_c = mysql_fetch_array($category)){
              $catID = $row_c['category_id'];
          	?>
            	<li><a href="#" onclick='window.location="item_view.php?view=category-items&id=<?=$catID?>"'><?=$row_c['category_name']?></a></li>
        	<?php } ?>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Categories <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php 
          	$occasion = mysql_query("SELECT * FROM tbl_occasion_list");
          	while($row_o = mysql_fetch_array($occasion)){
              $occasionID = $row_o['occasion_id'];
          	?>
            	<li><a href="#" onclick='window.location="item_view.php?view=occasion-items&id=<?=$occasionID?>"'><?=$row_o['occasion_name']?></a></li>
        	<?php } ?>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li style="<?=$auth?>"><a href="#" data-toggle='modal' data-target='#loginModal'>Login</a></li>
        <li style="<?=$auth?>"><a href="#" data-toggle='modal' data-target='#signup'>Sign Up</a></li>
        <li style="<?=$cart?>"><a href="#" onclick='viewCart()'><span class="glyphicon glyphicon-shopping-cart"><span class="badge"><?=countCartItems($userID)?></span></span></a></li>
        <li class="dropdown" style="<?=$cart?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#" onclick='window.location="item_view.php?view=profile&id=<?=$userID?>"'> <span class="glyphicon glyphicon-user"></span> Profile </a></li>
            <li><a href="#" onclick='window.location="item_view.php?view=t_and_c"'><span class="glyphicon glyphicon-th-list"></span> Terms and Conditions </a></li>
            <li><a href="#" onclick='window.location="item_view.php?view=messages"'><span class="glyphicon glyphicon-envelope"></span> Messages </a></li>
            <li><a href="#" onclick='logout()'><span class="glyphicon glyphicon-log-out"></span> Logout </a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<?php include '../core/cust_routes.php';?>

<footer class="container-fluid text-center" style="background-color: #faa4c43d;">
 	<p class="credits">
         © 2020 Flower PauER Arrangement Shop System.
     </p>
</footer>
<div class="modal fade" id="loginModal" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="cake_variants"><span class="glyphicon glyphicon-lock"></span> Authentication </h4>
      </div>
      <div class="modal-body">
          <div class="input-group">
            <div class="input-group-addon">
                Username
            </div>
            <input type="text" id="username" name="username" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Password
            </div>
            <input type="password" id="password" name="password" class="form-control">
          </div>
      </div>
      <div class="modal-footer">
        <span class="btn-group">
          <button class="btn btn-primary btn-sm" id="btnLogin" onclick='loginUser()' type="button"><span class="fa fa-check-circle"></span> Login</button>
        </span>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="signup" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="cake_variants"><span class="glyphicon glyphicon-lock"></span> Register </h4>
      </div>
      <div class="modal-body">
        <div class="input-group">
            <div class="input-group-addon">
                Firstname
            </div>
            <input type="text" id="firstname" name="firstname" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Middlename
            </div>
            <input type="text" id="middlename" name="middlename" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Lastname
            </div>
            <input type="text" id="lastname" name="lastname" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Contact #
            </div>
            <input type="text" id="contact" name="contact" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Address
            </div>
            <textarea style="resize: none;" rows="4" class="form-control" id="address"></textarea>
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Username
            </div>
            <input type="text" id="s_username" name="s_username" class="form-control">
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-addon">
                Password
            </div>
            <input type="password" id="s_password" name="s_password" class="form-control">
          </div>
      </div>
      <div class="modal-footer">
        <span class="btn-group">
          <button class="btn btn-primary btn-sm" id="btnsignin" onclick='registerUser()' type="button"><span class="fa fa-check-circle"></span> Login</button>
        </span>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript">
  //window.location.href = 'index.php?view=home';
  $(document).ready(function(){

  });
  function viewCart(){
    window.location = 'item_view.php?view=view-cart';
  }
  function logout(){
    window.location = '../admin/ajax/cust_logout.php';
  }
  function registerUser(){
    var firstname = $("#firstname").val();
    var middlename = $("#middlename").val();
    var lastname = $("#lastname").val();
    var contact = $("#contact").val();
    var address = $("#address").val();
    var username = $("#s_username").val();
    var password = $("#s_password").val();
    $("#btnsignin").prop("disabled", true);
    $("#btnsignin").html("<span class='glyphicon glyphicon-play-circle'></span> Loading");
    $.post("../admin/ajax/register_customer.php", {
      firstname: firstname,
      middlename: middlename,
      lastname: lastname,
      contact: contact,
      address: address,
      username: username,
      password: password
    }, function(data){
      if(data > 0){
          alert("Successfully Registered");
      }else{
        alert("Error");
      }
      
      window.location.reload();
      $("#btnsignin").prop("disabled", true);
      $("#btnsignin").html("<span class='glyphicon glyphicon-play-circle'></span> Loading");
    });
  }
  function loginUser(){
    var username = $("#username").val();
    var password = $("#password").val();
    $.post("../admin/ajax/sign_in_customer.php", {
      username: username,
      password: password
    }, function(data){
      if(data > 0){
        window.location.reload();
      }else{
        alert("Login Error");
      }
    })
  }
</script>
