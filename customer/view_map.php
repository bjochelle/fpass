<?php 
$get = $_GET['id'];
?>
<style type="text/css">
#track_map {
      height: 450px;;  /* The height is 400 pixels */
      width: 100%;  /* The width is the width of the web page */
     }
</style>
<div class="container">  
<div class="container text-center">    
  <h3> Track Locations </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
  <div class="row">
    <div class='col-md-12'>
      <input type="hidden" id='transID' value="<?=$get?>" name="">
      <div id="track_map"></div>
    </div>
  </div>
</div>
</div><br>

<script type="text/javascript">
  $(document).ready(function(){
       var transID = $("#transID").val();
      setInterval( function(){
        loadMap(transID);
      }, 10000);
      
      
    })
function loadMap(transID){
  var transType = "online";
    $.post("../admin/ajax/locations_tracking.php",{
      transID: transID,
      transType: transType
    }, function(data){
      var markers = JSON.parse(data);
      var map = new google.maps.Map(document.getElementById('track_map'), {
            zoom:16,
            center: {lat: markers.data[1].lat, lng: markers.data[1].long},
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
      
      

      for (i = 0; i < markers.data.length; i++) {
        var marker = new google.maps.Marker({
              position: new google.maps.LatLng(markers.data[i].lat, markers.data[i].long),
              //icon: icon,
              map: map,
              // animation : google.maps.Animation.DROP,
              label: markers.data[i].text
            });

        // google.maps.event.addListener(marker, 'click', (function(marker, i) {
       //        return function() {
       //          infowindow.setContent(markers.data[i].text);
       //          infowindow.open(map, marker);
       //        }
       //      })(marker, i));
      }
       marker.setMap(map);
    });
  }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback"> </script>