<style type="text/css">

</style>
<div class="container">  
<div class="container text-center">    
  <h3> Terms And Conditions </h3><br>
</div>
<div class="col-md-12" style="border: 1px solid #d53974;"></div><br>  
  <div class="row">
  <div class="col-xs-12">
    <h5>By placing an order on this website, you are agreeing to the following terms and conditions:</h5>
  </div>
  <div class="col-xs-12">
    <ul>
      <li>
        <h4>Order Acceptance Policy</h4>
        <ul>
          <li>
            <p>
              All orders and other requests received are subject to acceptance by Beyond Flowers Inc. (legal entity for Flower PauER), and any of its personnel, reserve the right, at our absolute discretion, to reject any order without giving reasons. In the event of rejection, we will refund or cancel any payments received in full, via the payment method used to place the order.
            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Delivery of your order</h4>
        <ul>
          <li>
            <p>
              We provide same day deliveries for orders confirmed before 2:00 PM. Order placed from 2:00 PM onward are delivered on the night delivery slot or the following day or any other day as selected by the customer on the cart page.
            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Changes to your order</h4>
        <ul>
          <li>
            <p>
              If you wish to make any changes your order, please do so by message us via chat through our website. We will always do our best to make last-minute changes for you, but we can only guarantee changes (including to the delivery address and in relation to card messages) that are requested at least 24 hours or one (1) day before the intended delivery day.
            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Cancellation policy</h4>
        <ul>
          <li>
            <p>
              On demand orders can be cancelled 24 hours or one (1) day before the intended delivery day and a full refund will be issued. You can cancel by messaging us via chat through our website.
            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Product and Substitution Policy</h4>
        <ul>
          <li>
            <p>
            All Products are subject to availability. In the order of any supply difficulties, we reserve the right to substitute a Product of equivalent value and quality without any prior notice. In terms of product requests, we will try to fulfill all requests to the best of our ability. For simple requests like changing the color of the flower from say, red to white, please inform us via chat through our website and will try to accommodate the request subject to the availability of the color. 
            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Payments</h4>
        <ul>
          <li>
            <p>
              We accept through gcash, remittances, bank transfers, cash on delivery. But we are not linked to those payment processes, you'll only need to pay and send us your receipt for proof.
            </p>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="col-xs-12">
    <h5>All prices include delivery charges unless otherwise stated:</h5>
  </div>
  <div class="col-xs-12">
    <ul>
    <li>
        <h4>Delivery policy</h4>
        <ul>
          <li>
            <p>
              Flower deliveries can be done on Mondays to Sundays.
              Timed deliveries are not available.
              Although our team will always try our best to ensure punctual delivery for our customers, Flower PauER cannot be held responsible if a delivery arrives late due to any circumstances impacting the availability of the courier service that we use or anything else outside of Flower PauER's control.
              In the event of a non-delivery, please message us via chat through our website.  It is the customer's responsibility to contact us within one (1) day (24 hours) of the scheduled delivery date in order to claim a refund. Failure to do so will result in the lapse to any rights of a refund.
              Flower PauER only delivers within the areas listed in our FAQ at this moment but do not deliver to PO boxes.
              We reserve the right, at our absolute discretion, to use a different delivery method without prior notification.
              If you change address, you must update your address details on the "My Account" section of the website to ensure that no deliveries are sent out to the wrong address. Please ensure this is done 24 hours or one (1) day before the intended delivery date / slot. We are unable to provide refunds for any deliveries sent to the wrong location where we have not received advance notice in accordance with this paragraph.
            </p>
          </li>
        </ul>
      </li>
    <li>
        <h4>Freshness</h4>
        <ul>
          <li>
            <p>
            We promise to send you the freshest of flowers and this would mean that they will occasionally come in bud form which will last longer. If you receive flowers that do not seem fresh, please message us immediately. If, at our discretion, the flowers do not meet our high freshness standards, we will give you the choice of a replacement at our next available delivery date or a refund. In your email, please include a photo of the un-fresh flowers within 12 hours of receipt in order to be eligible for a refund. If we offer you a refund, please note that banks generally take up to 10 business days to process and transfer the funds into your account. Some banks can take up to 15-30 days, or until your next billing cycle. Flower PauER has no influence over these timescales.
            </p>
          </li>
        </ul>
      </li>
    <li>
        <h4>Damage</h4>
        <ul>
          <li>
            <p>
            Our flowers are wrapped or arranged carefully to ensure they arrive as beautiful and as fresh as when they left us. However, on a small number of occasions (and for reasons beyond our control) they may get damaged in transit. Should this occur to a level that you deem unacceptable, please message us immediately so that we can arrange a replacement at our next available delivery date, or a refund. Please note that you will need to email us a photo of the damaged flowers within 12 hours of receipt in order to be eligible for a refund. If we offer you a refund, please note that banks generally take up to 10 business days to process and transfer the funds into your account. Some banks can take up to 15-30 days, or until your next billing cycle. Flower PauER has no influence over these timescales.
            </p>
          </li>
        </ul>
      </li>
    <li>
        <h4>Non Delivery</h4>
        <ul>
          <li>
            <p>
              We do everything we can to ensure our flowers are delivered. If for any reason we made a mistake and deliveries don't turn up, (i.e. if your flowers have not arrived on their intended delivery date), please message us. We will send you a replacement on our next available delivery date, or a refund. If we offer you a refund, please note that banks generally take up to 10 business days to process and transfer the funds into your account. Some banks can take up to 15-30 days, or until your next billing cycle. Flower PauER has no influence over these timescales.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Satisfaction Guarantee</h4>
        <ul>
          <li>
            <p>
              Satisfying flower recipient and sender is our ultimate aim. We always strive to deliver fresh flowers at its best condition and on time.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Return and Refunds Policy</h4>
        <ul>
          <li>
            <p>
              To be eligible for returns and refunds, you should report your case within 24 hours upon delivery of your flowers and is subjected to a mandatory review and investigation process by our Customer Support team. The flowers should also remain the same way as delivered. When filing your report, kindly provide us with your order number, several pictures of the defective product, and reasons on why are you requesting for a return or refund. In all other cases, it is the responsibility of the recipient to care for the flowers once it has been delivered.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Flower availability and substitution</h4>
        <ul>
          <li>
            <p>
              All products are subject to availability. In the event of any supply difficulties or if the flowers we have received from our growers that are needed to make up your order do not meet our high quality standards, we reserve the right, at our absolute discretion, to substitute any product with an alternate product of a similar style and equivalent (or greater) value and quality.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Offers and promotion codes</h4>
        <ul>
          <li>
            <p>
              At our discretion, from time to time, we may offer products at discounted prices. These offers are valid from the time that we introduce them to the end date of the offer and they cannot be used for purchases before the offer introduction date or after the offer end date.
              In the event that a customer has made a purchase and the price of the purchased product subsequently falls or is discounted owing to a special offer, the price of the product at the time of purchase shall prevail. We are unable to offer special offer discounts for purchases that have already been made.
              As our special offers are contingent on availability, we may change the terms of special offers, or withdraw them altogether, at any time, and without prior notice.
              We also reserve the right, at our absolute discretion, to offer different personalised special offers and promotions and it will therefore only be possible for the customer in receipt of the special offer to redeem the discount.
              Unless explicitly otherwise stated, free or discounted introductory offers are only available to new users of the Flower PauER service, and are only available once to any one person.
              Discounts and credits cannot be used in conjunction with any other offers.
              Unless otherwise stated, we only allow one promotion code to be used per order.
              Circumstances beyond our control

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Adverse weather conditions</h4>
        <ul>
          <li>
            <p>
              
            During adverse weather conditions (including heavy rain, storm, typhoon and flood), our delivery drivers may not be able to deliver orders on time. This is outside of our control and we cannot accept responsibility for the late delivery of the order. Therefore, in the event of adverse weather conditions, we aren't able to refund or offer re-delivery of affected orders.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Force Majeure</h4>
        <ul>
          <li>
            <p>
              
            Flower PauER shall not be liable for delay in performing or for failure to perform its obligations if the delay or failure results from any of the following: (i) Acts of God, (ii) outbreak of hostilities, riot, civil disturbance, acts of terrorism, (iii) the act of any government or authority (including refusal or revocation of any licence or consent), (iv) fire, explosion, flood, fog or adverse weather, (v) power failure, failure of telecommunications lines, failure or breakdown of plant, machinery or vehicles, (vi) default of suppliers, sub-contractors or delivery partners, (vii) theft, malicious damage, strike, lock-out or industrial action of any kind, and (viii) any cause or circumstance whatsoever beyond Flower PauER's reasonable control.

            </p>
          </li>
        </ul>
      </li>
      <li>
        <h4>Customer and recipient personal information</h4>
        <ul>
          <li>
            <p>
              
            To ensure that we can communicate effectively with both customers and recipients, it is very important that you provide accurate personal information.
            Please remember that we value your privacy and will never lease, rent or sell your private information. For more information, please see our privacy policy.

            </p>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="col-xs-12">
    <h5>During the checkout process, we ask for the following personal information:</h5>
  </div>
  <div class="col-xs-12">
    <ul>
    <li>
        <h4>Customer's email address</h4>
        <ul>
          <li>
            <p>
              We use this information to provide a better customer experience by sending order confirmations, substitution information, dispatch confirmations and delivery confirmations. We will also use the customer's email for marketing communications from time to time. Please ensure that email addresses are accurate.
            </p>
          </li>
          <li>
            <p>
              Customer's full name and address
            </p>
          </li>
          <li>
            <p>
              Customer's telephone number
            </p>
          </li>
          <li>
            <p>
              We use this information to contact customers in the event of problems with the order such as payment failures or delivery issues.
            </p>
          </li>
          <li>
            <p>
              Recipient's full name and address
            </p>
          </li>
          <li>
            <p>
              We require this information in order to deliver the flowers you have ordered. It is vital that the recipient's address is accurate.
            </p>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  </div>
   
  </div>
</div><br>

<script type="text/javascript">

</script>